<?php

Route::get('/', 'Controller@home');
Route::post('/', 'Controller@home');

### Admin interface routes ###

Route::group(['prefix' => '/admin'], function() {
    #authentication
    Route::get('/login', 'Auth\LoginController@getLogin')->name('getLogin');
    Route::post('/login', 'Auth\LoginController@postLogin')->name('postLogin');
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
    
    #content
    Route::get('/', 'Admin\DefaultController@dashboard')->name('dashboard');
    Route::get('/content', 'Admin\ContentController@index')->name('content_list');
    Route::get('/content/create', 'Admin\ContentController@create')->name('content_create');
    Route::get('/content/{id}', 'Admin\ContentController@edit')->name('content_edit');
    Route::post('/content/store', 'Admin\ContentController@store')->name('content_store');
    Route::post('/content/put/{id}', 'Admin\ContentController@put')->name('content_put');
    Route::get('/content/{id}/delete', 'Admin\ContentController@delete')->name('content_delete');
    
    #users
    Route::get('/users', 'Admin\UserController@index')->name('user_list');
    Route::get('/users/{id}', 'Admin\UserController@view')->name('user_view');
    Route::get('/users/{id}/block', 'Admin\UserController@block')->name('user_block');
    Route::get('/location/map', 'Admin\UserController@showMap')->name('show_map');
    
    #drifts
    Route::get('/journeys', 'Admin\DriftController@index')->name('drift_list');
    
    #notification
    Route::any('/notifications', 'Admin\NotificationController@index')->name('notification_list');

    #statistic
    Route::get('/statistics', 'Admin\StatisticController@index')->name('statistic_list');
    
    Route::get('password/email', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');

    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

});

### End admin interface routes ###

Route::post('/login', 'LoginController@login')->name('login');
Route::post('/logout', 'LoginController@logout')->name('logout');
Route::post('/login/checkemail', 'LoginController@checkEmail')->name('check_email');
Route::post('/login/checknickname', 'LoginController@checkNickname')->name('check_nickname');

Route::post('/register', 'RegistrationController@registration')->name('register');

Route::get('/countries', 'DataController@getCountryList');
Route::get('/content/{name}', 'DataController@getContent');
Route::get('/support/categories', 'SupportController@getCategoryList');

Route::group(['prefix' => '/', 'middleware' => ['token_validator']], function() {
    Route::post('/changepassword', 'UserController@changePassword');    
});

Route::group(['prefix' => '/drifts', 'middleware' => ['token_validator']], function(){
   Route::post('/{id}/review', 'DriftController@addReview');
   Route::post('/{id}/start', 'DriftController@startDrift')->where(['id'=>'[0-9]+']);
   Route::post('/{id}/end', 'DriftController@endDrift')->where(['id'=>'[0-9]+']);
   Route::post('/{id}/cancel', 'DriftController@cancelDrift')->where(['id'=>'[0-9]+']);
   Route::post('/{id}/uploadphoto', 'DriftController@uploadPhoto')->where(['id'=>'[0-9]+']);
   Route::get('/{id}/photos', 'DriftController@getDriftPhotoList')->where(['id'=>'[0-9]+']);
   Route::get('/{id}', 'DriftController@getDrift')->where(['id'=>'[0-9]+']);
});

Route::group(['prefix' => '/driftinvitations', 'middleware' => ['token_validator']], function(){
   Route::post('/send', 'DriftInvitationController@sendInvitation');
   Route::post('/{id}/accept', 'DriftInvitationController@acceptInvitation')->where(['id'=>'[0-9]+']);
   Route::post('/{id}/reject', 'DriftInvitationController@rejectInvitation')->where(['id'=>'[0-9]+']);
});

Route::group(['prefix' => '/users', 'middleware' => ['token_validator']], function() {
    Route::post('/me/vehicles/update', 'UserController@updateVehicles');
    Route::post('/me/profilepicture', 'UserController@updateProfilePicture');
    Route::post('/me/vehicles/havenone', 'UserController@hasNoneVehicle');
    Route::post('/me/about', 'UserController@updateAbout');
    Route::get('/me', 'UserController@getProfileInfo');
    Route::post('/me', 'UserController@updateProfileInfo');
    Route::get('/{id}', 'UserController@getUser')->where(['id'=>'[0-9]+']);
    Route::get('/{id}/location', 'UserController@getLastLocation')->where(['id'=>'[0-9]+']);
    Route::post('/me/mode', 'UserController@updateUserMode');
    Route::post('/me/companionmode', 'UserController@updateUserCompanionMode');
    Route::post('/me/invisiblemode', 'UserController@updateUserVisibilityMode');
    Route::get('/{id}/companions', 'UserController@getCompanionList')->where(['id'=>'[0-9]+']);
    Route::get('/{id}/reviews', 'UserController@getReviewList')->where(['id'=>'[0-9]+']);
    Route::get('/{id}/drifts', 'UserController@getDriftList')->where(['id'=>'[0-9]+']);
    Route::get('/{id}/photos', 'UserController@getPhotoList')->where(['id'=>'[0-9]+']);
    Route::get('/me/driftinvitations', 'UserController@getDriftInvitationList');
    Route::post('/me/location', 'UserController@addNewLocation');
    Route::post('/me/destination', 'UserController@updateDestination');
    Route::post('/me/companions/{id}/remove', 'UserController@removeCompanion')->where(['id'=>'[0-9]+']);
    Route::post('/me/companions/{id}/block', 'UserController@blockCompanion')->where(['id'=>'[0-9]+']);
    Route::post('/me/companions/{id}/unblock', 'UserController@unblockCompanion')->where(['id'=>'[0-9]+']);
    Route::post('/me/devicetoken', 'UserController@updateDeviceToken');
    Route::post('/{id}/companions/request', 'UserController@requestCompanion')->where(['id'=>'[0-9]+']);
    Route::get('/me/companions/request', 'UserController@requestMyCompanionRequestList');
    Route::get('/search', 'UserController@searchUser');
    Route::get('/me/driftsandinvitations', 'UserController@driftAndInvitationList');
    Route::get('/me/openrequests', 'UserController@openRequestList');
    Route::get('/me/chatclienturl', 'UserController@getChatClientUrl');
    Route::post('/me/route', 'UserController@updatePlannedRoute');
    Route::post('/{id}/message', 'UserController@sendMessage');
});

Route::group(['prefix' => '/companionrequests', 'middleware' => ['token_validator']], function(){
   Route::post('/{id}/accept', 'CompanionRequestController@acceptRequest')->where(['id'=>'[0-9]+']);
   Route::post('/{id}/reject', 'CompanionRequestController@rejectRequest')->where(['id'=>'[0-9]+']);
});

Route::group(['prefix' => '/support', 'middleware' => ['token_validator']], function(){
   Route::post('/submit', 'SupportController@sendMessage');
});

Route::group(['prefix' => '/map', 'middleware' => ['token_validator']], function(){
   Route::get('/users', 'MapController@userList');
   Route::get('/driftpoints', 'MapController@driftList');
});

Route::post('chat/notification', 'ChatController@postNotification');
