<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'email' => 'Email',
    'submit_button' => 'Submit',
    'reset_password' => 'Reset password',
    'password_reset_subject' => 'Reset password mail',
    'password_reset_mail_text' => 'if you want to reset password click to button below',
    'click_here' => 'Click here',
    'password' => 'New password',
    'password_repeat' => 'Repeat password',
    'enter_email_help' => 'You will receive an email with reset password link',
];
