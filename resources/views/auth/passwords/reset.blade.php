@extends('layout.admin')

@section('content')

 <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-wrapper">
                <div class="box">
                    <div class="content-wrap">
                        <h6>{{ trans('auth.reset_password') }}</h6>
                        <form role="form" method="post" action="{{action('Auth\ResetPasswordController@reset')}}">
                            {!! csrf_field() !!}
                            <input type="hidden" name="token" value='{{$token}}'>
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="{{ trans('auth.email') }}" name="email" type="email">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="{{ trans('auth.password') }}" name="password" type="password" value="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="{{ trans('auth.password_repeat') }}" name="password_confirmation" type="password" value="">
                                </div>
                                <input type="submit" class="btn btn-primary" value="{{ trans('auth.submit_button') }}">
                            </fieldset>
                        </form>
                        @if (count($errors) > 0)
                            <div style="color:#A70000;">
                                @foreach ($errors->all() as $error)
                                    <span>{{ $error }}</span><br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection