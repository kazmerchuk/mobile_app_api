@extends('layout.admin')

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-wrapper">
            <div class="box">
                <div class="content-wrap">
                    <h6>{{ trans('default.admin.signin') }}</h6>
                    <form action="{{ route('postLogin') }}" method="POST">
                        <input class="form-control" name="email" type="text" placeholder="{{ trans('default.admin.email') }}">
                        <input class="form-control" name="password" type="password" placeholder="{{ trans('default.admin.password') }}">
                        <input type="submit" class="btn btn-primary" value="{{ trans('default.admin.login') }}">
                        <input type="hidden" value="{{ csrf_token() }}"/>
                    </form>
                    <a href="{{ url('admin/password/email')}}" style="margin-top:10px;">{{ trans('default.forgot_password_link') }}</a>
                </div>
                @if (count($errors) > 0)
                    <div style="color:#A70000; margin-top:10px;">
                        @foreach ($errors->all() as $error)
                            <span>{{ $error }}</span><br>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
</div>

@endsection