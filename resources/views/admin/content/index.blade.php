@extends('layout.admin')

@section('right-panel')
    <div class="content-box-large">
        <div class="panel-heading">
            <div class="panel-title">{{ trans('default.admin.content.title') }}</div>
        </div>
        <div class="panel-body">
            @if (count($contentItems))
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                    <thead>
                        <tr style="font-weight: bold;">
                            <td>
                                {{ trans('default.admin.content.name') }}
                            </td>
                            <td>
                                {{ trans('validation.attributes.language') }}
                            </td>
                            <td>
                                {{ trans('validation.attributes.actions') }}
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($contentItems as $item)
                            <tr>
                                <td>
                                    <a href="{{ route('content_edit', ['id' => $item->id]) }}">
                                        {{ $item->name }}
                                    </a> 
                                </td>
                                <td>
                                    {{ $item->language }}
                                </td>
                                <td>
                                    <a class="btn btn-danger" href="{{ route('content_delete', ['id' => $item->id]) }}" onclick="return confirm('{{ trans('default.delete_confirmation_message') }}');">
                                        {{ trans('validation.attributes.delete') }}
                                    </a> 
                                </td>
                            </tr>
                        @endforeach
                    <tbody>
                </table>
            @endif
            <a href="{{ route('content_create') }}" class="btn btn-success">{{ trans('validation.attributes.create') }}</a>
        </div>
    </div>
@endsection
