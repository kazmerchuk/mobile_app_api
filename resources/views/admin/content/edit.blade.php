@extends('layout.admin')

@section('right-panel')
    <div class="content-box-large">
        <div class="panel-heading">
            <div class="panel-title">{{ trans('default.admin.content.edit_title') }}</div>
        </div>
        <div class="panel-body">
            {!! Form::open(['method' => 'POST', 'route' => ['content_put', $content->id], 'class' => 'form-horizontal']) !!}
                <div class="form-group">
                    <div class="col-lg-3">
                        {!! Form::label('name', trans('validation.attributes.name'), ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-lg-9">
                        {!! Form::text('name', $content->name, ['class' => 'form-control client-list']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-3">
                        {!! Form::label('language', trans('validation.attributes.language'), ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-lg-9">
                        {!! Form::text('language', $content->language, ['class' => 'form-control client-list']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-3">
                        {!! Form::label('content', trans('validation.attributes.content'), ['class' => 'control-label']) !!}
                    </div>
                    <div class="col-lg-9">
                        {!! Form::textarea('content', $content->content, ['class' => 'form-control content-body']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-2">
                        {!! Form::submit(trans('validation.attributes.create'), ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! csrf_field() !!}
            {!! Form::close() !!}
            @include('partials.errors')
        </div>
    </div>
@endsection

