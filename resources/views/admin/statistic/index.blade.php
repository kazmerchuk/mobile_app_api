@extends('layout.admin')

@section('right-panel')
    <div class="content-box-large">
        <div class="panel-heading">
            <div class="panel-title">{{ trans('default.admin.statistic.title') }}</div>
             <div class="pull-right">
                <form action="" method="GET">
                    <input type="text" name="date-from" value="{{ Input::get('date-from') }}" placeholder="{{ trans('default.admin.drift.date_from') }}"/>
                    <input type="text" name="date-until" value="{{ Input::get('date-until') }}" placeholder="{{ trans('default.admin.drift.date_until') }}"/>
                    <input type="submit" value="{{ trans('default.admin.user.search_button') }}"/> 
                </form>
            </div>
        </div>
        <hr/>
        <div class="panel-body">
            @if (count($statistics))
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                    <thead>
                        <tr style='font-weight: bold;'>
                            <td>
                                <b>{{ trans('default.admin.statistic.date') }} </b>
                            </td>
                            <td>
                                <b>{{ trans('default.admin.statistic.registrations') }} </b>
                            </td>
                            <td>
                                <b>{{ trans('default.admin.statistic.active_users') }} </b>
                            </td>
                            <td>
                                <b>{{ trans('default.admin.statistic.proposed_drifts') }} </b>
                            </td>
                            <td>
                                <b>{{ trans('default.admin.statistic.agreed_drifts') }} </b>
                            </td>
                            <td>
                                <b>{{ trans('default.admin.statistic.reviews') }} </b>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($statistics as $statistic)
                            <tr>
                                <td>
                                   {{ $statistic->date }}
                                </td>
                                <td>
                                   {{ $statistic->new_registration_count }}
                                </td>
                                <td>
                                   {{ $statistic->active_users }}
                                </td>
                                <td>
                                   {{ $statistic->proposed_drift_count }}
                                </td>
                                <td>
                                   {{ $statistic->agreed_drift_count }}
                                </td>
                                <td>
                                   {{ $statistic->rating_number_count }}
                                </td>
                            </tr>
                        @endforeach
                    <tbody>
                </table>
                {!! $statistics->appends(['query' => Input::get('query'), 'date-from' => Input::get('date-from'), 'date-until' => Input::get('date-until')])->render() !!}
            @endif
        </div>
    </div>
@endsection
