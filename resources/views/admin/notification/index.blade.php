@extends('layout.admin')

@section('right-panel')
    <div class="content-box-large">
        <div class="panel-heading">
            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
            <div class="panel-title">{{ trans('default.admin.notification.title') }}</div>
        </div>
        <hr/>
        <div class="panel-body">
            {!! Form::open(['method' => 'POST', 'route' => ['notification_list'], 'class' => 'form-horizontal']) !!}
                <div>
                    <div class="form-group">
                        <div class="col-lg-3">
                            {!! Form::label('message', trans('validation.attributes.notification_message'), ['class' => 'control-label']) !!}
                        </div>
                        <div class="col-lg-7">
                            {!! Form::textarea('message', '', ['class' => 'form-control', 'required' => 'true']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-3">
                            {!! Form::label('user_group', trans('validation.attributes.user_group'), ['class' => 'control-label']) !!}
                        </div>
                        <div class="col-lg-7">
                            {!! Form::select('user_group', $groups, [], ['class' => 'form-control', 'required' => 'true']) !!}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-7">
                            {!! Form::submit(trans('default.admin.notification.send_button'), ['class' => 'btn btn-primary form-control', 'style' => 'width:200px; float:right;']) !!}
                        </div>
                    </div>
                    
                    <div style="width:200px;">
                        
                    </div>
                    {!! csrf_field() !!}
                </div>
                {!! Form::close() !!}   
        </div>
    </div>
@endsection
