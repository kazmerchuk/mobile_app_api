@extends('layout.admin')

@section('right-panel')
    <div class="content-box-large">
        <div class="panel-heading">
            <div class="panel-title">{{ trans('default.admin.drift.title') }}</div>
             <div class="pull-right">
                <form action="" method="GET">
                    <input type="text" name="date-from" value="{{ Input::get('date-from') }}" placeholder="{{ trans('default.admin.drift.date_from') }}"/>
                    <input type="text" name="date-until" value="{{ Input::get('date-until') }}" placeholder="{{ trans('default.admin.drift.date_until') }}"/>
                    <select name="status">
                        <option value=""></option>
                        @foreach ($statuses as $key => $status)
                            <option value="{{ $status }}">{{ trans('default.status_' . $status) }}</option>
                        @endforeach
                    </select>
                    <input type="submit" value="{{ trans('default.admin.user.search_button') }}"/> 
                </form>
            </div>
        </div>
        <div class="panel-body">
            @if (count($drifts))
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                    <thead>
                        <tr style='font-weight: bold;'>
                            <td>
                                {{ trans('default.admin.drift.dates') }}
                            </td>
                            <td>
                                {{ trans('default.admin.drift.user_data') }}
                            </td>
                            <td>
                                {{ trans('default.admin.drift.from') }}
                            </td>
                            <td>
                                {{ trans('default.admin.drift.to') }}
                            </td>
                            <td>
                                {{ trans('default.admin.drift.status') }}
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($drifts as $drift)
                            <tr>
                                <td>
                                    {{ !is_null($drift->startdate) ? $drift->startdate : '?' }} - {{ !is_null($drift->enddate) ? $drift->enddate: '?'  }}
                                </td>
                                <td>
                                    {{ trans('default.admin.drift.driver') }} : <a href='{{ route('user_view', $drift->driver_id) }}'>{{ $drift->driver->firstname }} {{ $drift->driver->lastname }}</a><br/>
                                    {{ trans('default.admin.drift.drifter') }} : <a href='{{ route('user_view', $drift->drifter_id) }}'>{{ $drift->drifter->firstname }} {{ $drift->drifter->lastname }}</a><br/>
                                </td>
                                <td>
                                    {{ $drift->startcaption }}
                                </td>
                                <td>
                                    {{ $drift->endcaption }}
                                </td>
                                <td>
                                    {{ $drift->getStatus() }}
                                </td>
                            </tr>
                        @endforeach
                    <tbody>
                </table>
            @endif
            {!! $drifts->appends(['query' => Input::get('query'), 'date-from' => Input::get('date-from'), 'date-until' => Input::get('date-until'), 'status' => Input::get('status')])->render() !!}
        </div>
    </div>
@endsection
