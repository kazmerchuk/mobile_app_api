@extends('layout.admin')

@section('right-panel')
    <div class="content-box-large">
        <div class="panel-heading">
            <div class="panel-title">{{ trans('default.admin.user.title') }}</div>
            <div  class="pull-right">
                <form action="" method="GET">
                        <input type="text"  name="search" value="{{ Input::get('query') }}" placeholder="{{ trans('default.admin.user.input_string') }}"/>
                        <input type="submit" value="{{ trans('default.admin.user.search_button') }}"/> 
                </form>
            </div>
        </div>
        <div class="panel-body">
            @if (count($users))
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <td>
                                {{ trans('validation.attributes.initials') }}
                            </td>
                            <td>
                                {{ trans('validation.attributes.email') }}
                            </td>
                            <td>
                                {{ trans('validation.attributes.nickname') }}
                            </td>
                            <td>
                                {{ trans('validation.attributes.actions') }}
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>
                                    {{ $user->firstname }} {{ $user->lastname }}
                                </td>
                                <td>
                                    {{ $user->email }}
                                </td>
                                <td>
                                    {{ $user->nickname }}
                                </td>
                                <td>
                                    <a class="btn btn-block" href="{{ route('user_view', $user->id) }}">{{ trans('validation.attributes.view') }}</a>
                                </td>
                            </tr>
                        @endforeach
                    <tbody>
                </table>
            @endif
            {!! $users->appends(['query' => Input::get('query')])->render() !!}
        </div>
    </div>
@endsection
