@extends('layout.admin')

@section('right-panel')
    <div class="content-box-large">
        <div class="panel-heading">
            <div class="panel-title">{{ trans('default.admin.user.view_title') }} {{ $user->firstname }} {{ $user->lastname }}</div>
        </div>
        <hr>
        <div class="panel-body" style="font-size: 15px;">
            <div>
                {{ trans('default.admin.user.nickname') }}: <b>{{ $user->nickname }} </b><br/>
                {{ trans('default.admin.user.firstname') }}: <b>{{ $user->firstname }}</b><br/>
                {{ trans('default.admin.user.lastname') }}: <b>{{ $user->lastname }}</b><br/>
                {{ trans('default.admin.user.birthdate') }}: <b>{{ $user->birthdate }}</b><br/>
                <hr/>
                {{ trans('default.admin.user.email') }}: <b>{{ $user->email }} </b><br/>
                {{ trans('default.admin.user.mobile') }}: <b>{{ $user->mobile }} </b><br/>
                <hr/>
                {{ trans('default.admin.user.address') }}: <b>{{ $user->address }} </b><br/>
                {{ trans('default.admin.user.zip') }}: <b>{{ $user->zip }} </b><br/>
                {{ trans('default.admin.user.country') }}: <b>{{ $user->countrycode }} </b><br/>
                <hr/>
                {{ trans('default.admin.user.upvotes') }}: <b>{{ $upvotes }} </b><br/>
                {{ trans('default.admin.user.downvotes') }}: <b>{{ $downvotes }}</b> <br/>
            </div>
            <div style="margin-top: 15px;">
                <a href="{{ route('user_view', $user->id) }}?mode=ratings" class="btn btn-info">{{ trans('default.admin.user.reviews') }}</a>
                <a href="{{ route('user_view', $user->id) }}?mode=locations" class="btn btn-info">{{ trans('default.admin.user.locations') }}</a>
                <a href="{{ route('user_block', $user->id) }}" class="btn btn-{{ $user->is_blocked ? 'success' : 'danger' }}">{{ $user->is_blocked ? trans('default.admin.user.unblock_user') : trans('default.admin.user.block_user') }}</a>
            </div>
            <div style="margin-top:15px;">
                @if (!is_null($reviews))
                    @if (count($reviews))
                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                            <tr>
                                <td><b>{{ trans('default.admin.user.review_text') }}</b></td>
                                <td><b>{{ trans('default.admin.user.drift_info') }}</b></td>
                            </tr>
                            @foreach ($reviews as $review)
                                <tr>
                                    <td>
                                        {{ $review->text }}
                                    </td>
                                    <td>
                                        {{ trans('default.admin.user.drift_date') }}: {{ !is_null($review->drift->start_date) ? $review->drift->start_date : '?' }} - {{ !is_null($review->drift->end_date) ? $review->drift->end_date : '?' }} <br/>
                                        {{ trans('default.admin.user.drift_with') }}: {{ $review->drift->driver->firstname }} {{ $review->drift->driver->lastname }}<br/>
                                        {{ trans('default.admin.user.drift_from') }}: {{ $review->drift->startcaption }}<br/>
                                        {{ trans('default.admin.user.drift_to') }}: {{ $review->drift->endcaption }}<br/>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {!! $reviews->appends(['mode' => 'ratings'])->render() !!}
                    @else
                        <span style="color:red">{{ trans('default.admin.user.no_review') }}</span>
                    @endif
                @elseif (!is_null($locations))
                    @if (count($locations))
                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                            <tr>
                                <td><b>{{ trans('default.admin.user.coordinates') }}</b></td>
                                <td><b>{{ trans('default.admin.user.timestamp') }}</b></td>
                            </tr>
                            @foreach ($locations as $location)
                                <tr>
                                    <td>
                                        {{ trans('default.admin.user.latitude') }}: {{ $location->lat }}, {{ trans('default.admin.user.longitude') }}: {{ $location->lon }}
                                        <a href="{{ route('show_map') }}?latitude={{ $location->lat }}&longitude={{ $location->lon }}" class='btn btn-info pull-right'>{{ trans('default.admin.user.show_on_map') }}</a>
                                    </td>
                                    <td>
                                        {{ $location->timestamp }}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {!! $locations->appends(['mode' => 'locations'])->render() !!}
                    @else
                        <span style="color:red">{{ trans('default.admin.user.no_locations') }}</span>
                    @endif
                @endif
            </div>
        </div>
    </div>
@endsection

