@extends('layout.admin')

@section('right-panel')
    <div class="content-box-large">
        <div class="panel-body" style="font-size: 15px;">
            <div class="col-lg-12" id="google-map" style="width:900px;height:600px;"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&key={{ env('google_map_api_key') }}"></script>
    <script type='text/javascript'>
        $(document).ready(function(){
            function initMap() {
                var latLng = {lat: {{ $latitude }}, lng: {{ $longitude }}};
                
                var map = new google.maps.Map(document.getElementById('google-map'), {
                  zoom: 4,
                  center: latLng
                });
                
                var marker = new google.maps.Marker({
                  position: latLng,
                  map: map,
                });
            }
            
            initMap();
        });
    </script>
@endsection

