<!DOCTYPE html>
<html>
    <head>
      <title>{{ trans('default.admin.logo_title') }}</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!-- Bootstrap -->
      <link rel="stylesheet" href="{{ URL::asset("bootstrap/css/bootstrap.min.css") }}" />
      <link rel="stylesheet" href="{{ URL::asset("css/styles.css") }}" />

      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
        @if (!Auth::guest())
        <div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-10">
	              <div class="logo">
	                 <h1><a href="{{ route('dashboard') }}">{{ trans('default.admin.logo_title') }}</a></h1>
	              </div>
	           </div>
	           <div class="col-md-2">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ trans('default.admin.my_account') }}<b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="{{ url('/admin/logout') }}">{{ trans('default.admin.logout') }}</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>
        @endif
        <div class="page-content">
            @if (!Auth::guest())
                <div class="row">
                    <div class="col-md-2">
                        <div class="sidebar content-box" style="display: block;">
                            <ul class="nav">
                                <li class="{{ Request::is('admin/content') ? 'current' : '' }}"><a href="{{ route('content_list') }}"> {{ trans('default.admin.content.title') }}</a></li>
                                <li class="{{ Request::is('admin/users') ? 'current' : '' }}"><a href="{{ route('user_list') }}"> {{ trans('default.admin.user.title') }}</a></li>
                                <li class="{{ Request::is('admin/journeys') ? 'current' : '' }}"><a href="{{ route('drift_list') }}"> {{ trans('default.admin.drift.title') }}</a></li>
                                <li class="{{ Request::is('admin/notifications') ? 'current' : '' }}"><a href="{{ route('notification_list') }}"> {{ trans('default.admin.notification.title') }}</a></li>
                                <li class="{{ Request::is('admin/statistics') ? 'current' : '' }}"><a href="{{ route('statistic_list') }}"> {{ trans('default.admin.statistic.title') }}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-10">
                        @yield('right-panel')
                    </div>
                </div>
            @endif
            @yield('content')
        </div>
        <script src="https://code.jquery.com/jquery.js"></script>
        <script src="{{ URL::asset('bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('js/custom.js') }}"></script>
        <script src="//cdn.ckeditor.com/4.6.0/standard/ckeditor.js"></script>
        <script type='text/javascript'>
            $(document).ready(function() {
                if ($('.content-body').length) {
                    CKEDITOR.replace($('.content-body').attr('id'));
                }
            });
        </script>
        @yield('scripts')
    </body>
</html>
