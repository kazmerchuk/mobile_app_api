<?php

namespace App\Service;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Support\Facades\Log;

class RouteService
{
    protected $scriptPath = null;
    protected $routeStorage = null;
    
    public function __construct() {
        $this->scriptPath = env('route_script_path');
        $this->routeStorage = storage_path('routes');
        
        if (!is_dir($this->routeStorage)) {
            mkdir($this->routeStorage);
        }
    }
    
    public function saveRoute($userId, $route)
    {   
        file_put_contents($this->routeStorage . '/' . $userId . '.json', json_encode($route));
        
        return $this->runProcess('i', $userId);
    }
    
    public function deleteRoute($userId)
    {
        $this->runProcess('d', $userId);
        unlink($this->routeStorage . '/' . $userId . '.json');
        
        return true;
    }
    
    public function cleanRoutes()
    {
        $files = array_diff(scandir($this->routeStorage), array('.', '..'));
        
        //last 24 hours
        $deleteTime = 60 * 60 * 24;
        foreach ($files as $file) {
            $fileCreateTime = filemtime($this->routeStorage . '/' . $file);
            
            if (time() - $fileCreateTime > $deleteTime) {
                $userId = explode('.', $file)[0];
                
                try {
                    $this->deleteRoute($userId);
                } catch (ProcessFailedException $e) {
                    Log::error($e->getMessage());
                    continue;
                }
            }
        }
        
        return true;
    }
    
    public function getRoutes($userId)
    {
        try {
            $result = $this->runProcess('r', $userId);
        } catch (ProcessFailedException $e) {
            Log::error($e->getMessage());
            return [];
        }
        
        return json_decode($result, true);
    }
    
    protected function runProcess($argument, $userId) 
    {
        $process = new Process(sprintf('python %s -%s %s', $this->scriptPath, $argument, $userId));
        $process->run();

        if (!$process->isSuccessful() || $process->getExitCode() != 0) {
            $e = new ProcessFailedException($process);
            Log::error("Error running route script: ".$e->getMessage());
            throw $e;
        }
        
        return $process->getOutput();
    }
}
