<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photos extends Model
{   
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'photos';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'drift_id', 
        'timestamp',
        'lat',
        'lon',
        'url',
        'caption',
    ];
    
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'timestamp',
        'lat',
        'lon',
        'caption',
        'url',
    ];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    public function drift()
    {
        return $this->belongsTo('App\Models\Drift');
    }
}
