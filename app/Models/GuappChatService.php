<?php
namespace App\Models;

use SebastianBergmann\CodeCoverage\InvalidArgumentException;

class GuappChatService {

    protected $baseUrl;
    protected $clientUrl;
    protected $communityName;
    protected $communitySecret;

    public function __construct()
    {
        $this->baseUrl = config('guapp.chat.baseurl');
        $this->clientUrl = config('guapp.chat.clienturl');
        $this->communityName = config('guapp.chat.communityname');
        $this->communitySecret = config('guapp.chat.communitysecret');
    }

    public static function make()
    {
        return new static();
    }

    public function renderClient() {
        echo '<iframe class="guappchat" style="width:100%; height:100%;" src="'.$this->getClientUrl().'"></iframe>';
    }

    protected function getSessionToken() {
        return session()->get('guappSessionToken', null);
    }

    protected function setSessionToken($token) {
        session()->put('guappSessionToken', $token);
    }

    public function getClientUrl() {
        $sessionToken = $this->getSessionToken();
        return $this->clientUrl.'?token='.$sessionToken.'&community_name='.$this->communityName;
    }

    public function getClientUrlChatWithEmail($email) {
        $sessionToken = $this->getSessionToken();
        $url = $this->clientUrl.'?token='.$sessionToken.'&community_name='.$this->communityName;

        $otherUserGuappId = $this->getUserIdByEmail($email);
        if($otherUserGuappId) {
            $url = $url . '&chatwith_user_id=' . $otherUserGuappId;
        }

        return $url;
    }

    public function login($email, $nickname) {

        // Authorize current session, obtain associated user data and initiate Guapp login

        $deviceToken = session()->getId();

        // call Guapp service login()
        $url = $this->baseUrl.'/login';
        $request = array(
            'communityname' => $this->communityName,
            'communitysecret' => $this->communitySecret,
            'devicetoken' => $deviceToken,
            'email' => $email,
            'nickname' => $nickname
        );
        $jsonRequest = json_encode($request, JSON_FORCE_OBJECT);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonRequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonRequest))
        );

        $jsonResponse = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($responseCode != 200) return;

        $response = json_decode($jsonResponse);
        $sessionToken = $response->{'sessiontoken'};
        $this->setSessionToken($sessionToken);
    }

    // This method will logoff the Guapp session
    public function logout() {
        $sessionToken = $this->getSessionToken();

        // call Guapp service
        $url = $this->baseUrl.'/logout';
        $request = array(
            'sessiontoken' => $sessionToken
        );
        $jsonRequest = json_encode($request, JSON_FORCE_OBJECT);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonRequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonRequest))
        );

        $jsonResponse = curl_exec($ch);
        curl_close($ch);

        $this->setSessionToken('');
    }

    public function setEmail($email) {
        if (empty($email)) return;

        $sessionToken = $this->getSessionToken();

        // call Guapp service
        $url = $this->baseUrl.'/users/me/email';
        $request = array(
            'sessiontoken' => $sessionToken,
            'email' => $email
        );
        $jsonRequest = json_encode($request, JSON_FORCE_OBJECT);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonRequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonRequest))
        );

        $jsonResponse = curl_exec($ch);
        curl_close($ch);
    }

    public function setNickname($nickname) {
        if (empty($nickname)) return;

        $sessionToken = $this->getSessionToken();

        // call Guapp service logout()
        $url = $this->baseUrl.'/users/me/nickname';
        $request = array(
            'sessiontoken' => $sessionToken,
            'nickname' => $nickname
        );
        $jsonRequest = json_encode($request, JSON_FORCE_OBJECT);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonRequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonRequest))
        );

        $jsonResponse = curl_exec($ch);
        curl_close($ch);
    }

    public function setDisplayName($displayName) {
        if (empty($displayName)) return;

        $sessionToken = $this->getSessionToken();

        // call Guapp service logout()
        $url = $this->baseUrl.'/users/me/displayname';
        $request = array(
            'sessiontoken' => $sessionToken,
            'displayname' => $displayName
        );
        $jsonRequest = json_encode($request, JSON_FORCE_OBJECT);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonRequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonRequest))
        );

        $jsonResponse = curl_exec($ch);
        curl_close($ch);
    }

    public function setProfilePicture($profilePictureUrl) {
        if (empty($profilePictureUrl)) return;

        $sessionToken = $this->getSessionToken();

        // call Guapp service logout()
        $url = $this->baseUrl.'/users/me/profilepicture';
        $request = array(
            'sessiontoken' => $sessionToken,
            'profilepictureurl' => $profilePictureUrl
        );
        $jsonRequest = json_encode($request, JSON_FORCE_OBJECT);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonRequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonRequest))
        );

        $jsonResponse = curl_exec($ch);
        curl_close($ch);
    }

    public function addFriendByNickname($friendNickname) {
        if (empty($friendNickname)) return;

        $sessionToken = $this->getSessionToken();

        // call Guapp service
        $url = $this->baseUrl.'/users/me/friends/add';
        $request = array(
            'communityname' => $this->communityName,
            'communitysecret' => $this->communitySecret,
            'sessiontoken' => $sessionToken,
            'nickname' => $friendNickname
        );
        $jsonRequest = json_encode($request, JSON_FORCE_OBJECT);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonRequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonRequest))
        );

        $jsonResponse = curl_exec($ch);
        curl_close($ch);
    }

    public function removeFriendByNickname($friendNickname) {
        if (empty($friendNickname)) return;

        $sessionToken = $this->getSessionToken();

        // call Guapp service
        $url = $this->baseUrl.'/users/me/friends/remove';
        $request = array(
            'communityname' => $this->communityName,
            'communitysecret' => $this->communitySecret,
            'sessiontoken' => $sessionToken,
            'nickname' => $friendNickname
        );
        $jsonRequest = json_encode($request, JSON_FORCE_OBJECT);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonRequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonRequest))
        );

        $jsonResponse = curl_exec($ch);
        curl_close($ch);
    }

    public function addFriendByEmail($friendEmail) {
        if (empty($friendEmail)) return;

        $sessionToken = $this->getSessionToken();

        // call Guapp service
        $url = $this->baseUrl.'/users/me/friends/add';
        $request = array(
            'communityname' => $this->communityName,
            'communitysecret' => $this->communitySecret,
            'sessiontoken' => $sessionToken,
            'email' => $friendEmail
        );
        $jsonRequest = json_encode($request, JSON_FORCE_OBJECT);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonRequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonRequest))
        );

        $jsonResponse = curl_exec($ch);
        curl_close($ch);
    }

    public function removeFriendByEmail($friendEmail) {
        if (empty($friendEmail)) return;

        $sessionToken = $this->getSessionToken();

        // call Guapp service
        $url = $this->baseUrl.'/users/me/friends/remove';
        $request = array(
            'communityname' => $this->communityName,
            'communitysecret' => $this->communitySecret,
            'sessiontoken' => $sessionToken,
            'email' => $friendEmail
        );
        $jsonRequest = json_encode($request, JSON_FORCE_OBJECT);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonRequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonRequest))
        );

        $jsonResponse = curl_exec($ch);
        curl_close($ch);
    }

    public function sendTextMessage($recipientEmail, $recipientNickname, $message) {
        if (empty($message)) return;

        $sessionToken = $this->getSessionToken();

        if ($recipientEmail) {
            $userspec = "email=".$recipientEmail;
        }
        elseif ($recipientNickname) {
            $userspec = "nickname=".$recipientNickname;
        }
        else {
            throw new InvalidArgumentException("either recipient email or nickname must be given");
        }

        // call Guapp service
        $url = $this->baseUrl.'/users/'.$userspec.'/messages/send';
        $request = array(
            'sessiontoken' => $sessionToken,
            'message' => $message
        );
        $jsonRequest = json_encode($request, JSON_FORCE_OBJECT);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonRequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonRequest))
        );

        $jsonResponse = curl_exec($ch);
        curl_close($ch);
    }

    private function getUserIdByEmail($email) {
        if (empty($email)) return;

        $userSpec = 'email=' . $email;

        // call Guapp service
        $url = $this->baseUrl.'/users/' . $userSpec . '?communityname=' . $this->communityName . '&communitysecret=' . $this->communitySecret;

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json')
        );

        $jsonResponse = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($responseCode != 200) return;

        $response = json_decode($jsonResponse);
        $id = $response->{'id'};
        return $id;
    }


}