<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicles extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vehicles';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'platenumber', 
        'carmodel',
        'pictureurl',
    ];
    
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'platenumber',
        'carmodel',
        'pictureurl',
    ];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
