<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceToken extends Model
{   
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'devicetoken';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'client',
        'token',
    ];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
