<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Drifts extends Model
{
    const STATUS_AGREED = 0;
    const STATUS_RUNNING = 1;
    const STATUS_COMPLETED = 2;
    const STATUS_CANCELLED = 3;
    
    public static $textStatuses = array(
        self::STATUS_AGREED => 'agreed',
        self::STATUS_RUNNING => 'running',
        self::STATUS_COMPLETED => 'completed',
        self::STATUS_CANCELLED => 'cancelled',
    );
    
    public static $statusConstants = array(
        'agreed' => self::STATUS_AGREED,
        'running' => self::STATUS_RUNNING,
        'completed' => self::STATUS_COMPLETED,
        'cancelled' => self::STATUS_CANCELLED,
    );
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'drifts';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'driver_id',
        'drifter_id',
        'status',
        'startlat',
        'startlon',
        'startcaption',
        'endlat',
        'endlon',
        'endcaption',
    ];
    
    public function driver()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    public function drifter()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    public function getStatus()
    {
        return trans('default.status_' . self::$textStatuses[$this->status]);
    }
}
