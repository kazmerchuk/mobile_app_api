<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Companions extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'companions';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 
        'companion_id',
    ];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    public function companion()
    {
        return $this->belongsTo('App\Models\User');
    }
}
