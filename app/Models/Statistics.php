<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Statistics extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'statistics';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'new_registration_count', 
        'proposed_drift_count',
        'agreed_drift_count',
        'rating_number_count',
        'date',
    ];
    
    public $timestamps = false;
    
    public function getActiveUsersAttribute()
    {
        $dateFrom = Carbon::createFromTimestamp(strtotime($this->date . ' 00:00:00'));
        $dateUntil =Carbon::createFromTimestamp(strtotime($this->date . ' 23:59:59'));
        
        $users = UserLocations::select()
            ->whereBetween('timestamp', [$dateFrom, $dateUntil])
            ->groupBy('user_id')
            ->get();
        
        return count($users);
    }
}
