<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLocations extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'userlocations';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'lon',
        'lat',
        'timestamp',
    ];
    
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'lat',
        'lon',
    ];
    
    public $timestamps = false;
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
