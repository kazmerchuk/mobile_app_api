<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DriftPoint extends Model
{
    protected $table = 'driftpoints';
}
