<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    const REVIEW_VOTE_DOWN = 0;
    const REVIEW_VOTE_UP = 1;
    
    public static $reviewTextVotes = array(
        self::REVIEW_VOTE_DOWN => 'down',
        self::REVIEW_VOTE_UP => 'up'
    );
    
    public static $textVotes = array(
        'down' => self::REVIEW_VOTE_DOWN,
        'up' => self::REVIEW_VOTE_UP
    );
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reviews';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'drift_id', 
        'reviewby',
        'updown',
        'text',
    ];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    public function drift()
    {
        return $this->belongsTo('App\Models\Drifts');
    }
}
