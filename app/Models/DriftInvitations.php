<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DriftInvitations extends Model
{
    const STATUS_OPEN = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_REJECTED = 2;
    
    public static $textStatuses = array(
        self::STATUS_OPEN => 'open',
        self::STATUS_ACCEPTED => 'accepted',
        self::STATUS_REJECTED => 'rejected',
    );
    
    public static $statusConstants = array(
        'open' => self::STATUS_OPEN,
        'assepted' => self::STATUS_ACCEPTED,
        'rejected' => self::STATUS_REJECTED,
    );
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'driftinvitations';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'inviteduser_id', 
        'invitinguser_id',
        'status',
        'invitationdate',
        'responsedate'
    ];
    
    public function inviteduser()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    public function invitinguser()
    {
        return $this->belongsTo('App\Models\User');
    }
}
