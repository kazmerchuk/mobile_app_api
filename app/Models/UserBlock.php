<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBlock extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'userblock';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 
        'blockeduser_id',
    ];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    public function blockeduser()
    {
        return $this->belongsTo('App\Models\User');
    }
}
