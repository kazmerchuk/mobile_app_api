<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;
use App\Notifications\CustomResetPassword;

class User extends Authenticatable
{
    const GENDER_NONE = 0;
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;
    
    const MODE_DRIFTER = 0;
    const MODE_DRIVER = 1;
    
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email',
        'password',
        'gender',
        'firstname',
        'lastname',
        'nickname',
        'email',
        'mobile',
        'address',
        'zip',
        'countrycode',
        'birthdate',
        'hasdriverslicense',
        'password',
        'invisiblemode',
        'companionmode',
        'city',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'passwordhash',
        'remember_token',
        'is_admin'
    ];
    
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'gender',
        'firstname',
        'lastname',
        'nickname',
        'email',
        'mobile',
        'address',
        'zip',
        'countrycode',
        'birthdate',
        'hasdriverslicense',
        'hasvehicle',
        'about',
        'profilepictureurl',
        'invisiblemode',
        'companionmode',
        'destinationlat',
        'destinationlon',
        'destinationcaption',
        'mode',
        'city',
    ];
    
    public function companions()
    {
        return $this->hasMany('App\Models\Companions');
    }
    
    public function getGenderAttribute()
    {
        switch($this->attributes['gender']) {
            case self::GENDER_NONE:
                $gender = trans('default.gender_none');
                break;
            case self::GENDER_MALE:
                $gender = trans('default.gender_male');
                break;
            case self::GENDER_FEMALE:
                $gender = trans('default.gender_female');
                break;
        }
        
        return $gender;
    }
    
    public function setGenderAttribute($value)
    {
        switch($value) {
            case 'none':
                $gender = self::GENDER_NONE;
                break;
            case 'male':
                $gender = self::GENDER_MALE;
                break;
            case 'female':
                $gender = self::GENDER_FEMALE;
                break;
            default:
                $gender = self::GENDER_NONE;
        }
        
        $this->attributes['gender'] = $gender;
    }
    
    public function setPasswordAttribute($value)
    {
        $this->attributes['passwordhash'] = sha1($value);
    }
    
    public function setBirthdateAttribute($value)
    {
        $this->attributes['birthdate'] = Carbon::parse($value)->format('Y-m-d');
    }
    
    public function setModeAttribute($value)
    {
        $mode = null;
        switch($value) {
            case 'driver':
                $mode = self::MODE_DRIVER;
                break;
            case 'drifter':
                $mode = self::MODE_DRIFTER;
                break;
        }
        
        $this->attributes['mode'] = $mode;
    }
    
    public function getModeAttribute()
    {
        $mode = null;
        switch($this->attributes['mode']) {
            case 0:
                $mode = trans('default.mode_drifter');
                break;
            case 1:
                $mode = trans('default.mode_driver');
                break;
        }
        
        return $mode;
    }
    
    public function getModeKeyAttribute()
    {
        return $this->attributes['mode'];
    }
    
    public function getDisplayNameAttribute()
    {
        return !is_null($this->attributes['nickname']) ? $this->attributes['nickname'] : $this->attributes['firstname'];
    }

    public function userSessions()
    {
        return $this->hasMany('App\Models\UserSession');
    }
    
    public function token()
    {
        return $this->hasMany('App\Models\DeviceToken');
    }
    
    public function vehicle()
    {
        return $this->hasOne('App\Models\Vehicles');
    }
    
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomResetPassword($token));
    }
}
