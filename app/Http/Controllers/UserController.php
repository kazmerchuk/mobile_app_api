<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use Validator;
use DB;
use Carbon\Carbon;
use Storage;
use Symfony\Component\Process\Exception\ProcessFailedException;

use App\Service\RouteService;

use App\Models\Vehicles;
use App\Models\User;
use App\Models\Reviews;
use App\Models\UserLocations;
use App\Models\Drifts;
use App\Models\Photos;
use App\Models\Companions;
use App\Models\UserBlock;
use App\Models\DeviceToken;
use App\Models\CompanionRequests;
use App\Models\GuappChatService;
use App\Models\DriftInvitations;

class UserController extends Controller
{
    protected $routeService = null;
    
    public function __construct() {
        $this->routeService = new RouteService();
        
        parent::__construct();
    }
    
    public function updateVehicles(Request $request)
    {
        $data = $request->all();
        $validator = $this->validateVehicleArguments($data);
        
        if ($validator->fails())
        {
            return $this->processValidatorErrors($validator->errors());
        }
        
        $vehicle = Vehicles::where('user_id', '=', $this->user->id)->first();
        if (is_null($vehicle)) {
            $vehicle = Vehicles::create([
                'platenumber' => $data['platenumber'],
                'carmodel' => $data['carmodel'],
                'user_id' => $this->user->id
            ]);
        } else {
            $vehicle->carmodel = $data['carmodel'];
            $vehicle->platenumber = $data['platenumber'];
        }
        
        if (isset($data['picture'])) {
            $ext = $this->getExt($data['picture']);
            $imageName = sha1($this->user->id . time()) . '.' . $ext;
            $image = base64_decode(explode(',', $data['picture'])[1]);
            
            Storage::disk('car_pictures')->put($imageName, $image);
            
            $vehicle->pictureurl = 'cars/' . $imageName;
        }
        $this->user->hasvehicle = true;
        
        $this->user->save();
        $vehicle->save();
        
        return $this->returnSuccess();
    }
    
    public function updateProfilePicture(Request $request) {
        $picture = $request->get('picture');

        if (!is_null($picture)) {
            $ext = $this->getExt($picture);
            $imageName = sha1($this->user->id . time()) . '.' . $ext;
            $image = base64_decode(explode(',', $picture)[1]);
            
            Storage::disk('profile_pictures')->put($imageName, $image);
            
            $this->user->profilepictureurl = 'profile/' . $imageName;
            $this->user->save();

            // notify Guapp chat on new profile picture
            $chatService = GuappChatService::make();
            $chatService->login($this->user->email, null);
            $chatService->setProfilePicture(!empty($this->user->profilepictureurl) ? asset(Storage::disk('profile_pictures')->url($this->user->profilepictureurl)) : null);

            return $this->returnSuccess();
        }
        
        return $this->returnError();
    }
    
    public function hasNoneVehicle()
    {
        $this->user->hasvehicle = false;
        $this->user->save();
        
        return $this->returnSuccess();
    }
    
    public function updateAbout(Request $request)
    {
        $about = $request->get('text');
        
        if (strlen($about) > 180) {
            return $this->returnError(['message' => trans('default.about_is_long')]);
        }
        
        $this->user->about = $about;
        $this->user->save();
        
        return $this->returnSuccess();
    }
    
    public function getProfileInfo()
    {
        $userData = $this->user->toArray();
        
        if ($userData['hasvehicle'] == true) {
            $vehicle = Vehicles::where('user_id', '=', $this->user->id)->get()->last();    
            if (!is_null($vehicle)) {
                $userData['vehicle'] = $vehicle->toArray();
            
                if (!is_null($vehicle->pictureurl)) {
                    $userData['vehicle']['pictureurl'] = asset(Storage::disk('car_pictures')->url($vehicle->pictureurl));
                }
            }
        }
        
        if (!is_null($userData['profilepictureurl'])) {
            $userData['pictureurl'] = asset(Storage::disk('profile_pictures')->url($userData['profilepictureurl']));
            unset($userData['profilepictureurl']);
        }
        
        $upvotes = DB::table('reviews')
            ->where('user_id', '=', $this->user->id)
            ->where('updown', '=', Reviews::REVIEW_VOTE_UP)
            ->count();
        $downvotes = DB::table('reviews')
            ->where('user_id', '=', $this->user->id)
            ->where('updown', '=', Reviews::REVIEW_VOTE_DOWN)
            ->count();
        
        $userData['upvotes'] = $upvotes;
        $userData['downvotes'] = $downvotes;
        $userData['companions'] = $this->user->companions()->count();
        
        return $this->returnSuccess($userData);
    }
    
    public function updateProfileInfo(Request $request)
    {
        $data = $request->all();
        $validator = $this->validateProfileArguments($data);
        
        if ($validator->fails())
        {
            return $this->processValidatorErrors($validator->errors());
        }

        $oldEmail = $this->user->email;
        
        $this->user->update($data);

        // report update to Guapp chat
        $chatService = GuappChatService::make();
        $chatService->login($oldEmail, null); // this will automatically create the user
        $chatService->setEmail($this->user->email);
        $chatService->setDisplayName($this->user->displayname);

        return $this->returnSuccess();
    }
    
    public function changePassword(Request $request)
    {
        $data = $request->all();
        $validator = $this->validatePasswordArguments($data);
        
        if ($validator->fails())
        {
            return $this->processValidatorErrors($validator->errors());
        }
        
        if ($this->user->passwordhash != sha1($data['oldpassword'])) {
            return $this->returnError(['message' => trans('default.wrong_old_password')]);
        }
        
        $this->user->passwordhash = sha1($data['newpassword']);
        $this->user->save();
        
        return $this->returnSuccess();
    }
    
    public function getUser($id)
    {
        $user = User::where('id', '=', $id)->first();
        if (is_null($user)) {
            return $this->returnNotFound(['message' => trans('default.user_not_found')]);
        }
        $vehicle = Vehicles::where('user_id', '=', $user->id)->get()->last();
        $companion = Companions::where(function($query) use ($user) {
            $query
                ->where('user_id', '=', $this->user->id)
                ->where('companion_id', '=', $user->id);
        })
        ->orWhere(function($query) use ($user) {
            $query
                ->where('user_id', '=', $user->id)
                ->where('companion_id', '=', $this->user->id);
        })->get();
        
        $isCompanion = count($companion) ? true : false;
        
        $userData = [];
        $userData['id'] = $id;
        $userData['gender'] = $user->gender;
        $userData['nickname'] = $user->displayname;
        $userData['mode'] = $user->mode;
        $userData['about'] = $user->about;
        $userData['pictureurl'] = asset(Storage::disk('profile_pictures')->url($user->profilepictureurl));
        if (!is_null($vehicle)) {
            $vehicleData = $vehicle->toArray();
            $vehicleData['pictureurl'] = asset(Storage::disk('car_pictures')->url($vehicleData['pictureurl']));

            $userData['vehicle'] = $vehicleData;
        }
        
        $upvotes = DB::table('reviews')
            ->where('user_id', '=', $user->id)
            ->where('updown', '=', Reviews::REVIEW_VOTE_UP)
            ->count();
        $downvotes = DB::table('reviews')
            ->where('user_id', '=', $user->id)
            ->where('updown', '=', Reviews::REVIEW_VOTE_DOWN)
            ->count();
        
        $userData['upvotes'] = $upvotes;
        $userData['downvotes'] = $downvotes;
        $userData['companions'] = $user->companions()->count();
        $userData['isCompanion'] = $isCompanion;
        
        if ($user->invisiblemode == false && (!$isCompanion || $user->companionmode == true)) {
             $lastLocation = UserLocations::where('user_id', '=', $user->id)->get()->last();
            $userData['lat'] = $lastLocation['lat'];
            $userData['lon'] = $lastLocation['lon'];
        }
        
        return $this->returnSuccess($userData);
    }
    
    public function getLastLocation($id)
    {
        $user = User::where('id', '=', $id)->first();
        if (is_null($user)) {
            return $this->returnNotFound(['message' => trans('default.user_not_found')]);
        }
        
        if ($user->invisiblemode == true) {
            return $this->returnForbidden(['message' => trans('default.user_invisible')]);
        }
        
        $lastLocation = UserLocations::where('user_id', '=', $user->id)->get()->last();
        $userData = ['user_id' => $user->id];
        
        if (!is_null($lastLocation)) {
            return $this->returnSuccess($lastLocation->toArray() + $userData);
        }
        
        return $this->returnSuccess($userData);
    }
    
    public function updateUserMode(Request $request)
    {
        $mode = $request->get('mode');
        
        if (!in_array($mode, ['driver', 'drifter'])) {
            return $this->returnError(['message' => trans('default.wrong_mode')]);
        }
        
        if ($mode == 'driver') {
            $vehicle = Vehicles::where('user_id', '=', $this->user->id)->get()->last();
            
            if (is_null($vehicle)) {
                return $this->returnError(['message' => trans('default.no_vehicle_for_driver')]);
            }
        }
        
        $this->user->mode = $mode;
        $this->user->save();
        
        return $this->returnSuccess();
    }
    
    public function updateUserCompanionMode(Request $request) 
    {
        $mode = $request->get('value');
        
        if (!is_bool($mode)) {
            return $this->returnError(['message' => trans('validation.attributes.must_be_boolean')]);
        }
        
        $this->user->companionmode = $mode;
        $this->user->save();
        
        return $this->returnSuccess();
    }
    
    public function updateUserVisibilityMode(Request $request)
    {
        $mode = $request->get('value');
        
        if (!is_bool($mode)) {
            return $this->returnError(['message' => trans('validation.attributes.must_be_boolean')]);
        }
        
        $this->user->invisiblemode = $mode;
        $this->user->save();
        
        return $this->returnSuccess();
    }
    
    public function getCompanionList(Request $request, $id)
    {
        $data = $request->all();
        $itemCount = isset($data['itemcount']) ? $data['itemcount'] : 10;
        
        $user = User::where('id', '=', $id)->first();
        if (is_null($user)) {
            return $this->returnNotFound(['message' => trans('default.user_not_found')]);
        }
        
        $companionsQuery = DB::table('companions')
            ->join('users', 'companions.companion_id', '=', 'users.id')
            ->where('companions.user_id', '=', $id);
        
        if (isset($data['pagenumber'])) {
            $companionsQuery->skip($data['pagenumber'] * $itemCount);
        }
        
        $companionsQuery->take($itemCount);
        $companions = $companionsQuery->get();
        
        if (count($companions)) {
            $companionList = [];
            foreach ($companions as $companion) {
		$photoUrl = asset(Storage::disk('profile_pictures')->url($companion->profilepictureurl));
                
		$companionList[] = [
                    'user_id' => $companion->companion_id,
                    'nickname' => !is_null($companion->nickname) ? $companion->nickname : $companion->firstname,
                    'pictureurl' => $photoUrl
                ];
            }
            
            return $this->returnSuccess(['companions' => $companionList]);
        }
        
        return $this->returnNotFound(['message' => trans('default.companions_not_found')]);
    }
    
    public function getReviewList(Request $request, $id)
    {
        $data = $request->all();
        $itemCount = isset($data['itemcount']) ? $data['itemcount'] : 10;
        
        $user = User::where('id', '=', $id)->first();
        if (is_null($user)) {
            return $this->returnNotFound(['message' => trans('default.user_not_found')]);
        }
        
        $reviewsQuery = DB::table('reviews')
            ->where('reviews.user_id', '=', $id);
        
        if (isset($data['pagenumber'])) {
            $reviewsQuery->skip($data['pagenumber'] * $itemCount);
        }
        
        $reviewsQuery->take($itemCount);
        $reviews = $reviewsQuery->get();
        
        $reviewList = [];
        if (count($reviews)) {
            foreach ($reviews as $review) {
                $reviewList[] = [
                    'datetime' => $review->created_at,
                    'drift_id' => $review->drift_id,
                    'reviewby' => $review->reviewby,
                    'updown' => Reviews::$reviewTextVotes[$review->updown],
                    'text' => $review->text,
                ];
            }
        }
            
        return $this->returnSuccess(['reviews' => $reviewList]);
    }
    
    public function getDriftList(Request $request, $id)
    {
        $data = $request->all();
        $itemCount = isset($data['itemcount']) ? $data['itemcount'] : 10;
        
        $user = User::where('id', '=', $id)->first();
        if (is_null($user)) {
            return $this->returnNotFound(['message' => trans('default.user_not_found')]);
        }
        
        $statuses = array_values(Drifts::$statusConstants);
        if (isset($data['statuses'])) {
            $requestStatuses = array_map(function($item) { return trim($item); }, explode(',', $data['statuses']));
            
            $statuses = [];
            foreach ($requestStatuses as $status) {
                if (isset(Drifts::$statusConstants[$status])) {
                    $statuses[] = Drifts::$statusConstants[$status];
                }
            }
        }
        
        $driftsQuery = DB::table('drifts')
            ->where(function ($query) use ($id){
                $query
                    ->where('drifts.drifter_id', '=', $id)
                    ->orWhere('drifts.driver_id', '=', $id);
            })
            ->whereIn('drifts.status', $statuses);
        
        if (isset($data['date-until'])) {
            $driftsQuery->where('drifts.startdate', '<', $data['date-until']);
        }
        
        $driftsQuery->take($itemCount);
        $drifts = $driftsQuery->get();
        
        if (count($drifts)) {
            $driftList = [];
            foreach ($drifts as $drift) {
                $coverDriftPhoto = Photos::where('drift_id', '=', $drift->id)->first();
                
                $driftData = [
                    'drift_id' => $drift->id,
                    'drifter_id' => $drift->drifter_id,
                    'driver_id' => $drift->driver_id,
                    'status' => Drifts::$textStatuses[$drift->status],
                    'created_at' => $drift->created_at,
                    'start_date' => $drift->startdate,
                    'end_date' => $drift->enddate,
                    'startlon' => $drift->startlon,
                    'startlat' => $drift->startlat,
                    'startcaption' => $drift->startcaption,
                    'endlon' => $drift->endlon,
                    'endlat' => $drift->endlat,
                    'endcaption' => $drift->endcaption,
                ];
                
                if ($coverDriftPhoto) {
                    $driftData['coverpictureurl'] = asset(Storage::disk('drift_pictures')->url($coverDriftPhoto->url));
                }
                
                $driftList[] = $driftData;
            }
            
            return $this->returnSuccess(['drifts' => $driftList]);
        }
        
        return $this->returnNotFound(['message' => trans('default.drifts_not_found')]); 
    }
    
    public function getPhotoList(Request $request, $id)
    {
        $data = $request->all();
        
        $user = User::where('id', '=', $id)->first();
        if (is_null($user)) {
            return $this->returnNotFound(['message' => trans('default.user_not_found')]);
        }
        
        $itemCount = isset($data['itemcount']) ? $data['itemcount'] : 10;
        $page = isset($data['pagenumber']) ? $data['pagenumber'] : 0;
        
        $photos = Photos::where('user_id', '=', $user->id)
            ->limit($itemCount)
            ->offset($itemCount * $page)
            ->get()
            ->sortBy('timestamp');
        
        $photoList = ['user_id' => $user->id, 'photos' => []];
        foreach ($photos as $photo) {
            $photoData = $photo->toArray();
            $photoData['url'] = asset(Storage::disk('drift_pictures')->url($photoData['url']));
            
            $photoList['photos'][] = $photoData;
        }
        
        return $this->returnSuccess($photoList);
    }
    
    public function getDriftInvitationList(Request $request)
    {
        $data = $request->all();
        $itemCount = isset($data['itemcount']) ? $data['itemcount'] : 10;
        
        $statuses = array_values(Drifts::$statusConstants);
        if (isset($data['statuses'])) {
            $requestStatuses = array_map(function($item) { return trim($item); }, explode(',', $data['statuses']));
            
            $statuses = [];
            foreach ($requestStatuses as $status) {
                if (isset(Drifts::$statusConstants[$status])) {
                    $statuses[] = Drifts::$statusConstants[$status];
                }
            }
        }
        
        $invitesQuery = DB::table('driftinvitations')
            ->where(function ($query) {
                $query->where('driftinvitations.inviteduser_id', $this->user->id)
                    ->orWhere('driftinvitations.invitinguser_id', $this->user->id);
            })
            ->whereIn('driftinvitations.status', $statuses);
        
        if (isset($data['dateuntil'])) {
            $invitesQuery->where('driftinvitations.invitationdate', '<', $data['dateuntil']);
        }
        
        $invitesQuery->take($itemCount);
        $invites = $invitesQuery->get();
        
        if (count($invites)) {
            $inviteList = [];
            foreach ($invites as $invite) {
                $inviteList[] = [
                    'id' => $invite->id,
                    'inviteduser_id' => $invite->inviteduser_id,
                    'invitinguser_id' => $invite->invitinguser_id,
                    'invitationdate' => $invite->invitationdate,
                    'status' => DriftInvitations::$textStatuses[$invite->status],
                ];
            }

            return $this->returnSuccess(['driftinvitations' => $inviteList]);
        }
        
        return $this->returnNotFound(['message' => trans('default.invites_not_found')]);
    }
    
    public function addNewLocation(Request $request)
    {
        $data = $request->all();
        $validator = $this->validateLocationArguments($data);
        
        if ($validator->fails())
        {
            return $this->processValidatorErrors($validator->errors());
        }
        
        UserLocations::firstOrCreate([
            'user_id' => $this->user->id,
            'lat' => $data['lat'],
            'lon' => $data['lon'],
            'timestamp' => (new \DateTime())->format('Y-m-d H:i:s')
        ]);
        
        return $this->returnSuccess();
    }
    
    public function updateDestination(Request $request)
    {
        $data = $request->all();
        $validator = $this->validateDestinationArguments($data);
        
        if ($validator->fails())
        {
            return $this->processValidatorErrors($validator->errors());
        }

        User::where('id', '=', $this->user->id)
            ->update([
                'destinationlat' => $data['lat'],
                'destinationlon' => $data['lon'],
                'destinationcaption' => $data['caption'],
            ]);
        
        return $this->returnSuccess();
    }
    
    public function removeCompanion($id)
    {
        $companion = Companions::where('companions.user_id', '=', $this->user->id)
            ->where('companions.companion_id', '=', $id)
            ->first();
        
        if (is_null($companion)) {
            return $this->returnError(['message' => trans('default.companion_not_found')]);
        }
        
        Companions::where('companions.user_id', '=', $this->user->id)
            ->where('companions.companion_id', '=', $id)
            ->delete();
        
        Companions::where('companions.companion_id', '=', $this->user->id)
            ->where('companions.user_id', '=', $id)
            ->delete();
        
        return $this->returnSuccess();
    }
    
    public function blockCompanion($id)
    {
        $companion = User::where('id', '=', $id)->first();
        
        if (is_null($companion)) {
            return $this->returnError(['message' => trans('default.user_not_found')]);
        }
        
        UserBlock::create([
            'user_id' => $this->user->id,
            'blockeduser_id' => $companion->id,
        ]);

        // remove companionship in Guapp chat
        $chatService = GuappChatService::make();
        $chatService->login($this->user->email, null);
        $chatService->removeFriendByEmail($companion->email);

        return $this->returnSuccess();
    }
    
    public function unblockCompanion($id)
    {
        $companion = User::where('id', '=', $id)->first();
        
        if (is_null($companion)) {
            return $this->returnError(['message' => trans('default.user_not_found')]);
        }
        
        UserBlock::where([
            'user_id' => $this->user->id,
            'blockeduser_id' => $companion->id,
        ])->delete();
        
        return $this->returnSuccess();
    }
    
    public function updateDeviceToken(Request $request)
    {
        $data = $request->all();
        $validator = $this->validateDeviceTokenArguments($data);
        
        if ($validator->fails())
        {
            return $this->processValidatorErrors($validator->errors());
        }
        
        $device = DeviceToken::where([
            'user_id' => $this->user->id
        ])->first();
        
        if (is_null($device)) {
            $device = DeviceToken::create([
                'user_id' => $this->user->id,
                'client' => $data['client'],
                'token' => $data['devicetoken']
            ]);
        } else {
            DeviceToken::where('user_id', '=', $this->user->id)
                ->update([
                    'client' => $data['client'],
                    'token' => $data['devicetoken']
                ]);
        }
        
        return $this->returnSuccess();
    }
    
    public function requestCompanion($id)
    {
        $user = User::where('id', '=', $id)->first();
        
        if (is_null($user)) {
            return $this->returnError(['message' => trans('default.user_not_found')]);
        }
        
        $alreadyInvite = CompanionRequests::where('inviteduser_id', '=', $this->user->id)
            ->where('invitinguser_id', '=', $id)
            ->where('status', '=', CompanionRequests::STATUS_OPEN)
            ->first();
        
        $alreadyInvited = CompanionRequests::where('inviteduser_id', '=', $id)
            ->where('invitinguser_id', '=', $this->user->id)
            ->where('status', '=', CompanionRequests::STATUS_OPEN)
            ->first();
        
        if (!is_null($alreadyInvite) || !is_null($alreadyInvited)) {
            return $this->returnError(['message' => trans('default.already_have_companion_invite')]);
        }
        
        $request = CompanionRequests::create([
            'invitinguser_id' => $this->user->id,
            'inviteduser_id' => $id,
            'invitationdate' => Carbon::now(),
            'status' => CompanionRequests::STATUS_OPEN
        ]);
        
        $messageData = [
            'type' => 'companioninvitation_received',
            'message' => trans('default.push_companion_received'),
            'companionrequest_id' => $request->id,
            'invitationdate' => $request->invitationdate->format('Y-m-d H:i:s'),
            'invitinguser' => [
                'id' => $this->user->id,
                'displayname' => $this->user->displayname,
                'pictureurl' => asset(Storage::disk('profile_pictures')->url($this->user->profilepictureurl)),
            ]
        ];
        
        $this->pushNotification($id, $messageData);
        
        return $this->returnSuccess();
    }
    
    public function requestMyCompanionRequestList(Request $request)
    {
        $data = $request->all();
        $itemCount = isset($data['itemcount']) ? $data['itemcount'] : 10;
        
        $statuses = array_values(CompanionRequests::$statusConstants);
        if (isset($data['statuses'])) {
            $requestStatuses = array_map(function($item) { return trim($item); }, explode(',', $data['statuses']));
            
            $statuses = [];
            foreach ($requestStatuses as $status) {
                if (isset(CompanionRequests::$statusConstants[$status])) {
                    $statuses[] = CompanionRequests::$statusConstants[$status];
                }
            }
        }
        
        $requestsQuery = CompanionRequests::whereIn('status', $statuses)
            ->where(function ($query) {
                $query->where('inviteduser_id', $this->user->id)
                    ->orWhere('invitinguser_id', $this->user->id);
            });
        
        if (isset($data['dateuntil'])) {
            $requestsQuery->where('invitationdate', '<', $data['dateuntil']);
        }
        
        $requestsQuery->take($itemCount);
        $requests = $requestsQuery->get();
        
        if (count($requests)) {
            $inviteList = [];
            foreach ($requests as $request) {
                $inviteList[] = [
                    'id' => $request->id,
                    'inviteduser_id' => $request->inviteduser_id,
                    'invitinguser_id' => $request->invitinguser_id,
                    'invitationdate' => $request->invitationdate,
                    'status' => CompanionRequests::$textStatuses[$request->status],
                ];
            }

            return $this->returnSuccess(['companionrequest' => $inviteList]);
        }
        
        return $this->returnNotFound(['message' => trans('default.companion_requests_not_found')]);
    }
    
    public function searchUser(Request $request)
    {
        $data = $request->all();
        
        $itemCount = isset($data['itemcount']) ? $data['itemcount'] : 10;
        $page = isset($data['pagenumber']) ? $data['pagenumber'] : 0;
       	
	$companions['companions'] = [];
	if (isset($data['searchtext'])) {
	    $text = $data['searchtext'];
        
            $users = User::where(function ($query) use ($text) {
                $search = '%' . $text . '%';
                $query
                    ->orWhere('nickname', 'LIKE', $search)
                    ->orWhere('firstname', 'LIKE', $search)
                    ->orWhere('lastname', 'LIKE', $search);
            })
            ->limit($itemCount) 
            ->offset($itemCount * $page)
            ->get();
        
            if (count($users)) {
                foreach ($users as $user) {
                    $companions['companions'][] = [
                        'user_id' => $user->id,
                        'nickname' => !is_null($user->nickname) ? $user->nickname : $user->firstname,
                        'pictureurl' => asset(Storage::disk('profile_pictures')->url($user->profilepictureurl))
                    ];
                }
         
            }
	}
 
    	return $this->returnSuccess($companions);    
    }

    public function driftAndInvitationList(Request $request)
    {
        $data = $request->all();
        $itemCount = isset($data['itemcount']) ? $data['itemcount'] : 10;
        
        $driftsQuery = DB::table('drifts')
            ->where(function ($query){
                $query->where('drifts.driver_id', $this->user->id)
                    ->orWhere('drifts.drifter_id', $this->user->id);
            })
            ->whereIn('drifts.status', [Drifts::STATUS_AGREED, Drifts::STATUS_RUNNING]);
        
        if (isset($data['date-until'])) {
            $driftsQuery->where('drifts.created_at', '<=', $data['date-until']);
        }
        $drifts = $driftsQuery->orderBy('drifts.created_at', 'desc')->take($itemCount)->get();
        
        $invitesQuery = DB::table('driftinvitations')
            ->where('driftinvitations.invitinguser_id', $this->user->id)
            ->whereIn('driftinvitations.status', [DriftInvitations::STATUS_OPEN, DriftInvitations::STATUS_REJECTED]);
        
        if (isset($data['date-until'])) {
            $invitesQuery->where('driftinvitations.invitationdate', '<=', $data['date-until']);
        }
        $invites = $invitesQuery->orderBy('driftinvitations.invitationdate', 'desc')->take($itemCount)->get();

        $result['items'] = [];
        if (count($drifts)) {
            foreach ($drifts as $drift) {
                $anotherUserId = $drift->driver_id == $this->user->id ? $drift->drifter_id : $drift->driver_id;
                $user = User::findOrFail($anotherUserId);
                $text = $drift->status == Drifts::STATUS_AGREED ? 
                    trans('default.drift_agreed_text_status', ['nickname' => $user->displayname]) :
                    trans('default.drift_running_text_status', ['nickname' => $user->displayname]);
                
                $result['items'][] = [
                   'type' => 'drift',
                   'id' => $drift->id,
                   'otheruser_id' => $anotherUserId,
                   'date' => $drift->created_at,
                   'status' => Drifts::$textStatuses[$drift->status],
                   'text' => $text
                ];
            }
        }
        
        if (count($invites)) {
            foreach ($invites as $invite) {
                $anotherUserId = $invite->inviteduser_id;
                $user = User::findOrFail($anotherUserId);
                $text = $invite->status == DriftInvitations::STATUS_OPEN ? 
                    trans('default.driftinvitation_open_text_status', ['nickname' => $user->displayname]) :
                    trans('default.driftinvitation_rejected_text_status', ['nickname' => $user->displayname]);
                
                $result['items'][] = [
                   'type' => 'driftinvitation',
                   'id' => $invite->id,
                   'date' => $invite->invitationdate,
                   'otheruser_id' => $anotherUserId,
                   'status' => DriftInvitations::$textStatuses[$invite->status],
                   'text' => $text
                ];
            }
        }
        
        return $this->returnSuccess($result);
    }
    
    public function openRequestList(Request $request)
    {
        $data = $request->all();
        $itemCount = isset($data['itemcount']) ? $data['itemcount'] : 10;
        
        $driftsQuery = DB::table('drifts')
            ->where(function ($query){
                $query->where('drifts.driver_id', $this->user->id)
                    ->orWhere('drifts.drifter_id', $this->user->id);
            })
            ->whereIn('drifts.status', [Drifts::STATUS_COMPLETED]);
        
        if (isset($data['date-until'])) {
            $driftsQuery->where('drifts.created_at', '<=', $data['date-until']);
        }
        $drifts = $driftsQuery->orderBy('drifts.created_at', 'desc')->take($itemCount)->get();
        
        $invitesQuery = DB::table('driftinvitations')
            ->where('driftinvitations.inviteduser_id', $this->user->id)
            ->whereIn('driftinvitations.status', [DriftInvitations::STATUS_OPEN]);
        
        if (isset($data['date-until'])) {
            $invitesQuery->where('driftinvitations.invitationdate', '<=', $data['date-until']);
        }
        $invites = $invitesQuery->orderBy('driftinvitations.invitationdate', 'desc')->take($itemCount)->get();

        $companionsQuery = DB::table('companionrequests')
            ->where('companionrequests.inviteduser_id', $this->user->id)
            ->whereIn('companionrequests.status', [CompanionRequests::STATUS_OPEN]);
        
        if (isset($data['date-until'])) {
            $invitesQuery->where('companionrequests.invitationdate', '<=', $data['date-until']);
        }
        $companions = $companionsQuery->orderBy('companionrequests.invitationdate', 'desc')->take($itemCount)->get();
        
        $result['items'] = [];
        if (count($drifts)) {
            foreach ($drifts as $drift) {
                $hasReview = Reviews::where('drift_id', '=', $drift->id)->first();
                if ($hasReview) {
                    continue;
                }
                
                $anotherUserId = $drift->driver_id == $this->user->id ? $drift->drifter_id : $drift->driver_id;
                $user = User::findOrFail($anotherUserId);
                $text = trans('default.review_open_request_text', ['nickname' => $user->displayname]);
                
                $result['items'][] = [
                   'type' => 'openreview',
                   'id' => $drift->id,
                   'otheruser_id' => $anotherUserId,
                   'date' => $drift->created_at,
                   'status' => Drifts::$textStatuses[$drift->status],
                   'text' => $text
                ];
            }
        }
        
        if (count($invites)) {
            foreach ($invites as $invite) {
                $anotherUserId = $invite->invitinguser_id;
                $user = User::findOrFail($anotherUserId);
                $text = trans('default.driftinvitation_open_request_text', ['nickname' => $user->displayname]);
                
                $result['items'][] = [
                   'type' => 'driftinvitation',
                   'id' => $invite->id,
                   'otheruser_id' => $anotherUserId,
                   'date' => $invite->invitationdate,
                   'status' => DriftInvitations::$textStatuses[$invite->status],
                   'text' => $text
                ];
            }
        }
        
        if (count($companions)) {
            foreach ($companions as $companion) {
                $anotherUserId = $companion->invitinguser_id;
                $user = User::findOrFail($anotherUserId);
                $text = trans('default.companion_open_request_text', ['nickname' => $user->displayname]);
                
                $result['items'][] = [
                   'type' => 'companionrequest',
                   'id' => $companion->id,
                   'otheruser_id' => $anotherUserId,
                   'date' => $companion->invitationdate,
                   'status' => CompanionRequests::$textStatuses[$companion->status],
                   'text' => $text
                ];
            }
        }
        
        return $this->returnSuccess($result);
    }
    
    public function getChatClientUrl(Request $request) {
        $user = $this->user;

        if (!$user) {
            return $this->returnNotFound();
        }

        // if the other user is given to chat with: try to get that user's guapp id and append to url
        $otherUserId = $request->get('otheruser_id');
        $otherUserEmail = null;
        if($otherUserId) {
            $otherUser = User::where('id', '=', $otherUserId)->first();
            if (!is_null($otherUser)) {
                $otherUserEmail = $otherUser->email;
            }
        }

        $chatService = GuappChatService::make();
        $chatService->login($user->email, null);
        if($otherUserEmail) {
            $url = $chatService->getClientUrlChatWithEmail($otherUserEmail);
        } else {
            $url = $chatService->getClientUrl();
        }

        return $this->returnSuccess(['url' => $url]);
    }

    public function updatePlannedRoute(Request $request)
    {
        $route = $request->get('route');
        
        if (is_null($route)) {
            return $this->returnError(['message' => trans('default.route_not_passed')]);
        }
        
        try {
            $this->routeService->saveRoute($this->user->id, $route);
        } catch (ProcessFailedException $e) {
            return $this->returnError(['message' => trans('default.route_save_error')]);
        }
        
        return $this->returnSuccess(['message' => trans('default.route_saved')]);
    }
    
    public function sendMessage($id, Request $request)
    {
        $message = $request->get('message');
        
        if (!$message) {
            return $this->returnError(['message' => trans('default.message_is_empty')]);
        }
        
        $hasBlock = UserBlock::where('user_id', '=', $id)->where('blockeduser_id', '=', $this->user->id)->first();
        if ($hasBlock) {
            return $this->returnError(['message' => trans('default.user_block_you')]);
        }
        
        $recipient = User::where('id', '=', $id)->first();
        
        $chatService = GuappChatService::make();
        $chatService->login($this->user->email, null);
        $chatService->addFriendByEmail($recipient->email);
        $chatService->sendTextMessage($recipient->email, null, $message);
      
        return $this->returnSuccess(['message' => trans('default.message_sended')]);
    }   
    
    private function validateVehicleArguments($data)
    {
        return Validator::make($data, [
            'platenumber' => 'required',
            'carmodel' => 'required',
        ]);
    }
    
    private function validateProfileArguments($data)
    {
        return Validator::make($data, [
            'gender' => 'required',
            'firstname' => 'required',
            'lastname' => 'required', 
            'email' => 'required',
            'mobile' => 'required',
            'address' => 'required',
            'zip' => 'required',
            'countrycode' => 'required',
            'birthdate' => 'required',
            'hasdriverslicense' => 'required',
            'city' => 'required|max:255',
        ]);
    }
    
    private function validatePasswordArguments($data)
    {
        return Validator::make($data, [
            'oldpassword' => 'required',
            'newpassword' => 'required',
        ]);
    }
    
    private function validateLocationArguments($data)
    {
        return Validator::make($data, [
            'lat' => 'required',
            'lon' => 'required',
        ]);
    }
    
    private function validateDestinationArguments($data)
    {
        return Validator::make($data, [
            'lat' => 'required',
            'lon' => 'required',
            'caption' => 'required',
        ]);
    }
    
    private function validateDeviceTokenArguments($data)
    {
        return Validator::make($data, [
            'client' => 'required',
            'devicetoken' => 'required',
        ]);
    }
}
