<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Session;
use Validator;

use App\Models\User;
use App\Models\UserSession;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $data = $request->all();
        $validator = $this->validateLoginArguments($data);
        
        if ($validator->fails())
        {
            return $this->processValidatorErrors($validator->errors());
        }
        
        $user = User::where('email', '=', $data['email'])->first();
        if (is_null($user)) {
            return $this->returnForbidden(['message' => trans('default.user_not_found')]);
        }
        
        if ($user->is_blocked) {
            return $this->returnForbidden();
        }
        
        if ($user->passwordhash != sha1($data['password'])) {
            return $this->returnForbidden(['message' => trans('default.password_not_matched')]);
        }
        
        $session = UserSession::where('token', '=', Session::getId())->first();
        if ($session) {
            return $this->returnSuccess(['token' => $session->token]);
        }
        
        try {
            UserSession::create(['user_id' => $user->id, 'token' => Session::getId()]);
            
            return $this->returnSuccess(['token' => Session::getId()]);
        } catch (\Exception $e) {
            return $this->returnForbidden(['message' => trans('default.failed_login')]);
        }
    }
    
    public function logout(Request $request)
    {
        $data = $request->all();
        $validator = $this->validateLogoutArguments($data);
        
        if ($validator->fails())
        {
            return $this->processValidatorErrors($validator->errors());
        }
        
        try {
            $sessions = UserSession::where('token', '=', $data['token'])->get()->pluck('id');
            if (count($sessions)) {
                UserSession::destroy($sessions);
            }

            // invalidate login in Guapp chat as well
            $chatService = GuappChatService::make();
            $chatService->logout();

        } catch (\Exception $e) {
            return $this->returnError(['message' => trans('default.failed_logout')]);
        }

        return $this->returnSuccess();
    }
    
    public function checkNickname(Request $request)
    {
        $data = $request->all();
        $validator = $this->validateNicknameArguments($data);
        
        if ($validator->fails())
        {
            return $this->processValidatorErrors($validator->errors());
        }
        
        $user = User::where('nickname', '=', $data['nickname'])->first();
        $response = $user ? ['available' => false] : ['available' => true];
        
        return $this->returnSuccess($response);
    }
    
    public function checkEmail(Request $request)
    {
        $data = $request->all();
        $validator = $this->validateEmailArguments($data);
        
        if ($validator->fails())
        {
            return $this->processValidatorErrors($validator->errors());
        }
        
        $user = User::where('email', '=', $data['email'])->first();
        $response = $user ? ['available' => false] : ['available' => true];
        
        return $this->returnSuccess($response);
    }
    
    private function validateLoginArguments($data)
    {
        return Validator::make($data, [
            'email' => 'required',
            'password' => 'required',
        ]);
    }
    
    private function validateNicknameArguments($data)
    {
        return Validator::make($data, [
            'nickname' => 'required',
        ]);
    }
    
    private function validateEmailArguments($data)
    {
        return Validator::make($data, [
            'email' => 'required',
        ]);
    }
    
    private function validateLogoutArguments($data)
    {
        return Validator::make($data, [
            'token' => 'required',
        ]);
    }
}
