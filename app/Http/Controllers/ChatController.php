<?php

namespace App\Http\Controllers;

use App\Mail\NewChatMessage;
use App\Models\PushService;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Mail;

class ChatController extends Controller
{

    public function postNotification(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'secret' => 'required',
                'senderemail' => 'required',
                'recipientemail' => 'required',
            ]
        );
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()
            ], 400);
        }

        $communitySecret = $request->get('secret');
        if ($communitySecret != config('guapp.chat.notificationsecret')) {
            return response('', 403);
        }

        $senderEmail = $request->get('senderemail');
        $users = User::where('email', $senderEmail)->get();
        if($users->isEmpty()) {
            return response('', 404);
        }
        $sender = $users->first();

        $recipientEmail = $request->get('recipientemail');
        $users = User::where('email', $recipientEmail)->get();
        if($users->isEmpty()) {
            return response('', 404);
        }
        $recipient = $users->first();

        $message = $request->get('message');
        $data = [
            'title' => trans('default.message.newmessage', ['fromname' => $sender->nickname]),
            'body' => $message,
            'type' => 'chatmessage',
            'message' => $message,
            'fromuser'=> [
                'id' => $sender->id,
                'email' => $sender->email,
                'displayname' => $sender->nickname
            ]
        ];

        $this->pushNotification($recipient->id, $data);

        return response('', 200);
    }

}