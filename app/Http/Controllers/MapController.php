<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use Validator;
use DB;
use Storage;

use App\Service\RouteService;
use App\Models\User;
use App\Models\Companions;
use App\Models\DriftInvitations;
use App\Models\UserLocations;

class MapController extends Controller
{
    public function __construct() 
    {
        parent::__construct();
    }
    
    public function userList(Request $request)
    {
        $routeService = new RouteService();
        $data = $request->all();
        
        $validator = $this->validateUserListArguments($data);

        if ($validator->fails()) {
            return $this->processValidatorErrors($validator->errors());
        }
        
        $hasDestination = !is_null($this->user->destinationlat) && 
            !is_null($this->user->destinationlon);

        //get last user points
        $userIds = DB::select(sprintf('SELECT user_id, MAX(timestamp) FROM userlocations WHERE user_id <> %s GROUP BY user_id', $this->user->id));
        $userIds = array_map(function($item) {
            return $item->user_id;
        }, $userIds);
        
        // "mode is Drifter"
        if ($this->user->modeKey == User::MODE_DRIFTER) {
            $calculatedTopUsers = array();
            
            // "destination selected"
            // "return top 10 drivers which share as much route as possible"
            if ($hasDestination) {
                $userPoints = $routeService->getRoutes($this->user->id);
                $calculatedTopUsers = array_slice(array_keys($userPoints), 0, 20);
            }
            
            // return top 10 nearest drivers and drifters which are < 10 kilometres away from current location
            $points = DB::select(sprintf('SELECT user_id, lat, lon, (6371 * acos(cos(radians(%s)) * cos(radians(lat)) 
                * cos(radians(lon) - radians(%s)) + sin(radians(%s)) * sin(radians(lat)))) AS distance 
                FROM userlocations
                WHERE user_id IN (%s)
                GROUP BY user_id
                HAVING distance < %s
                LIMIT 10',
                    $data['lat'],
                    $data['lon'],
                    $data['lat'],
                    implode(',', $userIds),
                    10
            ));
            
            $selectedTopUsers = array();
            foreach ($points as $point) {
                $selectedTopUsers[] = $point->user_id;
            }
            
            $allUsers = array_merge($selectedTopUsers, $calculatedTopUsers);
        } else { // "mode is driver"
            // "return top 10 drifters which share as much route possible"
            $userPoints = $routeService->getRoutes($this->user->id);
            $calculatedTopUsers = array_slice(array_keys($userPoints), 0, 10);
            
            // "return top 10 nearest drifters which do not have selected a destination and are < 10 kilometres away from current location"
            $pointsWithoutDestination = DB::select(sprintf('SELECT user_id, lat, lon, (6371 * acos(cos(radians(%s)) * cos(radians(lat)) 
                * cos(radians(lon) - radians(%s)) + sin(radians(%s)) * sin(radians(lat)))) AS distance 
                FROM userlocations
                JOIN users ON userlocations.user_id = users.id
                WHERE users.destinationlat IS NULL
                AND users.destinationlon IS NULL
                AND users.id IN (%s)
                GROUP BY user_id
                HAVING distance < %s
                LIMIT 10',
                    $data['lat'],
                    $data['lon'],
                    $data['lat'],
                    implode(',', $userIds),
                    10
            ));
            
            $selectedTopUsersWithoutDestination = array();
            foreach ($pointsWithoutDestination as $point) {
                $selectedTopUsersWithoutDestination[] = $point->user_id;
            }
            
            // "return top 10 nearest drifter which are < 10 kilometres away from current location"
            $pointsWithDestinations = DB::select(sprintf('SELECT user_id, lat, lon, (6371 * acos(cos(radians(%s)) * cos(radians(lat)) 
                * cos(radians(lon) - radians(%s)) + sin(radians(%s)) * sin(radians(lat)))) AS distance 
                FROM userlocations
                JOIN users ON userlocations.user_id = users.id
                WHERE users.id IN (%s)
                GROUP BY user_id
                HAVING distance < %s
                LIMIT 10',
                    $data['lat'],
                    $data['lon'],
                    $data['lat'],
                    implode(',', $userIds),
                    10
            ));
            
            $selectedTopUsersWithDestination = array();
            foreach ($pointsWithDestinations as $point) {
                $selectedTopUsersWithDestination[] = $point->user_id;
            }
            
            // return all drifters that have sent a drift invitation (open)
            $invitingUsers = DriftInvitations::select()
                ->join('users', 'driftinvitations.invitinguser_id', '=', 'users.id')
                ->where('driftinvitations.status', '=', DriftInvitations::STATUS_OPEN)
                ->get();
            
            $selectedInvitingUsers = array();
            foreach ($invitingUsers as $user) {
                $selectedInvitingUsers[] = $user->invitinguser_id;
            }
            
            $allUsers = array_merge(
                $calculatedTopUsers,
                $selectedTopUsersWithoutDestination, 
                $selectedTopUsersWithDestination, 
                $selectedInvitingUsers
            );  
        }
        
        // "return all companions (which are visible)"
        $companions = Companions::where('user_id', '=', $this->user->id)
            ->join('users', 'users.id', '=', 'companions.user_id')
            ->where('invisiblemode', '=', 0)
            ->get();
        
        $selectedCompanions = array();
        foreach ($companions as $companion) {
            $selectedCompanions[] = $companion->companion_id;
        }
        
        $allUsers = array_merge($allUsers, $selectedCompanions);
        //users should be unique
        $allUsers = array_unique($allUsers);
        
        $userExists = array_search($this->user->id, $allUsers);
        if ($userExists !== false) {
            unset($allUsers[$userExists]);
        }
        
        $userData = [];
        foreach ($allUsers as $userId) {
            $user = User::where('id', '=', $userId)->first();
            
            //exclude invisible users
            if ($user->invisiblemode) {
                continue;
            }
            
            $point = UserLocations::where('user_id', '=', $userId)->get()->last();
            if (!$point) {
                continue;
            }
            
            /*
            //only looking for companion if user switched this mode on
            $isCompanion = null;
            if ($user->companionmode) {
                $isCompanion = Companions::where('user_id', '=', $this->user->id)
                    ->where('companion_id', '=', $user->id)->first();
            }
            
            if (!in_array(strtolower($user->mode), explode(',', $data['types']))
                && is_null($isCompanion)) {
                continue;
            }
            */
            
            $userData['users'][] = [
                'id' => $user->id,
                'lat' => $point->lat,
                'lon' => $point->lon,
                'caption' => $user->display_name,
                'pictureurl' => !empty($user->profilepictureurl) ? asset(Storage::disk('profile_pictures')->url($user->profilepictureurl)) : null,
                'destinationlat' => $user->destinationlat,
                'destinationlon' => $user->destinationlon,
                'destinationcaption' => $user->destinationcaption,
                'mode' => $user->mode,
            ];
        }
        
        if (count($userData) == 0) {
            return $this->returnSuccess(['users' => []]);
        }
        
        return $this->returnSuccess($userData);
    }
    
    public function driftList(Request $request)
    {
        $data = $request->all();
        $validator = $this->validateDriftListArguments($data);
        
        if ($validator->fails())
        {
            return $this->processValidatorErrors($validator->errors());
        }
        
        $points = DB::select(sprintf('SELECT id, lat, lon, caption, (6371 * acos(cos(radians(%s)) * cos(radians(lat)) 
            * cos(radians(lon) - radians(%s)) + sin(radians(%s)) * sin(radians(lat)))) AS distance 
            FROM driftpoints
            HAVING distance < %s',
                $data['lat'],
                $data['lon'],
                $data['lat'],
                $data['distance'] / 1000
        ));
        
        if (!count($points)) {
            return $this->returnSuccess(['message' => trans('default.not_nearby_drifts')]);
        }
        
        $driftData = [];
        foreach ($points as $point) {
            $driftData['driftpoints'][] = [
                'id' => $point->id,
                'lat' => $point->lat,
                'lon' => $point->lon,
                'caption' => $point->caption
            ];
        }
        
        return $this->returnSuccess($driftData);
    }
    
    private function validateUserListArguments($data)
    {
        return Validator::make($data, [
            'lat' => 'required|numeric',
            'lon' => 'required|numeric',
//            'distance' => 'required',
//            'types' => 'required',
        ]);
    }
    
    private function validateDriftListArguments($data)
    {
        return Validator::make($data, [
            'lat' => 'required|numeric',
            'lon' => 'required|numeric',
            'distance' => 'required',
        ]);
    }
}
