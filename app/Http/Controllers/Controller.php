<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\HttpFoundation\Request;
use App;
use GuzzleHttp\Client;
use Input;

use App\Models\UserSession;
use App\Models\DeviceToken;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    protected $locale;
    protected $user;
    
    public function __construct()
    {
        $this->locale = Input::get('lang') ? Input::get('lang') : App::getLocale();
        App::setLocale($this->locale);
        
        $token = Input::get('token');
        $session = UserSession::where('token', '=', $token)->first();
        
        if ($session) {
            $this->user = $session->user()->first();
        }
    }
    
    public function home()
    {
        return response('It works!');
    }
    
    protected function processValidatorErrors($errors)
    {       
        $message = '';
        foreach($errors->getMessages() as $field => $error) {
            $message .= $error[0] . PHP_EOL;
        }

        return $this->returnError(['message' => $message]);
    }
    
    protected function returnSuccess($data = array())
    {
        return response()->json(['status' => 'success'] + $data, 200, [], JSON_UNESCAPED_SLASHES);
    }
    
    protected function returnError($data = array())
    {
        return response()->json(['status' => 'error'] + $data, 400);
    }
    
    protected function returnForbidden($data = array())
    {
        return response()->json(['status' => 'forbidden'] + $data, 403);
    }
    
    protected function returnNotFound($data = array())
    {
        return response()->json(['status' => 'not found'] + $data, 200);
    }
    
    protected function pushNotification($userId, array $data)
    {
        $device = DeviceToken::where('user_id', '=', $userId)->first();
        
        if (!$device) {
            return $this->returnError();
        }
        
        $apiUrl = env($device->client . '_fcm_api_endpoint');
        $serverKey = env($device->client . '_fcm_api_server_key');
        
        $client = \App::make('GuzzleHttp\Client', [
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'key=' . $serverKey
                ]
            ]
        ]);

        $client->post($apiUrl,
            [
                'body' => json_encode([
                    'to' => $device->token,
                    'priority' => 'high',
                    'data' => $data,
                    'notification' => [
                        'body' => $data['message'],
                        'title' => 'New notification from drift!',
                        'icon' => 'fcm_push_icon'
                    ]
                ])
            ]
        );
    }
    
    protected function getExt($base64string){
        $pos  = strpos($base64string, ';');
        $type = explode(':', substr($base64string, 0, $pos))[1];
        $ext = explode('/', $type)[1];
        
        return $ext;
    }
}
