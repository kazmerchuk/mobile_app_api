<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\Content;

class DataController extends Controller
{
    public function __construct() {
        parent::__construct();
    }
    
    public function getCountryList()
    {
        $dataUrl = 'https://restcountries.eu/rest/v1/all';
        $client = \App::make('GuzzleHttp\Client');

        $response = $client->get($dataUrl);
        
        $decodedResponse = json_decode($response->getBody()->getContents(), true);
        
        $processedCountries = [];
        $locale = $this->locale;
        
        foreach ($decodedResponse as $country) {
            $processedCountries[$country['alpha2Code']] = $locale == 'en' ? $country['name'] : $country['translations'][$locale];
        }
        
        //Frequently used countries, null is delimiter
        $countries = [
            [
                'code' => 'DE',
                'text' => $processedCountries['DE'],
            ],
            null
        ];
        unset($processedCountries['DE']);
        
        foreach ($processedCountries as $code => $name) {
            $countries[] = [
                'code' => $code,
                'text' => $name,
            ];
        }
        
        return $this->returnSuccess(['countries' => $countries]);
    }
    
    public function getContent($name)
    { 
        $content = Content::where('name', '=', $name)->where('language', '=', \App::getLocale())->first();
        
        if (is_null($content)) {
            return response(trans('default.content_not_exists'), 404);
        }
        
        return response($content->content);
    }
}
