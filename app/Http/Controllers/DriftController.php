<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use Validator;
use Storage;
use Carbon\Carbon;

use App\Models\UserSession;
use App\Models\Drifts;
use App\Models\Reviews;
use App\Models\Photos;
use App\Models\UserLocations;

class DriftController extends Controller
{    
    public function __construct() {
        parent::__construct();
    }
    
    public function addReview(Request $request, $id)
    {
        $data = $request->all();
        $validator = $this->validateReviewArguments($data);

        if ($validator->fails())
        {
            return $this->processValidatorErrors($validator->errors());
        }
        
        $drift = Drifts::where('id', '=', $id)->first();
        if (is_null($drift)) {
            return $this->returnNotFound(['message' => trans('default.drift_not_found')]);
        }
        if ($drift->drifter_id != $this->user->id && $drift->driver_id != $this->user->id) {
            return $this->returnError(['message' => trans('default.not_participant_of_this_drift')]);
        }
        
        $review = Reviews::where('user_id', '=', $this->user->id)
            ->where('drift_id', '=', $id)
            ->first();
        
        if (!is_null($review)) {
            return $this->returnError(['message' => trans('default.review_already_exist')]);
        }
        
        Reviews::create([
            'drift_id' => $id,
            'user_id' => $this->user->id == $drift->drifter_id ? $drift->driver_id : $drift->drifter_id,
            'reviewby' => $this->user->id,
            'updown' => Reviews::$textVotes[$data['updown']],
            'text' => $data['text']
        ]);
        
        return $this->returnSuccess();
    }
    
    public function getDriftPhotoList(Request $request, $id)
    {
        $data = $request->all();
        
        $drift = Drifts::where('id', '=', $id)->first();
        if (is_null($drift)) {
            return $this->returnNotFound(['message' => trans('default.drift_not_found')]);
        }
        
        $itemCount = isset($data['itemcount']) ? $data['itemcount'] : 10;
        $page = isset($data['pagenumber']) ? $data['pagenumber'] : 0;
        
        $photos = Photos::where('drift_id', '=', $id)
            ->limit($itemCount)
            ->offset($itemCount * $page)
            ->get()
            ->sortBy('timestamp');
        
        $photoList = ['drift_id' => $drift->id, 'photos' => []];
        foreach ($photos as $photo) {
            $photoData = $photo->toArray();
            $photoData['url'] = asset(Storage::disk('drift_pictures')->url($photoData['url']));
            
            $photoList['photos'][] = $photoData;
        }
        
        return $this->returnSuccess($photoList);
    }
    
    public function startDrift($id)
    {
        $drift = Drifts::where('id', '=', $id)->first();
        if (
            is_null($drift) || 
            $drift->status != Drifts::STATUS_AGREED 
            || (
                    $drift->driver_id != $this->user->id 
                    && $drift->drifter_id != $this->user->id
            )
        ) {
            return $this->returnNotFound(['message' => trans('default.drift_not_found')]);
        }
        
        $userLastLocation = UserLocations::where('user_id', '=', $this->user->id)->get()->last();
        
        $drift->status = Drifts::STATUS_RUNNING;
        
        if ($userLastLocation) {
            $drift->startlat = $userLastLocation->lat;
            $drift->startlon = $userLastLocation->lon;
        }
        
        $drift->save();
        
        return $this->returnSuccess();
    }
    
    public function endDrift($id)
    {
        $drift = Drifts::where('id', '=', $id)->first();
        if (
            is_null($drift) || 
            $drift->status != Drifts::STATUS_RUNNING
            || (
                    $drift->driver_id != $this->user->id
                    && $drift->drifter_id != $this->user->id
            )
        ) {
            return $this->returnNotFound(['message' => trans('default.drift_not_found')]);
        }
        
        $userLastLocation = UserLocations::where('user_id', '=', $this->user->id)->get()->last();
        
        $drift->status = Drifts::STATUS_COMPLETED;
        
        if ($userLastLocation) {
            $drift->endlat = $userLastLocation->lat;
            $drift->endlon = $userLastLocation->lon;
        }
        
        $drift->save();
        
        return $this->returnSuccess();
    }
    
    public function cancelDrift($id)
    {
        $drift = Drifts::where('id', '=', $id)->first();
        if (
            is_null($drift) || 
            !in_array($drift->status, [Drifts::STATUS_AGREED, Drifts::STATUS_RUNNING])
            || (
                    $drift->driver_id != $this->user->id 
                    && $drift->drifter_id != $this->user->id
            )
        ) {
            return $this->returnNotFound(['message' => trans('default.drift_not_found')]);
        }
        
        $userLastLocation = UserLocations::where('user_id', '=', $this->user->id)->get()->last();
        
        $drift->status = Drifts::STATUS_CANCELLED;
        
        if ($userLastLocation) {
            $drift->endlat = $userLastLocation->lat;
            $drift->endlon = $userLastLocation->lon;
        }
        
        $drift->save();
        
        return $this->returnSuccess();
    }
    
    public function uploadPhoto(Request $request, $id)
    {
        $data = $request->all();
        
        $validator = $this->validatePhotoArguments($data);
        
        if ($validator->fails()) {
            return $this->processValidatorErrors($validator->errors());
        }
        
        $drift = Drifts::where('id', '=', $id)->first();
        if (is_null($drift)) {
            return $this->returnNotFound(['message' => trans('default.drift_not_found')]);
        }
        
        if ($drift->drifter_id != $this->user->id && $drift->driver_id != $this->user->id) {
            return $this->returnError(['message' => trans('default.not_participant_of_this_drift')]);
        }
        
        $ext = $this->getExt($data['picture']);
        $imageName = sha1($this->user->id . time()) . '.' . $ext;
        $image = base64_decode(explode(',', $data['picture'])[1]);

        Storage::disk('drift_pictures')->put($imageName, $image);
        
        $photo = Photos::create([
            'url' => 'drifts/' . $imageName,
            'timestamp' => Carbon::now(),
            'drift_id' => $drift->id,
            'user_id' => $this->user->id
        ]);
        
        if (isset($data['lat'])) {
            $photo->lat = $data['lat'];
        }
        
        if (isset($data['lon'])) {
            $photo->lon = $data['lon'];
        }
        
        if (isset($data['caption'])) {
            $photo->caption = $data['caption'];
        }
        
        $photo->save();

        return $this->returnSuccess();
    }
    
    public function getDrift($id)
    {
        $drift = Drifts::where('id', '=', $id)->first();
        if (is_null($drift)) {
            return $this->returnNotFound(['message' => trans('default.drift_not_found')]);
        }
        
        $driftData = [
            'drift_id' => $drift->id,
            'drifter_id' => $drift->drifter_id,
            'driver_id' => $drift->driver_id,
            'status' => Drifts::$textStatuses[$drift->status],
            'created_at' => $drift->created_at,
            'start_date' => $drift->startdate,
            'end_date' => $drift->enddate,
            'startlon' => $drift->startlon,
            'startlat' => $drift->startlat,
            'startcaption' => $drift->startcaption,
            'endlon' => $drift->endlon,
            'endlat' => $drift->endlat,
            'endcaption' => $drift->endcaption,
        ];
        
        $driftPhoto = Photos::where('drift_id', '=', $id)->first();
        if (!is_null($driftPhoto)) {
            $driftData['coverpictureurl'] = asset(Storage::disk('drift_pictures')->url($driftPhoto->url));
        }
        
        return $this->returnSuccess($driftData);
    }
    
    private function validateReviewArguments($data)
    {
        return Validator::make($data, [
            'text' => 'required',
            'updown' => 'required|in:up,down',
        ]);
    }
    
    private function validatePhotoArguments($data)
    {
        return Validator::make($data, [
            'picture' => 'required',
        ]);
    }
}
