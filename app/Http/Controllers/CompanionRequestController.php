<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Storage;

use App\Models\CompanionRequests;
use App\Models\Companions;
use App\Models\GuappChatService;
use App\Models\User;

class CompanionRequestController extends Controller
{
    public function __construct() 
    {
        parent::__construct();
    }
    
    public function acceptRequest($id)
    {
        $request = CompanionRequests::with('inviteduser')->where('id', '=', $id)->first();
        if (is_null($request) || $request->status != CompanionRequests::STATUS_OPEN) {
            return $this->returnNotFound(['message' => trans('default.companionrequest_not_found')]);
        }
        
        if ($request->inviteduser_id != $this->user->id) {
            return $this->returnError(['message' => trans('default.companion_wrong_user')]);
        }
        
        $request->status = CompanionRequests::STATUS_ACCEPTED;
        $request->save();
        
        Companions::create([
            'user_id' => $this->user->id,
            'companion_id' => $request->invitinguser_id
        ]);
        
        Companions::create([
            'user_id' => $request->invitinguser_id,
            'companion_id' => $this->user->id
        ]);

        // report companionship to Guapp chat
        $chatService = GuappChatService::make();
        $chatService->login($this->user->email, null);
        $invitingUser = User::where('id', $request->invitinguser_id)->first();
        $chatService->addFriendByEmail($invitingUser->email);

        $messageData = [
            'type' => 'companioninvitation_accepted',
            'message' => trans('default.push_companion_accepted'),
            'companionrequest_id' => $request->id,
            'invitationdate' => $request->invitationdate,
            'inviteduser' => [
                'id' => $request->inviteduser->id,
                'displayname' => $request->inviteduser->displayname,
                'pictureurl' => asset(Storage::disk('profile_pictures')->url($request->inviteduser->profilepictureurl)),
            ]
        ];
        
        $this->pushNotification($request->invitinguser_id, $messageData);
        
        return $this->returnSuccess();
    }
    
    public function rejectRequest($id)
    {
        $request = CompanionRequests::with('inviteduser')->where('id', '=', $id)->first();
        if (is_null($request) || $request->status != CompanionRequests::STATUS_OPEN) {
            return $this->returnNotFound(['message' => trans('default.companionrequest_not_found')]);
        }
        
        if ($request->inviteduser_id != $this->user->id) {
            return $this->returnError(['message' => trans('default.companion_wrong_user')]);
        }
        
        $request->status = CompanionRequests::STATUS_REJECTED;
        $request->save();
        
        $messageData = [
            'type' => 'companioninvitation_rejected',
            'message' => trans('default.push_companion_rejected'),
            'companionrequest_id' => $request->id,
            'invitationdate' => $request->invitationdate,
            'inviteduser' => [
                'id' => $request->inviteduser->id,
                'displayname' => $request->inviteduser->displayname,
                'pictureurl' => asset(Storage::disk('profile_pictures')->url($request->inviteduser->profilepictureurl)),
            ]
        ];
        
        $this->pushNotification($request->invitinguser_id, $messageData);
        
        return $this->returnSuccess();
    }
}
