<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use Validator;
use Carbon\Carbon;
use Storage;

use App\Models\User;
use App\Models\DriftInvitations;
use App\Models\Drifts;

class DriftInvitationController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function sendInvitation(Request $request)
    {
        $data = $request->all();
        $validator = $this->validateInvitationArguments($data);
        
        if ($validator->fails())
        {
            return $this->processValidatorErrors($validator->errors());
        }
        
        $user = User::where('id', '=', $data['user_id'])->first();
        if (is_null($user)) {
            return $this->returnNotFound(['message' => trans('default.user_not_found')]);
        }
        if ($user->mode == $this->user->mode) {
            return $this->returnError(['message' => trans('default.user_in_same_mode')]);
        }
        
        $invite = Driftinvitations::where('invitinguser_id', '=', $this->user->id)
            ->where('inviteduser_id', '=', $user->id)
            ->where('status', '=', DriftInvitations::STATUS_OPEN)
            ->first();
        
        if ($invite) {
            return $this->returnError(['message' => trans('default.driftinvite_exists')]);
        }
        
        $invite = DriftInvitations::create([
            'invitinguser_id' => $this->user->id,
            'inviteduser_id' => $user->id,
            'status' => DriftInvitations::STATUS_OPEN,
            'invitationdate' => Carbon::now(),
            'responsedate' => Carbon::now(),
        ]);
        
        $messageData = [
            'type' => 'driftinvitation_received',
            'message' => trans('default.push_drift_received'),
            'driftinvitation_id' => $invite->id,
            'invitationdate' => $invite->invitationdate,
            'invitinguser' => [
                'id' => $this->user->id,
                'displayname' => $this->user->displayname,
                'pictureurl' => asset(Storage::disk('profile_pictures')->url($this->user->profilepictureurl)),
            ]
        ];
        
        $this->pushNotification($user->id, $messageData);
        
        return $this->returnSuccess(['driftinvitation_id' => $invite->id]);
    }
    
    public function acceptInvitation(Request $request, $id)
    {
        $invite = DriftInvitations::with('inviteduser')->where('id', '=', $id)->first();
        if (is_null($invite) || $invite->status != DriftInvitations::STATUS_OPEN) {
            return $this->returnNotFound(['message' => trans('default.driftinvitation_not_found')]);
        }
        
        if ($invite->inviteduser_id != $this->user->id) {
            return $this->returnError(['message' => trans('default.invite_wrong_user')]);
        }
        
        $invite->status = DriftInvitations::STATUS_ACCEPTED;
        $invite->save();
        
        $drift = Drifts::create([
            'drifter_id' => $invite->invitinguser_id,
            'driver_id'  => $invite->inviteduser_id,
            'status' => Drifts::STATUS_AGREED,
        ]);
        
        $messageData = [
            'type' => 'driftinvitation_accepted',
            'message' => trans('default.push_drift_accepted'),
            'driftinvitation_id' => $invite->id,
            'invitationdate' => $invite->invitationdate,
            'inviteduser' => [
                'id' => $invite->inviteduser->id,
                'displayname' => $invite->inviteduser->displayname,
                'pictureurl' => asset(Storage::disk('profile_pictures')->url($invite->inviteduser->profilepictureurl)),
            ]
        ];
        
        $this->pushNotification($invite->invitinguser_id, $messageData);
        
        return $this->returnSuccess(['drift_id' => $drift->id]);
    } 
    
    public function rejectInvitation($id)
    {
        $invite = DriftInvitations::where('id', '=', $id)->first();
        if (is_null($invite) || $invite->status != DriftInvitations::STATUS_OPEN) {
            return $this->returnNotFound(['message' => trans('default.driftinvitation_not_found')]);
        }
        
        if ($invite->inviteduser_id != $this->user->id) {
            return $this->returnError(['message' => trans('default.invite_wrong_user')]);
        }
        
        $invite->status = DriftInvitations::STATUS_REJECTED;
        $invite->save();
        
        $messageData = [
            'type' => 'driftinvitation_rejected',
            'message' => trans('default.push_drift_rejected'),
            'driftinvitation_id' => $invite->id,
            'invitationdate' => $invite->invitationdate,
            'inviteduser' => [
                'id' => $invite->inviteduser->id,
                'displayname' => $invite->inviteduser->displayname,
                'pictureurl' => asset(Storage::disk('profile_pictures')->url($invite->inviteduser->profilepictureurl)),
            ]
        ];
        
        $this->pushNotification($invite->invitinguser_id, $messageData);
        
        return $this->returnSuccess();
    }
    
    private function validateInvitationArguments($data)
    {
        return Validator::make($data, [
            'user_id' => 'required',
        ]);
    }
}
