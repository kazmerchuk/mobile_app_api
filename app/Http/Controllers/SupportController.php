<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use Validator;
use Mail;

class SupportController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getCategoryList()
    {
        $categories = [
            'supportcategories' => [
                0 => trans('default.support_category.technical'),
                1 => trans('default.support_category.usability'),
            ]
        ];
        
        return $this->returnSuccess($categories);
    }
    
    public function sendMessage(Request $request)
    {
        $data = $request->all();
        $validator = $this->validateSupportContactArguments($data);
        
        if ($validator->fails()) {
            return $this->processValidatorErrors($validator->errors());
        }
        
        $recipient = env('SUPPORT_EMAIL');
        
        Mail::send('emails.support-message', ['text' => $data['message']], function($message) use($recipient) {
            $message
                ->to($recipient);
        });
        
        return $this->returnSuccess();
    }
    
    private function validateSupportContactArguments($data)
    {
        return Validator::make($data, [
            'supportcategory_id' => 'required',
            'message' => 'required'
        ]);
    }
}
