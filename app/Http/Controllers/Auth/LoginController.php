<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Symfony\Component\HttpFoundation\Request;

use Auth;
use Lang;

use App\Models\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        
        $this->middleware('guest', ['except' => 'logout']);
    }
    
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
        
        $email = $request->get('email');
        $password = sha1($request->get('password'));
        
        $user = User::where(['email' => $email])->first();
        
        if (!$user || $user->is_admin == false || $user->passwordhash != $password || $user->is_blocked) {
            return redirect($this->getRedirectUrl())
                ->withInput($request->only($email, 'remember'))
                ->withErrors([
                    $email => Lang::get('auth.failed'),
                ]);
        }
        
        Auth::login($user);
        
        return redirect('admin');
    }
    
    public function getLogin()
    {
        return view('auth.login');
    }
    
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/admin/login');
    }
}
