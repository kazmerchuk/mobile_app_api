<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\GuappChatService;
use Symfony\Component\HttpFoundation\Request;
use Validator;

use App\Models\User;

class RegistrationController extends Controller
{    
    public function registration(Request $request)
    {
        $arguments = $request->all();
        $validator = $this->validateRegistrationArguments($arguments);
        
        if ($validator->fails()) {
            return $this->processValidatorErrors($validator->errors());
        }
        
        try {
            $user = User::create($arguments);

            // report new user to Guapp chat
            $chatService = GuappChatService::make();
            $chatService->login($user->email, null); // this will automatically create the user
            $displayName = empty($user->nickname) ? $user->nickname : $user->firstname." ".$user->lastname;
            $chatService->setDisplayName($displayName);

            return $this->returnSuccess();
        } catch (\Exception $e) {
            return $this->returnError(['message' => trans('default.registration_error_message')]);
        }
    }
    
    private function validateRegistrationArguments($data)
    {
        return Validator::make($data, [
            'gender' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|unique:users,email',
            'mobile' => 'required',
            'address' => 'required',
            'zip' => 'required',
            'countrycode' => 'required',
            'hasdriverslicense' => 'required',
            'password' => 'required',
            'birthdate' => 'required',
            'city' => 'required|max:255',
        ]);
    }
}
