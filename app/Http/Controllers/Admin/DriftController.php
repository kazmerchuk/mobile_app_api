<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use Carbon\Carbon;

use App\Models\User;
use App\Models\Drifts;

class DriftController extends Controller
{
    public function index(Request $request)
    {
        $dateFrom = $request->get('date-from');
        $dateUntil = $request->get('date-until');
        (int)$status = $request->get('status');
        
        $driftsQb = Drifts::with('driver')->with('drifter');
        $statuses = Drifts::$textStatuses;
        
        if (!empty($dateFrom)) {
            $dateFrom .= ' 00:00:00';
            $driftsQb->where('startdate', '>=', Carbon::createFromTimestamp(strtotime($dateFrom)));
        }
        
        if (!empty($dateUntil)) {
            $dateUntil .= ' 23:59:59';
            $driftsQb->where('enddate', '<=', Carbon::createFromTimestamp(strtotime($dateUntil)));
        }
        
        if (!empty($status)) {
            $driftsQb->where('status', '=', Drifts::$statusConstants[$status]);
        }
        
        $drifts = $driftsQb->paginate(20);
        
        return view('admin.drift.index', compact('drifts', 'statuses'));
    }
}
