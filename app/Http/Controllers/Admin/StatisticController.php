<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use Carbon\Carbon;

use App\Models\Statistics;

class StatisticController extends Controller
{
    public function index(Request $request)
    {   
        $dateFrom = $request->get('date-from') ? $request->get('date-from') : Carbon::createFromTimestamp(time())->setTime(0, 0, 0);
        $dateUntil = $request->get('date-until') ? $request->get('date-until') : Carbon::createFromTimestamp(time())->setTime(23, 59, 59);
        
        $statisticQb = Statistics::select();
        if (!empty($dateFrom)) {
            $statisticQb->where('date', '>=', Carbon::createFromTimestamp(strtotime($dateFrom)));
        }
        
        if (!empty($dateUntil)) {
            $statisticQb->where('date', '<=', Carbon::createFromTimestamp(strtotime($dateUntil)));
        }
        
        $statistics = $statisticQb->paginate(20);
        
        return view('admin.statistic.index', compact('statistics'));
    }
}
