<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Client;

use App\Models\User;

class NotificationController extends Controller
{
    public function index(Request $request)
    {
        $groups = [
            0 => trans('default.admin.notification.groups.all_users'),
            1 => trans('default.admin.notification.groups.drivers'),
            2 => trans('default.admin.notification.groups.drifters'),
        ];
        
        if ($request->getMethod() == 'POST') {
            $text = $request->get('message');
            $group = $request->get('user_group');
            
            $usersQb = User::with('token');
            
            switch ($group) {
                case "1":
                    $usersQb->where('mode', '=', 1);
                break;
                case "2":
                    $usersQb->where('mode', '=', 0);
                break;
            }
            
            $users = $usersQb->where('is_blocked', '=', 0)->get();
            
            if (count($users)) {
                $success = 0;
                $error = 0;
                //FCM accepts maximum 1000 users, so split them here by parts
                foreach ($users->chunk(1000) as $partOfUsers) {
                    $registrationIds = [];
                    foreach ($partOfUsers as $user) {
                        $token = $user->token()->first();
                        if (is_null($token)) {
                            continue;
                        }
                        $device = $token->client;
                        
                        $registrationIds[$device][] = $token->token;
                    }
                    
                    foreach ($registrationIds as $device => $tokens) {
                        $apiUrl = env($device . '_fcm_api_endpoint');
                        $serverKey = env($device . '_fcm_api_server_key');
                        
                        if (is_null($apiUrl) || is_null($serverKey)) {
                            continue;
                        }
                        
                        $client = new Client([
                            'headers' => [
                                'Content-Type' => 'application/json',
                                'Authorization' => 'key=' . $serverKey
                            ]
                        ]);
                        
                        $response = $client->post($apiUrl,
                            [
                                'body' => json_encode([
                                    'registration_ids' => $tokens,
                                    'priority' => 'high',
                                    'data' => [
                                        'type' => 'text',
                                        'message' => $text
                                    ]
                                ])
                            ]
                        );
                        
                        $decodedResponse = json_decode($response->getBody()->getContents(), true);
                        
                        $success += $decodedResponse['success'];
                        $error += $decodedResponse['failure'];
                    }
                }
                
                return redirect()->route('notification_list')
                    ->with('message', trans('default.admin.notification.success_send_message', [
                        'success' => $success, 
                        'fails' => $error
                    ]));
            }
            return redirect()->route('notification_list')
                ->with('message', trans('default.admin.notification.fail_send_message'));
        }
        
        return view('admin.notification.index', compact('groups'));
    }
}
