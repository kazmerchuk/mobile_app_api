<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use DB;
use \Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

use App\Models\User;
use App\Models\Reviews;
use App\Models\UserSession;
use App\Models\UserLocations;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->get('search');
        
        $qb = User::select();
        
        if ($search) {
            $qb->where(function ($query) use ($search) {
                $search = '%' . $search . '%';
                $query
                    ->orWhere('nickname', 'LIKE', $search)
                    ->orWhere('email', 'LIKE', $search)
                    ->orWhere('firstname', 'LIKE', $search)
                    ->orWhere('lastname', 'LIKE', $search);
            });
        }
        
        $users = $qb->paginate(25);
        
        return view('admin.user.index', compact('users'));
    }
    
    public function view($id, Request $request)
    {
        $user = User::findOrFail($id);
        $reviews = null;
        $locations = null;
        
        $upvotes = DB::table('reviews')
            ->where('user_id', '=', $user->id)
            ->where('updown', '=', Reviews::REVIEW_VOTE_UP)
            ->count();
        
        $downvotes = DB::table('reviews')
            ->where('user_id', '=', $user->id)
            ->where('updown', '=', Reviews::REVIEW_VOTE_DOWN)
            ->count();
        
        if ($request->get('mode')) {
            if ($request->get('mode') == 'ratings') {
                $reviews = Reviews::with('drift')->where('user_id', '=', $id)->paginate(10);
            } else if ($request->get('mode') == 'locations') {
                $locations = UserLocations::where('user_id', '=', $id)->paginate(10);
            }   
        }
        
        return view('admin.user.view', compact('user', 'upvotes', 'downvotes', 'reviews', 'locations'));
    }
    
    public function block($id)
    {
        $user = User::findOrFail($id);
        
        if ($user->is_blocked) {
            $user->is_blocked = 0;
            $user->save();
        } else {
            $user->is_blocked = 1;
            $user->save();
            
            UserSession::where('user_id', '=', $user->id)->delete();
        }
        
        return redirect()->route('user_view', ['id' => $id]);
    }
    
    public function showMap(Request $request)
    {
        $latitude = $request->get('latitude');
        $longitude = $request->get('longitude');
        
        if (is_null($latitude) || is_null($longitude)) {
            throw new BadRequestHttpException();
        }
        
        return view('admin.user.map', compact('latitude', 'longitude'));
    }
}
