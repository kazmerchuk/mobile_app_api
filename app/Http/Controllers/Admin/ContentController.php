<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use Validator;
use App;

use App\Models\Content;

class ContentController extends Controller
{
    public function index()
    {
        $contentItems = Content::get();
        
        return view('admin.content.index', compact('contentItems'));
    }
    
    public function create()
    {
        return view('admin.content.create');
    }
    
    public function store(Request $request)
    {
        $data = $request->except(['_method', '_token']);
        
        $validator = $this->validateContent($data);
        
        if ($validator->fails())
        {
            return redirect()->route('content_create')->withErrors($validator->errors());
        }
        
        try {
            Content::create($data);
        } catch (\Exception $e) {
            return redirect()->route('content_create')->withErrors([trans('validation.attributes.content_already_exists')]);
        }
        
        return redirect()->route('content_list');
    }
    
    public function put($id, Request $request)
    {
        $content = Content::where(['id' => $id])->first();
        
        if (is_null($content)) {
            return redirect()->route('content_list');
        }
        
        $data = $request->except(['_method', '_token']);
        
        $validator = $this->validateContent($data);
        
        if ($validator->fails())
        {
            return redirect()->route('content_edit', ['id' => $id])->withErrors($validator->errors());
        }
        
        try {
            Content::where('id', '=', $id)->update($data);
        } catch (\Exception $e) {
            return redirect()->route('content_edit', ['id' => $id])->withErrors([trans('validation.attributes.content_already_exists')]);
        }
        
        return redirect()->route('content_list');
    }
    
    public function edit($id)
    {
        $content = Content::where(['id' => $id])->first();
        if (is_null($content)) {
            App::abort(404);
        }
        
        return view('admin.content.edit', compact('content'));
    }
    
    public function delete($id)
    {
        $content = Content::where(['id' => $id])->first();
        if (is_null($content)) {
            App::abort(404);
        }
        
        $content->delete();
        
        return redirect()->route('content_list');
    }
    
    public function validateContent($data)
    {
        return Validator::make($data, [
            'content'  => 'required',
            'name'     => 'required',
            'language' => 'required',
        ]);
    }
}
