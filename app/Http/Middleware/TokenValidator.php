<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

use App\Models\UserSession;

class TokenValidator
{
    /**
     * Validates user session token
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->get('token');
        
        if (is_null($token)) {
            return response()->json(['status' => 'forbidden'], 403);
        }
        
        $userSession = UserSession::where('token', '=', $token)->first();
        if (is_null($userSession)) {
            return response()->json(['status' => 'forbidden'], 403);
        }
        
        return $next($request);
    }
}
