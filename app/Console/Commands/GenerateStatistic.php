<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Carbon\Carbon;

use App\Models\User;
use App\Models\Reviews;
use App\Models\Drifts;
use App\Models\DriftInvitations;
use App\Models\Statistics;

class GenerateStatistic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statistic:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate statistic for current date or for given dates';
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startDate = Carbon::createFromTimestamp(time())->setTime(00, 00, 00);
        $endDate = Carbon::createFromTimestamp(time())->setTime(23, 59, 59);
    
        $registrations = User::where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->count();
        $proposedDrifts = DriftInvitations::where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->count();
        $agreedDrifts = Drifts::where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->where('status', '=', Drifts::STATUS_AGREED)->count();
        $reviews = Reviews::where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->count();
        
        $statistic = Statistics::where('date', $startDate)->first();
        
        if (!$statistic) {
            Statistics::create([
                'new_registration_count' => $registrations, 
                'proposed_drift_count' => $proposedDrifts,
                'agreed_drift_count' => $agreedDrifts,
                'rating_number_count' => $reviews,
                'date' => $startDate,
            ]);
        } else {
            $statistic->new_registration_count = $registrations;
            $statistic->proposed_drift_count = $proposedDrifts;
            $statistic->agreed_drift_count = $agreedDrifts;
            $statistic->rating_number_count = $reviews;
            $statistic->save();
        }
    }
}
