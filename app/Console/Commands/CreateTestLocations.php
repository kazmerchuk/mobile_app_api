<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\UserLocations;

class CreateTestLocations extends Command
{
    protected $testUsers = [80, 81, 82, 83, 84, 99, 100, 101, 102, 106];
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'routes:create-test-locations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill userlocations table for test needs';
    
    public function handle()
    {
        $storagePath = storage_path('testroutes');
        
        try {
            $iteration = file_get_contents($storagePath . '/iteration.pid');
        } catch (\Exception $e) {
            $iteration = 0;
        }
        
        $count = 1;
        while ($count < 7) {
            foreach ($this->testUsers as $user) {
                $userRoutes = json_decode(file_get_contents($storagePath . '/' . $user . '.json'), true);

                UserLocations::where('user_id', '=', $user)->delete();
                UserLocations::create([
                    'user_id' => $user,
                    'lat' => $userRoutes[$iteration]['latitude'],
                    'lon' => $userRoutes[$iteration]['longitude'],
                    'timestamp' => (new \DateTime())->format('Y-m-d H:i:s')
                ]);
            }
            
            $nextIterationValue = $iteration == 59 ? 0 : ++$iteration;
            file_put_contents($storagePath . '/iteration.pid', $nextIterationValue);
            
            sleep(10);
            $count++;
        }    
    }
}
