<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Service\RouteService;

class CleanRoutes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'routes:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean route files which older than twenty four hours';
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $routeService = new RouteService();
        $routeService->cleanRoutes();
    }
}
