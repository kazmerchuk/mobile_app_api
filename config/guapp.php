<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Guapp Chet Settings
    |--------------------------------------------------------------------------
    |
    | This option specifies
    | - The base URL to call Guap Chat API entpoints
    | - The URL of the chat client UI
    | - The name of the Guapp Chat community for this application
    | - The Guapp Chat community secret that authorizes API access
    */

    'chat' => [
        'baseurl' => env('GUAPP_BASEURL', 'http://chat.guapp.com'),
        'clienturl' => env('GUAPP_CLIENTURL', 'http://chat.guapp.com'),
        'communityname' => env('GUAPP_COMMUNITYNAME', ''),
        'communitysecret' => env('GUAPP_COMMUNITYSECRET', ''),
        'notificationsecret' => env('GUAPP_NOTIFICATIONSECRET', '')
    ],

];
