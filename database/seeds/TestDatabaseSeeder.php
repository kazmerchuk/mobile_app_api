<?php

use Illuminate\Database\Seeder;

class TestDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->runFactoriesToCreateUsersAndSessions();
        $this->runFactoriesForUsersWithVehicles();
        $this->runFactoriesForVisibleUsers();
        $this->runFactoriesForDriftReviews();
        $this->runFactoriesForDriftPoints();
        $this->runFactoriesForPhotos();
        $this->runFactoriesForDeviceTokens();
    }

    public function runFactoriesToCreateUsersAndSessions()
    {
        factory(App\Models\User::class, 50)->create()
            ->each(function ($u) {
                $u->userSessions()->save(factory(App\Models\Usersession::class)->make());
            });
    }

    public function runFactoriesForVisibleUsers()
    {
        $visibleUsers = App\Models\User::where('invisiblemode', false)->get();
        foreach ($visibleUsers as $user) {
            factory(App\Models\UserLocations::class)->create([
                'user_id' => $user->id
            ]);
        }
    }

    public function runFactoriesForUsersWithVehicles()
    {
        $usersWithVehicles = App\Models\User::where('hasvehicle', 1)->get();
        foreach ($usersWithVehicles as $user) {
            $user->vehicle()->save(factory(App\Models\Vehicles::class)->make());
            factory(App\Models\Drifts::class)->create([
                'driver_id' => $usersWithVehicles->random()->id,
                'drifter_id' => $usersWithVehicles->random()->id,
            ]);
            factory(App\Models\CompanionRequests::class)->create([
                'inviteduser_id' => $usersWithVehicles->random()->id,
                'invitinguser_id' => $usersWithVehicles->random()->id,
            ]);
            factory(App\Models\Companions::class)->create([
                'user_id' => $user->id,
                'companion_id' => $usersWithVehicles->where('id', '!=', $user->id)
                    ->random()->id
            ]);
            factory(App\Models\DriftInvitations::class)->create([
                'inviteduser_id' => $usersWithVehicles->where('id', '!=', $user->id)
                    ->random()->id,
                'invitinguser_id' => $user->id
            ]);
        }
    }

    public function runFactoriesForDriftReviews()
    {
        $drifts = App\Models\Drifts::inRandomOrder()->limit(3)->get();
        foreach ($drifts as $drift) {
            factory(App\Models\Reviews::class)->create([
                'user_id' => $drift->driver_id,
                'drift_id' => $drift->id,
                'reviewby' => $drift->drifter_id
            ]);
        }
    }

    public function runFactoriesForDriftPoints()
    {
        factory(App\Models\DriftPoint::class, 30)->create();
    }

    public function runFactoriesForPhotos()
    {
        $userIdsWithDrifts = App\Models\Drifts::all()->pluck('driver_id');
        foreach ($userIdsWithDrifts as $userId) {
            $drift = App\Models\Drifts::where('driver_id', $userId)->first();
            factory(App\Models\Photos::class, 15)->create([
                'user_id' => $userId,
                'drift_id' => $drift->id,
            ]);
        }
    }

    public function runFactoriesForDeviceTokens()
    {
        $users = App\Models\User::all();
        foreach ($users as $user) {
            factory(App\Models\DeviceToken::class)->create([
                'user_id' => $user->id
            ]);
        }
    }
}
