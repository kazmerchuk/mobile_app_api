<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

// Define the User model factory

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'nickname' => $faker->name,
        'firstname' => $faker->name,
        'lastname' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'mobile' => $faker->phoneNumber,
        'address' => $faker->address,
        'zip' => $faker->randomNumber,
        'countrycode' => $faker->countryCode,
        'birthdate' => $faker->dateTimeThisCentury->format('Y-m-d'),
        'profilepictureurl' => $faker->url,
        'passwordhash' => sha1('secret'),
        'about' => 'sentence',
        'hasdriverslicense' => $faker->boolean($changeOfGettingTrue = 33),
        'hasvehicle' => $faker->boolean($changeOfGettingTrue = 50),
        'mode' => $faker->randomElement([
            'driver',
            'drifter'
        ]),
        'destinationlat' => $faker->latitude(-90, 90),
        'destinationlon' => $faker->longitude(-70, 70),
        'destinationcaption' => $faker->sentence,
        'invisiblemode' => $faker->boolean(50),
        'gender' => $faker->randomElement([
            'none',
            'male',
            'female',
        ]),
    ];
});

$factory->define(App\Models\Usersession::class, function (Faker\Generator $faker) {
    return [
        'token' => str_random(20),
    ];
});

$factory->define(App\Models\Vehicles::class, function (Faker\Generator $faker) {
    return [
        'platenumber' => str_random(5),
        'carmodel' => $faker->company,
        'pictureurl' => 'picture.png',
    ];
});

$factory->define(App\Models\Drifts::class, function (Faker\Generator $faker) {
    return [
        'status' => $faker->randomElement([
            App\Models\Drifts::STATUS_AGREED,
            App\Models\Drifts::STATUS_RUNNING,
            App\Models\Drifts::STATUS_COMPLETED,
            App\Models\Drifts::STATUS_CANCELLED
        ]),
        'startlat' => $faker->latitude(-90, 90),
        'startlon' => $faker->longitude(-70, 70),
        'startcaption' => $faker->sentence,
        'endlat' => $faker->latitude(-90, 90),
        'endlon' => $faker->longitude(-70, 70),
        'endcaption' => $faker->sentence
    ];
});

$factory->define(App\Models\Reviews::class, function (Faker\Generator $faker) {
    return [
        'updown' => $faker->randomElement([
            App\Models\Reviews::REVIEW_VOTE_DOWN,
            App\Models\Reviews::REVIEW_VOTE_UP,
        ]),
        'text' => $faker->sentence
    ];
});

$factory->define(App\Models\UserLocations::class, function (Faker\Generator $faker) {
    return [
        'lat' => $faker->latitude(-90, 90),
        'lon' => $faker->longitude(-70, 70),
    ];
});

$factory->define(App\Models\DriftPoint::class, function (Faker\Generator $faker) {
    return [
        'lat' => $faker->latitude(-90, 90),
        'lon' => $faker->longitude(-70, 70),
        'caption' => $faker->sentence
    ];
});

$factory->define(App\Models\CompanionRequests::class, function (Faker\Generator $faker) {
    return [
        'status' => $faker->randomElement([
            App\Models\CompanionRequests::STATUS_OPEN,
            App\Models\CompanionRequests::STATUS_ACCEPTED,
            App\Models\CompanionRequests::STATUS_REJECTED,
        ]),
        'invitationdate' => $faker->date('Y-m-d', $max = 'now'),
        'responsedate' => $faker->date('Y-m-d', $max = 'now'),
    ];
});

$factory->define(App\Models\Companions::class, function (Faker\Generator $faker) {
    return [];
});

$factory->define(App\Models\Photos::class, function (Faker\Generator $faker) {
    return [
        'timestamp' => $faker->date('Y-m-d H:i:s', $max = 'now'),
        'lat' => $faker->latitude(-90, 90),
        'lon' => $faker->longitude(-70, 70),
        'url' => $faker->url,
        'caption' => $faker->sentence,
    ];
});

$factory->define(App\Models\DriftInvitations::class, function (Faker\Generator $faker) {
    return [
        'status' => $faker->randomElement([
            App\Models\DriftInvitations::STATUS_OPEN,
            App\Models\DriftInvitations::STATUS_ACCEPTED,
            App\Models\DriftInvitations::STATUS_REJECTED
        ]),
        'invitationdate' => $faker->date('Y-m-d', $max = 'now'),
        'responsedate' => $faker->date('Y-m-d', $max = 'now'),
    ];
});

$factory->define(App\Models\DeviceToken::class, function (Faker\Generator $faker) {
    return [
        'client' => $faker->randomElement([
            'android',
            'ios'
        ]),
        'token' => str_random(20)
    ];
});
