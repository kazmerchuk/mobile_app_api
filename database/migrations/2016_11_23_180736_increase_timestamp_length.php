<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IncreaseTimestampLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('userlocations', function (Blueprint $table) {
            $table->decimal('lat', 9, 7)->change();
            $table->decimal('lon', 10, 7)->change();
        });
        
        Schema::table('drifts', function (Blueprint $table) {
            $table->decimal('startlat', 9, 7)->change();
            $table->decimal('startlon', 10, 7)->change();
            $table->decimal('endlat', 9, 7)->change();
            $table->decimal('endlon', 10, 7)->change();
        });
        
        Schema::table('photos', function (Blueprint $table) {
            $table->decimal('lat', 9, 7)->change();
            $table->decimal('lon', 10, 7)->change();
        });
        
        Schema::table('driftpoints', function (Blueprint $table) {
            $table->decimal('lat', 9, 7)->change();
            $table->decimal('lon', 10, 7)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
