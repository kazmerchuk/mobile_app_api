<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::create('usersession', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->string('token', 512);
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
        });
        
        Schema::create('userlocations', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->dateTime('timestamp');
            $table->float('lat');
            $table->float('lon');
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
        });
        
        Schema::create('drifts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driver_id')->unsigned();
            $table->integer('drifter_id')->unsigned();
            $table->tinyInteger('status');
            $table->float('startlat');
            $table->float('startlon');
            $table->string('startcaption', 512);
            $table->float('endlat');
            $table->float('endlon');
            $table->string('endcaption', 512);
            $table->timestamps();
            
            $table->foreign('driver_id')->references('id')->on('users');
            $table->foreign('drifter_id')->references('id')->on('users');
        });  
        
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('drift_id')->unsigned();
            $table->integer('reviewby')->unsigned()->nullable();
            $table->tinyInteger('updown');
            $table->text('text');
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('reviewby')->references('id')->on('users');
            $table->foreign('drift_id')->references('id')->on('drifts');
        }); 
        
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('platenumber', 32);
            $table->string('carmodel', 1024);
            $table->string('pictureurl', 1024)->nullable();
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
        });
        
        Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('drift_id')->unsigned();
            $table->dateTime('timestamp');
            $table->float('lat')->nullable();
            $table->float('lon')->nullable();
            $table->string('url', 2048);
            $table->string('caption', 1024);
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('drift_id')->references('id')->on('drifts');
        });
        
        Schema::create('companions', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('companion_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('companion_id')->references('id')->on('users');
        });
        
        Schema::create('userblock', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('blockeduser_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('blockeduser_id')->references('id')->on('users');
        });
        
        Schema::create('companionrequests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inviteduser_id')->unsigned();
            $table->integer('invitinguser_id')->unsigned();
            $table->tinyInteger('status');
            $table->dateTime('invitationdate');
            $table->dateTime('responsedate');
            $table->timestamps();
            
            $table->foreign('inviteduser_id')->references('id')->on('users');
            $table->foreign('invitinguser_id')->references('id')->on('users');
        });
        
        Schema::create('driftinvitations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inviteduser_id')->unsigned();
            $table->integer('invitinguser_id')->unsigned();
            $table->tinyInteger('status');
            $table->dateTime('invitationdate');
            $table->dateTime('responsedate');
            $table->timestamps();
            
            $table->foreign('inviteduser_id')->references('id')->on('users');
            $table->foreign('invitinguser_id')->references('id')->on('users');
        });
        
        Schema::create('driftpoints', function (Blueprint $table) {
            $table->increments('id');
            $table->float('lat');
            $table->float('lon');
            $table->string('caption', 1024);
            $table->timestamps();
        });
        
        Schema::create('devicetoken', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->string('client', 512);
            $table->string('token', 512);
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('usersession');
        Schema::drop('userlocations');
        Schema::drop('drifts');
        Schema::drop('reviews');
        Schema::drop('vehicles');
        Schema::drop('photos');
        Schema::drop('companions');
        Schema::drop('userblock');
        Schema::drop('companionrequests');
        Schema::drop('driftinvitations');
        Schema::drop('driftpoints');
        Schema::drop('devicetoken');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
