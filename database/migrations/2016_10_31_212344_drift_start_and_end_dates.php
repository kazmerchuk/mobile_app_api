<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DriftStartAndEndDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drifts', function (Blueprint $table) {
            $table->dateTime('startdate')->nullable();
            $table->dateTime('enddate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drifts', function (Blueprint $table) {
            $table->dropColumn('startdate');
            $table->dropColumn('enddate');
        });
    }
}
