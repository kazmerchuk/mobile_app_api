<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTimestampFromUserLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('userlocations', function (Blueprint $table) {
            $table->dropTimestamps();
        });
        Schema::table('userlocations', function (Blueprint $table) {
            $table->dropColumn('timestamp');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('userlocations', function (Blueprint $table) {
            $table->integer('timestamp');
            $table->timestamps();
        });
    }
}
