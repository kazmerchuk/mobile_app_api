<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetResponseInvitationTimeNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('driftinvitations', function (Blueprint $table) {
            $table->dateTime('responsedate')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driftinvitations', function (Blueprint $table) {
            $table->dateTime('responsedate')->change();
        });
    }
}
