<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanionMode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('companionmode')->default(true);
            $table->string('nickname', 256)->nullable()->change();
            $table->boolean('invisiblemode')->default(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('companionmode');
            $table->string('nickname', 256)->nullable(false)->change();
            $table->boolean('invisiblemode')->change();
        });
    }
}
