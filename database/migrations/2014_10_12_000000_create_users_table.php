<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nickname', 256)->nullable();
            $table->string('firstname', 256);
            $table->string('lastname', 256);
            $table->string('email', 128);
            $table->string('mobile', 32);
            $table->string('address', 1024);
            $table->string('zip', 32);
            $table->string('countrycode', 8);
            $table->date('birthdate');
            $table->string('passwordhash', 128);
            $table->string('about', 180)->nullable();
            $table->string('profilepictureurl', 1024)->nullable();
            $table->boolean('hasdriverslicense');
            $table->boolean('hasvehicle')->nullable();
            $table->integer('mode')->nullable();
            $table->float('destinationlat')->nullable();
            $table->float('destinationlon')->nullable();
            $table->string('destinationcaption', 512)->nullable();
            $table->boolean('invisiblemode')->default(false);
            $table->tinyInteger('gender');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
