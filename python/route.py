#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
python route.py -h
usage: route.py [-h] [-c] [-i [IMPORTROUTE]] [-d DELETEROUTE]
                [-dp [DELETEPARTROUTE [DELETEPARTROUTE ...]]]
                [-r COMPAREROUTE] [-g] [-q] [-l LENGTH]

Script for find common route path and calculate route length.

optional arguments:
  -h, --help            show this help message and exit
  -c, --create          Create 'union_routes_point' table for route points
  -i [IMPORTROUTE], --importroute [IMPORTROUTE]
                        Import json file(s) to database. Use '-i' for import
                        all routes from input json dir or use '-i <userid>'
                        for import specified route
  -d DELETEROUTE, --deleteroute DELETEROUTE
                        Delete route by <userid>. Command: '-d <userid>'
  -dp [DELETEPARTROUTE [DELETEPARTROUTE ...]], --deletepartroute [DELETEPARTROUTE [DELETEPARTROUTE ...]]
                        Delete part of route by <userid> from <lat lng> point.
                        Command: '-dp <lat> <lng> <userid>'. Format: '-dp
                        dd.dddddd dd.dddddd <INT>'
  -r COMPAREROUTE, --compareroute COMPAREROUTE
                        Compare the route of a given client id with all other
                        routes. Command: '-r <userid>'
  -g, --getcommonroute  Get common route path from all routes: userid & length
  -q, --queryall        Query list userid sort by route length DESC, LIMIT 100
  -l LENGTH, --length LENGTH
                        Get route length by userid. Command '-l <userid>'
'''

import os
import json
import psycopg2
import datetime
import argparse
import sys

import config

# get all 'json' files in 'input_json' dir
json_files = filter(lambda x: x.endswith('.json'), os.listdir(config.input_json))
#print(json_files)


# define ArgumentParser
def createParser ():
	parser = argparse.ArgumentParser(description='Script for find common route path and calculate route length.')

	parser.add_argument('-c', '--create', action='store_true', help='Create \'union_routes_point\' table for route points')
	parser.add_argument('-i', '--importroute', action='store', nargs='?', type=int, default='-1', help='Import json file(s) to database. Use \'-i\' for import all routes from input json dir or use \'-i <userid>\' for import specified route')
	parser.add_argument('-d', '--deleteroute', type=int, help='Delete route by <userid>. Command: \'-d <userid>\'')
	parser.add_argument('-dp', '--deletepartroute', nargs='*', help='Delete part of route by <userid> from <lat lng> point. Command: \'-dp <lat> <lng> <userid>\'. Format: \'-dp dd.dddddd dd.dddddd <INT>\'')
	parser.add_argument('-r', '--compareroute', type=int, help='Compare the route of a given client id with all other routes. Command: \'-r <userid>\'')
	parser.add_argument('-g', '--getcommonroute', action='store_true', help='Get common route path from all routes: userid & length')
	parser.add_argument('-q', '--queryall', action='store_true', help='Query list userid sort by route length DESC, LIMIT 100')
	parser.add_argument('-l', '--length', type=int, help='Get route length by userid. Command \'-l <userid>\'')
	
	return parser


	# Create \'union_routes_point\' table for route points'
def initialize_union_routes_point_table():
	conn = psycopg2.connect(config.pg_connection)
	cursor = conn.cursor()
	cursor.execute(""" 
		DROP TABLE IF EXISTS union_routes_point;
		--
		CREATE TABLE union_routes_point ( userid INTEGER,
										  pid INTEGER,
										  lat numeric(20,15),
										  lon numeric(20,15),
										  geom geometry(Point,4326) );
	""")
	conn.commit()
	cursor.close()
	
	
# Import json file(s) to database. Use \'-i\' for import all routes from input json dir or use \'-i <userid>\' for import specified route
def import_all_json_files_to_database():
	print("start import: " + datetime.datetime.now().isoformat())
	for json_file in json_files:
		userid = int(json_file.replace('.json', ''))
		print("import userid: '" + str(userid) + "' route to database")
		
		conn = psycopg2.connect(config.pg_connection)
		cursor = conn.cursor()

		# delete existing rows in table for %userid
		cursor.execute("DELETE FROM union_routes_point WHERE userid = '%s'" % userid)
		conn.commit()
		
		f = open(os.path.join(config.input_json, json_file), 'r')
		data = f.read()
		coord_list = json.loads(data)

		#to_db = [(userid, pid, i['latitude'], i['longitude']) for i in coord_list]
		pid = 0
		for i in coord_list:
			pid = pid + 1
			to_db = [(userid, pid, i['latitude'], i['longitude'])]
			# add rows to table for %userid
			cursor.executemany("INSERT INTO union_routes_point (userid, pid, lat, lon, geom) VALUES (%s, %s, %s, %s, NULL );", to_db)
			conn.commit()

		cursor.close()
		conn.close()

	# add geometry column and create spatial index
	conn = psycopg2.connect(config.pg_connection)
	cursor = conn.cursor()
	cursor.execute(""" 
		UPDATE union_routes_point SET geom = ST_SetSRID(ST_MakePoint(lat, lon), 4326);
		DROP INDEX IF EXISTS union_routes_point_geom_idx;
		CREATE INDEX union_routes_point_geom_idx ON union_routes_point USING GIST(geom);
	""")
	conn.commit()
	cursor.close()
	conn.close()
	
	print("stop import: " + datetime.datetime.now().isoformat())
	

def import_one_json_file_to_database(userid):
	userid = int(userid)
	
	# create array of userid
	userid_arr = []
	for json_file in json_files:
		uid = int(json_file.replace('.json', ''))
		userid_arr.append(uid)

	# check if json file available for input userid
	if userid in userid_arr:
		print("\nuserid:", userid, "available in input_json folder")
		print("start import userid: '" + str(userid) + "' route to database")
		
		conn = psycopg2.connect(config.pg_connection)
		cursor = conn.cursor()

		# delete existing rows in table for %userid
		cursor.execute("DELETE FROM union_routes_point WHERE userid = '%s'" % userid)
		conn.commit()
		
		f = open(os.path.join(config.input_json, str(userid) + '.json'), 'r')
		data = f.read()
		coord_list = json.loads(data)

		#to_db = [(userid, pid, i['latitude'], i['longitude']) for i in coord_list]
		pid = 0
		for i in coord_list:
			pid = pid + 1
			to_db = [(userid, pid, i['latitude'], i['longitude'])]
			# add rows to table for %userid
			cursor.executemany("INSERT INTO union_routes_point (userid, pid, lat, lon, geom) VALUES (%s, %s, %s, %s, NULL );", to_db)
			conn.commit()

		cursor.close()
		conn.close()
	
		# add geometry column and create spatial index
		conn = psycopg2.connect(config.pg_connection)
		cursor = conn.cursor()
		cursor.execute(""" 
			UPDATE union_routes_point SET geom = ST_SetSRID(ST_MakePoint(lat, lon), 4326);
			DROP INDEX IF EXISTS union_routes_point_geom_idx;
			CREATE INDEX union_routes_point_geom_idx ON union_routes_point USING GIST(geom);
		""")
		conn.commit()
		cursor.close()
		conn.close()

		print("import userid: '" + str(userid) + "' route to database completed successfully")
		
	else:
		print("\nuserid:", userid, "not available in input_json folder. Please select another userid")

		
# Delete route by userid. Command: \'-d <userid>\'
def delete_route_by_userid(userid):
	conn = psycopg2.connect(config.pg_connection)
	cursor = conn.cursor()
	cursor.execute(" SELECT userid FROM union_routes_point GROUP BY userid ORDER BY userid; ", [userid])
	res = cursor.fetchall()

	route_arr = []
	if len(res) >= 1: 
		for row in res:
			route_arr.append(row[0])
		if userid in route_arr:
			cursor.execute("DELETE FROM union_routes_point WHERE userid = '%s'" % userid)
			conn.commit()
			print("\nuserid", userid, "deleted successfully from 'union_routes_point' table")
		else:
			print("\nuserid", userid, "not available in 'union_routes_point' table")
	elif len(res) == 0:
		print("\ntable 'union_routes_point' is empty. Please import routes")

	cursor.close()
	conn.close()	

	
# Delete part of route by <userid> from <lat lng> point. Command: \'-dp <lat> <lng> <userid>\''
def delete_part_of_route(lat, lng, userid):
	#-dp 29.07032 40.99370 13

	print("\nlat:",lat)
	print("lng:",lng)
	print("userid:",userid)

	conn = psycopg2.connect(config.pg_connection)
	cursor = conn.cursor()
	cursor.execute(" SELECT userid FROM union_routes_point GROUP BY userid ORDER BY userid; ", [userid])
	res = cursor.fetchall()

	route_arr = []
	if len(res) >= 1: 
		for row in res:
			route_arr.append(row[0])
		if userid in route_arr:
			cursor.execute("""
				DELETE FROM union_routes_point 
				WHERE pid < (SELECT pid FROM union_routes_point WHERE userid = %s ORDER BY geom <-> ST_SetSRID(ST_MakePoint(%s, %s),4326) LIMIT 1) 
				AND userid = %s	""", [userid, lat, lng, userid])
			conn.commit()
			print("\nfor", userid, "userid successfully deleted part of route from 'union_routes_point' table")
		else:
			print("\nuserid", userid, "not available in 'union_routes_point' table")
	elif len(res) == 0:
		print("\ntable 'union_routes_point' is empty. Please import routes")

	cursor.close()
	conn.close()	
	
	
# Compare the route of a given client id with all other routes. Command: \'-r <userid>\'
def compare_route_with_other(userid):
	conn = psycopg2.connect(config.pg_connection)
	cursor = conn.cursor()
	
	cursor.execute(" SELECT userid FROM union_routes_point ORDER BY userid; ")
	res = cursor.fetchall()

	# create 'userid_arr' array with userid
	userid_arr = []
	for row in res:
		userid_arr.append(row[0]) 

	if userid in userid_arr:
		cursor.execute(""" 
			SELECT b.userid, ST_LengthSpheroid(ST_MakeLine(b.geom),'SPHEROID["WGS 84",6378137,298.257223563]')/1000 AS length, ST_MakeLine(b.geom) AS geom
			FROM (SELECT * FROM union_routes_point WHERE userid = %s) a, 
				 (SELECT * FROM union_routes_point WHERE userid != %s) b
			WHERE ST_Intersects(a.geom, b.geom) GROUP BY b.userid ORDER BY length DESC, b.userid ASC LIMIT 100;
		""", [userid, userid])
		res_route = cursor.fetchall()
		
		route_json = {}
		# print("\nCOMMON ROUTES WITH", userid, "ROUTE:")
		# print("userid  length (km)")
		for row in res_route:
			# print(row[0], "    ", "{:.4f}".format(row[1]))
			route_json[row[0]] = "{:.4f}".format(row[1])
		
		# print("\nJSON OF COMMON ROUTES WITH", userid, "ROUTE:")
		json_data = json.dumps(route_json)
		print(json_data)

		# create json file in base_dir
		#file = open(os.path.join(base_dir, "routes_" + str(userid) + ".json"), "w") 
		#file.write(json_data) 
		#file.close() 
		
	else:
		print(json.dumps({}))

	cursor.close()
	conn.close()


# Create linear routes from points
def create_union_routes_line_table():
	conn = psycopg2.connect(config.pg_connection)
	cursor = conn.cursor()
	cursor.execute(""" 
		DROP TABLE IF EXISTS union_routes_line; 
		CREATE TABLE union_routes_line AS
		SELECT userid,
			   ST_MakeLine(geom) AS geom
		FROM (SELECT userid, pid, lat, lon, geom FROM union_routes_point ORDER BY userid, pid) t
		GROUP BY userid
		ORDER BY userid;
		
		ALTER TABLE union_routes_line ALTER COLUMN geom TYPE geometry(Linestring,4326);
		
		-- add column 'length' and calculate route length in kilometers
		ALTER TABLE union_routes_line ADD COLUMN length NUMERIC (20,4);
		UPDATE union_routes_line
		SET length = ST_LengthSpheroid(geom,'SPHEROID["WGS 84",6378137,298.257223563]')/1000
	""")
	conn.commit()
	
	'''
	# print userid and route length (length DESC, userid ASC LIMIT 100)
	cursor.execute(""" SELECT userid, length FROM union_routes_line ORDER BY length DESC, userid ASC LIMIT 100; """)
	print("\nROUTE LENGTH:")
	print("userid length (km)")
	for userid, length in cursor.fetchall():
		print(userid,"   ", length)
	'''
	
	cursor.close()
	conn.close()

	
# Get common route path from all routes: userid & length
def create_common_route():
	conn = psycopg2.connect(config.pg_connection)
	cursor = conn.cursor()
	cursor.execute(""" 
		-- create point table with max common points
		DROP TABLE IF EXISTS route_common_point;
		CREATE TABLE route_common_point AS
		SELECT DISTINCT geom,
						count(*) OVER (PARTITION BY geom) AS COUNT
		FROM
		  (SELECT geom,
				  count(*) OVER (PARTITION BY geom) AS COUNT
		   FROM union_routes_point) k
		WHERE COUNT =
			(SELECT MAX(COUNT)
			 FROM
			   (SELECT count(*) OVER (PARTITION BY geom) AS COUNT
				FROM union_routes_point) t);
		-- create linear table with common route
		DROP TABLE IF EXISTS route_common_line; 
		CREATE TABLE route_common_line AS
		SELECT ST_MakeLine(geom) AS geom
		FROM route_common_point;	
		-- add in 'route_common_line' table column with route names
		ALTER TABLE route_common_line ADD COLUMN userid character varying(254);
		UPDATE route_common_line
		SET userid =
		  (SELECT string_agg(aroutes.userid::text, ', ' ORDER BY aroutes.userid)
		   FROM route_common_line croute,
				union_routes_line aroutes
		   WHERE ST_Intersects(croute.geom, aroutes.geom));
		ALTER TABLE route_common_line ALTER COLUMN geom TYPE geometry(Linestring,4326);
		-- add column 'length' and calculate route length in kilometers
		ALTER TABLE route_common_line ADD COLUMN length NUMERIC (20,4);
		UPDATE route_common_line
		SET length = ST_LengthSpheroid(geom,'SPHEROID["WGS 84",6378137,298.257223563]')/1000
	""")
	conn.commit()
	
	# print userid;s and common route length
	cursor.execute(""" SELECT userid, length FROM route_common_line; """)
	print("\nCOMMON ROUTE:")
	for userid, length in cursor.fetchall():
		print("userid:", userid)
		print("length:", length, "km")
		
	cursor.close()
	conn.close()	

	
# Query list userid sort by route length DESC, LIMIT 100
def return_userid_list_length():
	conn = psycopg2.connect(config.pg_connection)
	cursor = conn.cursor()
	
	# print userid's as array with route length order DESC and LIMIT 100
	cursor.execute(""" SELECT userid FROM union_routes_line ORDER BY length DESC LIMIT 100; """)
	
	print("\nUSERID LIST:")
	res = cursor.fetchall()
	userid_arr = []
	for userid in res:
		userid_arr.append(userid[0])
	print(userid_arr)
	
	cursor.close()
	conn.close()	
	
	
# Get route length by userid. Command \'-l <userid>\'	
def get_route_length_by_userid(userid):
	conn = psycopg2.connect(config.pg_connection)
	cursor = conn.cursor()
	
	cursor.execute(" SELECT userid FROM union_routes_point ORDER BY userid; ")
	res = cursor.fetchall()

	# create 'userid_arr' array with userid
	userid_arr = []
	for row in res:
		userid_arr.append(row[0]) 

	if userid in userid_arr:
		cursor.execute(""" 
			SELECT r.userid, ST_LengthSpheroid(ST_MakeLine(r.geom),'SPHEROID["WGS 84",6378137,298.257223563]')/1000 AS length
			FROM union_routes_point r WHERE r.userid = %s GROUP BY r.userid ;
		""", [userid])
		res_route = cursor.fetchall()

		print("\nROUTE", userid, "LENGTH:")
		print("userid  length (km)")
		for row in res_route:
			print(row[0], "    ", "{:.4f}".format(row[1]))
	else:
		print("\nuserid:", userid, "not available in database. Please provide another userid")

	cursor.close()
	conn.close()

	
	
if __name__ == '__main__':
	parser = createParser()

	if len(sys.argv) == 1:
		print('\nERROR! You must specify at least one option\n')
		parser.print_help()
	
	namespace = parser.parse_args()
	
	# Create \'union_routes_point\' table for route points'
	if namespace.create == True:
		initialize_union_routes_point_table()
		print("\n'union_routes_point' table created successfully")

	# Import json file(s) to database. Use \'-i\' for import all routes from input json dir or use \'-i <userid>\' for import specified route
	if namespace.importroute == None:
		import_all_json_files_to_database()
		print("\njson files imported successfully")
	elif namespace.importroute != None and namespace.importroute > 0:
		import_one_json_file_to_database(namespace.importroute)
		
	# Delete route by <userid>. Command: \'-d <userid>\'
	if namespace.deleteroute != None and namespace.deleteroute > 0:
		delete_route_by_userid(namespace.deleteroute)

	# Delete part of route by <userid> from <lat lng> point. Command: \'-dp <lat> <lng> <userid>\''
	if namespace.deletepartroute != None and len(namespace.deletepartroute) == 3:
		delete_part_of_route(float(namespace.deletepartroute[0]), float(namespace.deletepartroute[1]), int(namespace.deletepartroute[2]))
		
	# Compare the route of a given client id with all other routes. Command: \'-r <userid>\'
	if namespace.compareroute != None and namespace.compareroute > 0:
		compare_route_with_other(namespace.compareroute)

	# Get common route path from all routes: userid & length
	if namespace.getcommonroute == True:
		create_union_routes_line_table()
		create_common_route()

	# Query list userid sort by route length DESC, LIMIT 100
	if namespace.queryall == True:
		create_union_routes_line_table()
		return_userid_list_length()

	# Get route length by userid. Command \'-l <userid>\'
	if namespace.length != None and namespace.length > 0:
		get_route_length_by_userid(namespace.length)

		