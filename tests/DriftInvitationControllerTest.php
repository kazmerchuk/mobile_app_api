<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\User;
use App\Models\DeviceToken;
use App\Models\DriftInvitations;

class DriftInvitationControllerTest extends TestCase
{
    use DriftServerTestHelperTrait;

    /** @test **/
    public function it_should_not_send_an_invitation_if_required_params_are_missing()
    {
        $token = $this->getRandomUserToken();
        $this->post('/driftinvitations/send', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => 'The user id field is required.' . PHP_EOL
            ]);
        
        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_not_send_an_invitation_if_an_invalid_user_id_is_passed()
    {
        $token = $this->getRandomUserToken();
        $maxId = User::max('id');
        $invalidId = $maxId + 10;

        $this->post('/driftinvitations/send', [
            'token' => $token,
            'user_id' => $invalidId
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.user_not_found')
            ]);

        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_not_allow_a_user_in_the_same_mode_to_send_an_invitation()
    {
        $users = User::where('mode', User::MODE_DRIVER)->limit(2)->get();
        $firstUserToken = $this->getTokenFromUser($users->first());

        $this->post('/driftinvitations/send', [
            'token' => $firstUserToken,
            'user_id' => $users->last()->id
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => trans('default.user_in_same_mode')
            ]);

        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_send_an_invitation_if_all_params_are_correct()
    {
        $firstUser = User::where('mode', User::MODE_DRIVER)
            ->where('hasvehicle', 1)
            ->inRandomOrder()->first();
        $secondUser = User::where('mode', User::MODE_DRIFTER)
            ->where('hasvehicle', 1)
            ->inRandomOrder()->first();
        $firstUserToken = $this->getTokenFromUser($firstUser);

        $randomDevice = factory(App\Models\DeviceToken::class)->make();

        $this->mockPushNotification();

        $this->post('/driftinvitations/send/', [
            'token' => $firstUserToken,
            'user_id' => $secondUser->id
        ])
            ->seeJsonStructure([
                'status',
                'driftinvitation_id'
            ]);

        $this->seeStatusCode(200);
        
        $this->seeInDatabase('driftinvitations', [
            'invitinguser_id' => $firstUser->id,
            'inviteduser_id' => $secondUser->id,
            'status' => DriftInvitations::STATUS_OPEN,
        ]);
    }

    /** @test **/
    public function it_should_not_accept_invitation_if_driftinvitation_is_not_found()
    {
        $token = $this->getRandomUserToken();
        $maxId = DriftInvitations::max('id');
        $invalidId = $maxId + 10;
        $this->post('/driftinvitations/' . $invalidId . '/accept', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.driftinvitation_not_found')
            ]);

        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_not_accept_a_driftinvitation_if_it_is_not_open()
    {
        $driftInvitation = DriftInvitations::where(
            'status',
            '!=',
            DriftInvitations::STATUS_OPEN
        )->inRandomOrder()->first();
        $token = $this->getRandomUserToken();

        $this->post('/driftinvitations/' . $driftInvitation->id . '/accept', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.driftinvitation_not_found')
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_not_accept_an_invitation_if_user_is_not_invited()
    {
        $driftInvitation = DriftInvitations::where(
            'status',
            DriftInvitations::STATUS_OPEN
        )->inRandomOrder()->first();
        $user = User::where('id', '!=', $driftInvitation->inviteduser_id)->first();
        $token = $this->getTokenFromUser($user);

        $this->post('/driftinvitations/' . $driftInvitation->id . '/accept', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => trans('default.invite_wrong_user')
            ]);

        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_accept_an_invitation_if_all_params_are_correct()
    {
        $driftInvitation = DriftInvitations::where('status', DriftInvitations::STATUS_OPEN)
            ->inRandomOrder()->first();
        $user = User::find($driftInvitation->inviteduser_id);
        $token = $this->getTokenFromUser($user);

        $this->mockPushNotification();

        $this->post('/driftinvitations/' . $driftInvitation->id . '/accept', [
            'token' => $token
        ])
            ->seeJsonStructure([
                'status',
                'drift_id'
            ]);

        $this->seeStatusCode(200);

        // Reload driftInvitation
        $driftInvitation = DriftInvitations::find($driftInvitation->id);
        $this->assertEquals(
            DriftInvitations::STATUS_ACCEPTED,
            $driftInvitation->status
        );
    }

    /** @test **/
    public function it_should_throw_an_error_if_the_driftinvitation_rejection_is_not_found()
    {
        $maxId = DriftInvitations::max('id');
        $invalidId = $maxId + 10;

        $token = $this->getRandomUserToken();
        $this->post('/driftinvitations/' . $invalidId . '/reject', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.driftinvitation_not_found')
            ]);

        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_not_reject_an_invitation_if_it_is_not_open()
    {
        $driftInvitation = DriftInvitations::where(
            'status',
            '!=',
            DriftInvitations::STATUS_OPEN
        )->first();
        $token = $this->getRandomUserToken();

        $this->post('/driftinvitations/' . $driftInvitation->id . '/reject', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.driftinvitation_not_found')
            ]);

        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_show_an_error_if_the_user_is_not_invited_for_a_driftinvitation_rejection_request()
    {
        $driftInvitation = DriftInvitations::where(
            'status',
            DriftInvitations::STATUS_OPEN
        )->inRandomOrder()->first();
        $user = User::where('id', '!=', $driftInvitation->inviteduser_id)
            ->first();
        $token = $this->getTokenFromUser($user);

        $this->post('/driftinvitations/' . $driftInvitation->id . '/reject', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => trans('default.invite_wrong_user')
            ]);
        
        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_reject_an_invitation_if_all_params_are_correct()
    {
        $driftInvitation = DriftInvitations::where(
            'status',
            DriftInvitations::STATUS_OPEN
        )->inRandomOrder()->first();
        $user = User::where('id', $driftInvitation->inviteduser_id)
            ->first();
        $token = $this->getTokenFromUser($user);

        $this->mockPushNotification();

        $this->post('/driftinvitations/' . $driftInvitation->id . '/reject', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        
        $this->seeStatusCode(200);

        //Reload the DriftInvitation
        $driftInvitation = DriftInvitations::find($driftInvitation->id);
        $this->assertEquals(
            DriftInvitations::STATUS_REJECTED,
            $driftInvitation->status
        );
    }
}
