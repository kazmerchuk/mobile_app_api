<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A test to see if the application is functional
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/')
             ->see('It works!');
    }
}
