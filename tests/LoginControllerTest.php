<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginControllerTest extends TestCase
{
    /** @test **/
    public function it_should_not_let_the_user_login_without_an_email()
    {
        $this->post('/login', ['password' => 'secret'])
            ->seeJsonEquals([
                'message' => 'The Email field is required.' . PHP_EOL,
                'status' => 'error'
            ]);
    }

    /** @test **/
    public function it_should_not_let_the_user_login_without_a_password()
    {
        $this->post('/login', ['email' => 'a@b.net'])
            ->seeJsonEquals([
                'message' => 'The password field is required.' . PHP_EOL,
                'status' => 'error'
            ]);
    }

    /** @test **/
    public function it_should_throw_an_error_if_a_user_email_is_not_found()
    {
        $this->post('/login', [
            'email' => 'thisemaildoesnotexistindb@hotmail.co.uk',
            'password' => 'secret'
        ])
            ->seeJsonEquals([
                'message' => trans('default.user_not_found'),
                'status' => 'forbidden'
            ]);
    }

    /** @test **/
    public function it_should_show_an_error_if_the_password_doesnot_match()
    {
        $user = App\Models\User::inRandomOrder()->first();
        $this->post('/login', [
            'email' => $user->email,
            'password' => 'fakepassword'
        ])
            ->seeJsonEquals([
                'message' => trans('default.password_not_matched'),
                'status' => 'forbidden'
            ]);
    }

    /** @test **/
    public function it_should_throw_an_error_if_nickname_is_not_provided()
    {
        $this->post('/login/checknickname')
            ->seeJsonEquals([
                'message' => 'The Nickname field is required.' . PHP_EOL,
                'status' => 'error',
            ]);
    }

    /** @test **/
    public function it_should_return_false_if_a_nickname_is_already_taken()
    {
        $user = App\Models\User::inRandomOrder()->first();
        $this->post('/login/checknickname', [
            'nickname' => $user->nickname
        ])
            ->seeJsonEquals([
                'available' => false,
                'status' => 'success',
            ]);
    }

    /** @test **/
    public function it_should_return_true_if_a_nickname_is_available()
    {
        $this->post('/login/checknickname', [
            'nickname' => 'allegaeon-rules'
        ])
            ->seeJsonEquals([
                'available' => true,
                'status' => 'success'
            ]);
    }

    /** @test **/
    public function it_should_throw_an_error_if_an_email_is_missing()
    {
        $this->post('/login/checkemail')
            ->seeJsonEquals([
                'message' => 'The Email field is required.' . PHP_EOL,
                'status' => 'error'
            ]);
    }

    /** @test **/
    public function it_should_return_false_if_an_email_is_already_taken()
    {
        $user = App\Models\User::inRandomOrder()->first();
        $this->post('/login/checkemail', [
            'email' => $user->email
        ])
            ->seeJsonEquals([
                'available' => false,
                'status' => 'success'
            ]);
    }

    /** @test **/
    public function it_should_return_true_if_an_email_is_available()
    {
        $this->post('/login/checkemail', [
            'email' => 'uniqueemail@box.net'
        ])
            ->seeJsonEquals([
                'available' => true,
                'status' => 'success'
            ]);
    }

    /** @test **/
    public function it_should_return_success_and_return_token_on_correct_details()
    {
        $user = App\Models\User::inRandomOrder()->first();
        $this->post('/login', [
            'email' => $user->email,
            'password' => 'secret'
        ])
            ->seeJsonStructure([
                'status',
                'token'
            ]);
    }

    /** @test **/
    public function it_should_not_logout_if_token_is_not_provided()
    {
        $this->post('/logout')
            ->seeJsonEquals([
                'message' => 'The token field is required.' . PHP_EOL,
                'status' => 'error'
            ]);
    }
}
