<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SupportControllerTest extends TestCase
{
    /** @test **/
    public function it_should_not_send_a_message_if_params_are_missing()
    {
        $user = App\Models\User::inRandomOrder()->first();
        $token = $user->userSessions()->first()->token;
        $this->post('/support/submit', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => 'The supportcategory id field is required.' . PHP_EOL
                    . 'The message field is required.' . PHP_EOL
            ]);
    }

    /** @test **/
    public function it_should_throw_an_error_if_an_email_support_category_does_not_exist()
    {
        $token = App\Models\User::inRandomOrder()
            ->first()
            ->userSessions()
            ->first()
            ->token;
        Mail::shouldReceive('send');
        $this->post('/support/submit', [
            'token' => $token,
            'supportcategory_id' => '3',
            'message' => str_random(30)
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
    }

    /** @test **/
    public function it_should_send_an_email_when_the_category_is_correct()
    {
        Mail::shouldReceive('send');
        $token = App\Models\User::inRandomOrder()
            ->first()
            ->userSessions()
            ->first()
            ->token;
        $this->post('/support/submit', [
            'token' => $token,
            'supportcategory_id' => 1,
            'message' => str_random(30)
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
    }
}
