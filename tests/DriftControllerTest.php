<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Drifts;
use App\Models\Photos;

class DriftControllerTest extends TestCase
{
    use DriftServerTestHelperTrait;

    /** @test **/
    public function it_should_not_post_a_review_if_required_params_are_not_present()
    {
        $drift = Drifts::inRandomOrder()->first();
        $user = App\Models\User::find($drift->driver_id);
        $token = $user->userSessions()->first()->token;
        $this->post('/drifts/' . $drift->id . '/review', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => 'The text field is required.' . PHP_EOL
                    . 'The updown field is required.' . PHP_EOL
            ]);
    }

    /** @test **/
    public function it_should_post_a_review_for_a_drift()
    {
        $reviewedDrifts = App\Models\Reviews::all()->pluck('drift_id');
        $drift = Drifts::whereNotIn('id', $reviewedDrifts)
            ->inRandomOrder()
            ->first();
        $user = App\Models\User::find($drift->driver_id);
        $token = $user->userSessions()->first()->token;
        $this->post('/drifts/' . $drift->id . '/review', [
            'token' => $token,
            'updown' => 'up',
            'text' => str_random(50)     
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        $this->seeInDatabase('reviews', [
            'user_id' => $user->id,
            'drift_id' => $drift->id,
            'updown' => '1'
        ]);
    }

    /** @test **/
    public function it_should_show_an_error_if_a_user_has_already_reviewed_the_drift()
    {
        $reviewedDrift = App\Models\Reviews::first();
        $user = App\Models\User::find($reviewedDrift->user_id);
        $token = $user->userSessions()->first()->token;
        $this->post('/drifts/' . $reviewedDrift->drift_id . '/review', [
            'token' => $token,
            'updown' => 'up',
            'text' => str_random(50)     
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => trans('default.review_already_exist')
            ]);
    }

    /** @test **/
    public function it_should_not_start_a_drift_if_it_is_not_found()
    {
        $maxDriftId = Drifts::max('id');
        $token = App\Models\User::inRandomOrder()->first()
            ->userSessions()->first()->token;
        $nonExistingDriftId = $maxDriftId + 10;
        $this->post('/drifts/' . $nonExistingDriftId . '/start', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.drift_not_found')
            ]);
    }

    /** @test **/
    public function it_should_not_start_a_drift_if_the_status_is_not_agreed()
    {
        $drift = Drifts::where('status', '!=', Drifts::STATUS_AGREED)
            ->first();
        $token = App\Models\User::inRandomOrder()->first()
            ->userSessions()->first()->token;
        $this->post('/drifts/' . $drift->id . '/start', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.drift_not_found')
            ]);
    }

    /** @test **/
    public function it_should_start_a_drift_if_the_user_is_a_drifter_or_driver()
    {
        $drift = Drifts::where('status', Drifts::STATUS_AGREED)
            ->first();
        $user = App\Models\User::find($drift->driver_id);
        $token = $user->userSessions()->first()->token;
        $this->post('/drifts/' . $drift->id . '/start', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        // Reload drift
        $drift = Drifts::find($drift->id);
        $this->assertEquals(
            Drifts::STATUS_RUNNING,
            $drift->status
        );
        $lastUserLocation = App\Models\UserLocations::where('user_id', $user->id)
            ->get()->last();
        // The following won't always be not null
        if ($lastUserLocation) {
            $this->assertEquals(
                $drift->startlat,
                $lastUserLocation->lat
            );
            $this->assertEquals(
                $drift->startlon,
                $lastUserLocation->lon
            );
        }
    }

    /** @test **/
    public function it_should_not_end_a_drift_if_it_is_not_found()
    {
        $maxDriftId = Drifts::max('id');
        $nonExistingDriftId = $maxDriftId + 10;
        $token = App\Models\User::inRandomOrder()
            ->first()
            ->userSessions()
            ->first()
            ->token;
        $this->post('/drifts/' . $nonExistingDriftId . '/end', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.drift_not_found')
            ]);
    }

    /** @test **/
    public function it_should_not_end_a_drift_if_the_status_is_not_running()
    {
        $drift = Drifts::where('status', '!=', Drifts::STATUS_RUNNING)
            ->first();
        $token = App\Models\User::inRandomOrder()->first()
            ->userSessions()->first()->token;
        $this->post('/drifts/' . $drift->id . '/end', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.drift_not_found')
            ]);
    }

    /** @test **/
    public function it_should_not_end_a_drift_if_a_user_does_not_belong_to_id()
    {
        $drift = Drifts::where('status', Drifts::STATUS_RUNNING)
            ->first();
        $user = App\Models\User::whereNotIn('id', [$drift->driver_id, $drift->drifter_id])
            ->first();
        $token = $user->userSessions()->first()->token;
        $this->post('/drifts/' . $drift->id . '/end', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.drift_not_found')
            ]);
    }

    /** @test **/
    public function it_should_end_a_drift_if_all_params_are_correct()
    {
        $drift = Drifts::where('status', Drifts::STATUS_RUNNING)
            ->first();
        $user = App\Models\User::find($drift->driver_id);
        $token = $user->userSessions()->first()->token;
        $this->post('/drifts/' . $drift->id . '/end', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        // Reload drift
        $drift = Drifts::find($drift->id);
        $this->assertEquals(
            $drift->status,
            Drifts::STATUS_COMPLETED
        );
        $lastUserLocation = App\Models\UserLocations::where('user_id', $user->id)
            ->get()->last();
        if ($lastUserLocation) {
            $this->assertEquals(
                $drift->endlat,
                $lastUserLocation->lat
            );
            $this->assertEquals(
                $drift->endlon,
                $lastUserLocation->lon
            );
        }
    }

    /** @test **/
    public function it_should_not_cancel_a_drift_if_it_is_not_found()
    {
        $maxDriftId = Drifts::max('id');
        $nonExistingDriftId = $maxDriftId + 10;
        $token = App\Models\User::inRandomOrder()->first()
            ->userSessions()
            ->first()
            ->token;
        $this->post('/drifts/' . $nonExistingDriftId . '/cancel', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.drift_not_found')
            ]);
    }

    /** @test **/
    public function it_should_not_cancel_a_drift_with_an_incorrect_status()
    {
        $drift = Drifts::whereNotIn(
            'status',
            [
                Drifts::STATUS_AGREED,
                Drifts::STATUS_RUNNING
            ]
        )->first();
        $token = App\Models\User::inRandomOrder()->first()
            ->userSessions()
            ->first()
            ->token;
        $this->post('/drifts/' . $drift->id . '/cancel', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.drift_not_found')
            ]);
    }

    /** @test **/
    public function it_should_not_cancel_a_drift_if_the_user_is_not_part_of_the_it()
    {
        $drift = Drifts::where('status', Drifts::STATUS_AGREED)
            ->first();
        $user = App\Models\User::whereNotIn('id', [
            $drift->driver_id,
            $drift->drifter_id
        ])->first();
        $token = $user->userSessions()->first()->token;
        $this->post('/drifts/' . $drift->id . '/cancel', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.drift_not_found')
            ]);
    }

    /** @test **/
    public function it_should_cancel_a_drift_if_all_params_are_correct()
    {
        $drift = Drifts::where('status', Drifts::STATUS_AGREED)
            ->first();
        $user = App\Models\User::where('id', $drift->driver_id)->first();
        $token = $user->userSessions()->first()->token;
        $this->post('/drifts/' . $drift->id . '/cancel', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        // Reload drift
        $drift = Drifts::find($drift->id);
        $this->assertEquals(
            Drifts::STATUS_CANCELLED,
            $drift->status
        );
        $lastUserLocation = App\Models\UserLocations::where('user_id', $user->id)
            ->get()->last();
        if ($lastUserLocation) {
            $this->assertEquals(
                $lastUserLocation->lat,
                $drift->endlat
            );
            $this->assertEquals(
                $lastUserLocation->lon,
                $drift->endlon
            );
        }
    }

    /** @test **/
    public function it_should_not_upload_a_photo_if_a_picture_param_is_missing()
    {
        $token = App\Models\User::inRandomOrder()->first()
            ->userSessions()->first()->token;
        $drift = Drifts::inRandomOrder()->first();
        $this->post('/drifts/' . $drift->id . '/uploadphoto', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => 'The picture field is required.' . PHP_EOL
            ]);
    }

    /** @test **/
    public function it_should_show_an_error_if_drift_is_not_found_when_uploading_photo()
    {
        $maxDriftId = Drifts::max('id');
        $nonExistingDriftId = $maxDriftId + 10;
        $token = App\Models\User::inRandomOrder()->first()
            ->userSessions()->first()->token;
        $this->post('/drifts/' . $nonExistingDriftId . '/uploadphoto', [
            'token' => $token,
            'picture' => 'image.jpg'
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.drift_not_found')
            ]);
    }

    /** @test **/
    public function it_should_show_an_error_if_user_is_not_part_of_the_drift_while_uploading_photo()
    {
        $drift = Drifts::inRandomOrder()->first();
        $user = App\Models\User::whereNotIn('id', [
            $drift->driver_id,
            $drift->drifter_id
        ])->first();
        $token =  $user->userSessions()->first()->token;
        $this->post('/drifts/' . $drift->id . '/uploadphoto', [
            'token' => $token,
            'picture' => 'image.jpg'
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => trans('default.not_participant_of_this_drift')
            ]);
    }

    /** @test **/
    public function it_should_upload_a_photo_if_all_params_are_correct()
    {
        $image = 'type:image/jpg;,carimage.jpg';
        $drift = Drifts::inRandomOrder()->first();
        $user = App\Models\User::find($drift->driver_id);
        $token = $user->userSessions()->first()->token;

        Storage::shouldReceive('disk->put')->once();

        $this->post('/drifts/' . $drift->id . '/uploadphoto', [
            'token' => $token,
            'picture' => $image,
            'caption' => 'this is awesome!'
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        $this->seeInDatabase('photos', [
            'caption' => 'this is awesome!'
        ]);
    }

    /** @test **/
    public function it_should_not_get_drift_photos_if_drift_does_not_exist()
    {
        $maxId = Drifts::max('id');
        $invalidId = $maxId + 10;
        $token = $this->getRandomUserToken();

        $this->get('/drifts/' . $invalidId . '/photos?token=' . $token)
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.drift_not_found')
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_get_drift_photo_list_if_all_params_are_correct()
    {
        $driftIds = Photos::all()->pluck('drift_id');
        $drift = Drifts::whereIn('id', $driftIds)
            ->inRandomOrder()->first();
        $user = $this->getRandomUser();
        $token = $this->getTokenFromUser($user);

        $query = http_build_query([
            'itemcount' => 5,
            'pagenumber' => 1,
            'token' => $token
        ]);

        Storage::shouldReceive('disk->url')->andReturn('image.jpg');

        $this->get('/drifts/' . $drift->id . '/photos?' . $query)
            ->seeJsonStructure([
                'status',
                'drift_id',
                'photos' => [
                    [
                        'caption',
                        'id',
                        'lat',
                        'lon',
                        'timestamp',
                        'url'
                    ]
                ]
            ]);
        
        $this->seeStatusCode(200);
    }
}
