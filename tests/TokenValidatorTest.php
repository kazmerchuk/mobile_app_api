<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TokenValidatorTest extends TestCase
{
    /** @test **/
    public function it_should_not_validate_an_invalid_token()
    {
        $this->post('/changepassword', [
            'token' => 'this_token_is_made_up'
        ])
            ->seeJsonEquals([
                'status' => 'forbidden',
            ]);
        
        $this->seeStatusCode(403);
    }
}
