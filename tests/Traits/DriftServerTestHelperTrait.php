<?php

use App\Models\User;
use App\Models\DeviceToken;

trait DriftServerTestHelperTrait
{
    public function getRandomUser()
    {
        return User::inRandomOrder()->first();
    }

    public function getTokenFromUser(User $user)
    {
        return $user->userSessions()->first()->token;
    }

    public function getRandomUserToken()
    {
        return User::inRandomOrder()->first()
            ->userSessions()->first()->token;
    }

    public function mockPushNotification()
    {
        $randomDevice = DeviceToken::inRandomOrder()->first();
        $deviceTokenMock = Mockery::mock('App\Models\DeviceToken');
        $deviceTokenMock->shouldReceive('where->first')
            ->andReturn($randomDevice);
        $this->app->instance('App\Models\DeviceToken', $deviceTokenMock);

        $guzzleClientMock = Mockery::mock('GuzzleHttp\Client');
        $guzzleClientMock->shouldReceive('post')
            ->andReturn(true);
        $this->app->instance('GuzzleHttp\Client', $guzzleClientMock);
    }
}