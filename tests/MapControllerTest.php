<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MapControllerTest extends TestCase
{
    /** @test **/
    public function it_should_not_fetch_a_user_list_without_required_params()
    {
        $user = App\Models\User::inRandomOrder()->first();
        $token = $user->userSessions()->first()->token;
        $this->get('/map/users?token=' . $token)
            ->seeJsonEquals([
                'status' => 'error',
                'message' => 'The lat field is required.' . PHP_EOL
                    . 'The lon field is required.' . PHP_EOL
                    . 'The distance field is required.' . PHP_EOL
                    . 'The types field is required.' . PHP_EOL
            ]);
    }

    /** @test **/
    public function it_should_fetch_nearby_users_and_return_a_message_if_no_users_found_nearby()
    {
        $user = App\Models\User::inRandomOrder()->first();
        $token = $user->userSessions()->first()->token;
        $faker = $this->app->make('Faker\Generator');
        $this->get(
            '/map/users?token='
            . $token
            . '&lat=' . $faker->latitude(-90, 90)
            . '&lon=' . $faker->longitude(-70, 70)
            . '&distance=100'
            . '&types[]=driver'
        )
            ->seeJsonEquals([
                'status' => 'success',
                'message' => trans('default.not_nearby_users')
            ]);
    }

    /** @test **/
    public function it_should_fetch_nearby_users_if_the_params_are_correct()
    {
         $user = App\Models\User::inRandomOrder()->first();
        $token = $user->userSessions()->first()->token;
        $faker = $this->app->make('Faker\Generator');
        $points = App\Models\UserLocations::all();
        DB::shouldReceive('select')
            ->once()
            ->andReturn($points);
        $this->get(
            '/map/users?token='
            . $token
            . '&lat=' . $faker->latitude(-90, 90)
            . '&lon=' . $faker->longitude(-70, 70)
            . '&distance=100'
            . '&types[]=driver'
        )
            ->seeJsonStructure([
                'status',
                'users'
            ]);
    }

    /** @test **/
    public function it_should_not_fetch_nearby_driftpoints_if_required_params_are_not_passed()
    {
        $user = App\Models\User::inRandomOrder()->first();
        $token = $user->userSessions()->first()->token;
        $this->post('/map/driftpoints', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => 'The lat field is required.' . PHP_EOL
                    . 'The lon field is required.' . PHP_EOL
                    . 'The distance field is required.' . PHP_EOL
            ]);
    }

    /** @test **/
    public function it_should_return_a_message_if_no_nearby_driftpoints_are_found()
    {
        $user = App\Models\User::inRandomOrder()->first();
        $token = $user->userSessions()->first()->token;
        $faker = $this->app->make('Faker\Generator');

        $this->post('/map/driftpoints', [
            'token' => $token,
            'lat' => $faker->latitude(-90, 90),
            'lon' => $faker->longitude(-70, 70),
            'distance' => 100
        ])
            ->seeJsonEquals([
                'status' => 'success',
                'message' => trans('default.not_nearby_drifts')
            ]);
    }

    /** @test **/
    public function it_should_return_nearby_driftpoints_if_params_are_correct()
    {
        $user = App\Models\User::inRandomOrder()->first();
        $token = $user->userSessions()->first()->token;
        $faker = $this->app->make('Faker\Generator');

        $driftPoints = DB::select(
            'SELECT * FROM driftpoints LIMIT 3'
        );
        DB::shouldReceive('select')->once()
            ->andReturn($driftPoints);

        $this->post('/map/driftpoints', [
            'token' => $token,
            'lat' => $faker->latitude(-90, 90),
            'lon' => $faker->longitude(-70, 70),
            'distance' => 100
        ])
            ->seeJsonStructure([
                'status',
                'driftpoints'
            ]);
    }
}
