<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegistrationControllerTest extends TestCase
{
    /** @test **/
    public function it_should_validate_the_registration_request()
    {
        $this->post('/register')
            ->seeJsonEquals([
                'message' => 'The gender field is required.' . PHP_EOL
                    . 'The firstname field is required.' . PHP_EOL
                    . 'The lastname field is required.' . PHP_EOL
                    . 'The Email field is required.' . PHP_EOL
                    . 'The mobile field is required.' . PHP_EOL
                    . 'The address field is required.' . PHP_EOL
                    . 'The zip field is required.' . PHP_EOL
                    . 'The countrycode field is required.' . PHP_EOL
                    . 'The hasdriverslicense field is required.' . PHP_EOL
                    . 'The password field is required.' . PHP_EOL
                    . 'The birthdate field is required.' . PHP_EOL,
                'status' => 'error'
            ]);
    }

    /** @test **/
    public function it_should_register_a_user_with_correct_parameters()
    {
        $this->post('/register', [
            'firstname' => 'Thor',
            'lastname' => 'Viking',
            'email' => 'thor@odin.com',
            'mobile' => '34566533',
            'address' => 'test address',
            'zip' => '123456',
            'countrycode' => 'se',
            'hasdriverslicense' => true,
            'password' => 'secret',
            'gender' => 'male',
            'birthdate' => '2001-10-10'
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        $user = App\Models\User::orderBy('id', 'DESC')->first();
        $this->assertEquals($user->firstname, 'Thor');
        $this->assertEquals($user->lastname, 'Viking');
        $this->assertEquals($user->email, 'thor@odin.com');
        $this->assertEquals($user->mobile, '34566533');
        $this->assertEquals($user->address, 'test address');
        $this->assertEquals($user->zip, '123456');
        $this->assertEquals($user->countrycode, 'se');
        $this->assertEquals($user->hasdriverslicense, '1');
        $this->assertEquals($user->gender, 'male');
    }
}
