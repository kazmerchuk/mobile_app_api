<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Companions;
use App\Models\User;
use App\Models\DriftInvitations;
use App\Models\UserBlock;
use App\Models\DeviceToken;
use App\Models\CompanionRequests;
use Carbon\Carbon;

class UserControllerTest extends TestCase
{
    use DriftServerTestHelperTrait;

    /** @test **/
    public function it_should_not_change_password_if_old_password_is_incorrect()
    {
        $token = User::inRandomOrder()->first()->userSessions()->first()->token;
        $this->post('/changepassword', [
            'oldpassword' => 'incorrect_password',
            'newpassword' => 'yahoo!',
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => 'Wrong old password!'
            ]);
        
        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_not_change_password_is_newpassword_is_not_provided()
    {
        $token = User::inRandomOrder()->first()
            ->userSessions()
            ->first()
            ->token;
        $this->post('/changepassword', [
            'token' => $token,
            'oldpassword' => 'secret'
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => 'The newpassword field is required.' . PHP_EOL
            ]);
        
        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_not_update_the_vehicle_if_platenumber_or_carmodel_is_missing()
    {
        $token = User::inRandomOrder()->first()->userSessions()->first()->token;
        $this->post('/users/me/vehicles/update', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => 'The platenumber field is required.' . PHP_EOL
                    . 'The carmodel field is required.' . PHP_EOL
            ]);
        
        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_create_or_update_the_users_vehicle_if_the_correct_params_are_passed()
    {
        // Shouldn't there be a cleanup script for the images?
        $user = User::inRandomOrder()->first();
        $token = $user->userSessions()->first()->token;
        $this->post('/users/me/vehicles/update', [
            'token' => $token,
            'platenumber' => '4ajku',
            'carmodel' => 'Nissan 350Z'
        ])
            ->seeJsonEquals([
                'status' => 'success',
            ]);
        $this->seeInDatabase('vehicles', [
            'user_id' => $user->id,
            'platenumber' => '4ajku',
            'carmodel' => 'Nissan 350Z'
        ]);
        // Updating the vehicle
        Storage::shouldReceive('disk->put')->andReturn(true);
        $this->post('/users/me/vehicles/update', [
            'token' => $token,
            'platenumber' => 'jaku',
            'carmodel' => 'Toyota Supra',
            'pictureurl' => 'type:image/jpg;,carimage.jpg'
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
            
        $this->seeStatusCode(200);

        $this->seeInDatabase('vehicles', [
            'user_id' => $user->id,
            'platenumber' => 'jaku',
            'carmodel' => 'Toyota Supra'
        ]);
    }

    /** @test **/
    public function it_should_not_update_a_profile_picture_if_param_is_missing()
    {
        $token = User::inRandomOrder()->first()->userSessions()->first()->token;
        $this->post('/users/me/profilepicture', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error'
            ]);
        
        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_add_profile_picture_if_params_are_correct()
    {
        $user = User::inRandomOrder()->first();
        $token = $user->userSessions()->first()->token;
        $oldProfilePicture = $user->profilepictureurl;
        Storage::shouldReceive('disk->put');
        $this->post('/users/me/profilepicture', [
            'token' => $token,
            'picture' => 'type:image/jpg;,profile.jpg'
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        
        $this->seeStatusCode(200);
        
        $user = User::find($user->id); // reload user
        $this->assertNotEquals($user->profilepictureurl, $oldProfilePicture);
    }

    /** @test **/
    public function it_should_set_the_user_to_have_no_vehicles()
    {
        $user = User::where('hasvehicle', true)->first();
        $this->assertEquals(1, $user->hasvehicle);
        $this->post('/users/me/vehicles/havenone', [
            'token' => $user->userSessions()->first()->token
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        
        $this->seeStatusCode(200);

        // Reload user
        $user = User::find($user->id);
        $this->assertEquals(0, $user->hasvehicle);
    }

    /** @test **/
    public function it_should_not_update_about_if_text_is_too_long()
    {
        $token = User::inRandomOrder()->first()
            ->userSessions()
            ->first()
            ->token;
        $text = str_random(181);
        $this->post('/users/me/about', [
            'token' => $token,
            'text' => $text
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => trans('default.about_is_long')
            ]);
        
        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_update_users_about_if_text_length_is_180_or_less()
    {
        $text = str_random(180);
        $user = User::inRandomOrder()->first();
        $token = $user->userSessions()->first()->token;
        
        $this->post('/users/me/about', [
            'token' => $token,
            'text' => $text
        ]);

        $this->seeStatusCode(200);
        
        // Reload user
        $user = User::find($user->id);
        $this->assertEquals($text, $user->about);
    }

    /** @test **/
    public function it_should_return_a_users_profile()
    {
        $token = User::where('hasvehicle', false)
            ->first()
            ->userSessions()
            ->first()
            ->token;
        $this->get('/users/me?token=' . $token)
            ->seeJson([
                'status' => 'success',
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_return_a_profile_with_a_vehicle()
    {
        $token = User::where('hasvehicle', true)
            ->first()
            ->userSessions()
            ->first()
            ->token;
        $this->get('/users/me?token=' . $token)
            ->seeJsonStructure([
                'status',
                'vehicle' => [
                    'carmodel',
                    'pictureurl',
                    'platenumber'
                ]
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_update_a_users_profile()
    {
        $oldUser = User::inRandomOrder()->first();
        $token = $oldUser->userSessions()->first()->token;
        
        $this->post('/users/me', [
            'gender' => '1',
            'firstname' => 'Thor',
            'lastname' => 'Viking',
            'email' => 'test@bc.net',
            'mobile' => '123',
            'gender' => 'male',
            'address' => 'blah blah',
            'zip' => '456',
            'countrycode' => 'de',
            'birthdate' => '1995-06-01',
            'hasdriverslicense' => false,
            'token' => $token,
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        
        $this->seeStatusCode(200);

        // Reload user
        $user = User::find($oldUser->id);
        $this->assertEquals($user->address, 'blah blah');
        $this->assertEquals($user->gender, 'male');
        $this->assertEquals($user->birthdate, '1995-06-01');
        $this->assertEquals($user->hasdriverslicense, 0);
        $this->assertEquals($user->countrycode, 'de');
        $this->assertEquals($user->zip, '456');
    }
    
    /** @test **/
    public function it_should_show_an_error_when_fetching_a_user_with_an_invalid_id()
    {
        $maxId = User::max('id');
        $invalidId = $maxId + 10;
        $token = User::inRandomOrder()->first()
            ->userSessions()->first()->token;
        $this->get('/users/' . $invalidId . '?token=' . $token)
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.user_not_found')
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_return_a_user_with_a_valid_token_and_id()
    {
        $user = User::where('hasvehicle', true)
            ->inRandomOrder()
            ->first();
        $token = $user->userSessions()->first()->token;
        Storage::shouldReceive('disk->url')->andReturn('http://picture.png');
        $this->get('/users/' . $user->id .'?token=' . $token)
            ->seeJsonStructure([
                'status',
                'companions',
                'gender',
                'vehicle' => [
                    'carmodel',
                    'pictureurl',
                    'platenumber'
                ],
                'nickname',
                'mode',
                'gender',
                'upvotes',
                'downvotes',
                'about'
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_get_a_users_last_location()
    {
        $user = User::where('invisiblemode', false)->first();
        $token = $user->userSessions()->first()->token;
        $this->get('/users/' . $user->id . '/location?token=' . $token)
            ->seeJsonStructure([
                'status',
                'lat',
                'lon',
                'user_id'
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_update_a_users_mode()
    {
        $user = User::where('hasvehicle', true)->first();
        $token = $user->userSessions()->first()->token;
        $this->post('/users/me/mode', [
            'token' => $token,
            'mode' => 'driver'
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_update_the_companion_mode_of_a_user()
    {
        $token = User::inRandomOrder()->first()
            ->userSessions()
            ->first()
            ->token;
        $this->post('/users/me/companionmode', [
            'token' => $token,
            'value' => true
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_return_an_error_if_an_invalid_value_is_sent_to_update_a_companion()
    {
        $token = User::inRandomOrder()
            ->first()
            ->userSessions()
            ->first()
            ->token;
        $this->post('/users/me/companionmode', [
            'token' => $token,
            'value' => 'not a boolean'
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => trans('validation.attributes.must_be_boolean')
            ]);
        
        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_update_a_users_visibility_mode()
    {
        $token = User::inRandomOrder()
            ->first()
            ->userSessions()
            ->first()
            ->token;
        $this->post('/users/me/invisiblemode', [
            'token' => $token,
            'value' => true
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_show_an_error_if_an_invalid_value_is_passed_to_update_visibility()
    {
        $token = User::inRandomOrder()->first()
            ->userSessions()
            ->first()
            ->token;
        $this->post('/users/me/invisiblemode', [
            'token' => $token,
            'value' => 'this is not a boolean'
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => trans('validation.attributes.must_be_boolean')
            ]);
        
        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_update_the_invisiblemode_of_a_user_if_the_correct_params_are_passed()
    {
        $user = User::where(
            'invisiblemode',
            false
        )->first();
        $token = $user->userSessions()->first()->token;
        $this->post('/users/me/invisiblemode', [
            'token' => $token,
            'value' => true
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        
        $this->seeStatusCode(200);
        
        // Reload the user model
        $user = User::find($user->id);
        // Make sure the invisiblemode property has been updated
        $this->assertEquals(1, $user->invisiblemode);
    }

    /** @test **/
    public function it_should_return_an_error_if_an_invalid_users_companionlist_is_requested()
    {
        $maxId = User::max('id');
        $token = User::inRandomOrder()->first()
            ->userSessions()->first()->token;
        $invalidId = $maxId + 10;
        $this->get('/users/' . $invalidId . '/companions?token=' . $token)
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.user_not_found')
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_return_a_users_companions_if_the_correct_params_are_passed()
    {
        $user = User::where('hasvehicle', true)
            ->inRandomOrder()->first();
        $token = $user->userSessions()->first()->token;
        $this->get('/users/' . $user->id . '/companions?token=' . $token)
            ->seeJsonStructure([
                'status',
                'companions' => [[
                    'user_id',
                    'nickname',
                    'pictureurl'
                ]]
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_not_return_any_reviews_if_the_user_is_not_found()
    {
        $maxId = User::max('id');
        $invalidId = $maxId + 10;
        $token = User::inRandomOrder()->first()
            ->userSessions()->first()->token;
        $this->get('/users/' . $invalidId . '/reviews?token=' . $token)
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.user_not_found')
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_return_an_empty_reviews_array_if_no_reviews_are_found_for_a_given_user()
    {
        $userIdsWithReviews = App\Models\Reviews::all()->pluck('user_id');
        $user = User::whereNotIn('id', $userIdsWithReviews)
            ->first();
        $token = $user->userSessions()->first()->token;

        $this->get('/users/' . $user->id . '/reviews?token=' . $token)
            ->seeJsonEquals([
                'status' => 'success',
                'reviews' => []
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_return_a_users_reviews_if_all_params_are_correct()
    {
        $userIdsWithReviews = App\Models\Reviews::all()->pluck('user_id');
        $user = User::whereIn('id', $userIdsWithReviews)
            ->inRandomOrder()->first();
        $token = $user->userSessions()->first()->token;

        $this->get('/users/' . $user->id . '/reviews?token=' . $token)
            ->seeJsonStructure([
                'status',
                'reviews' => [
                    [
                        'datetime',
                        'drift_id',
                        'reviewby',
                        'updown',
                        'text'
                    ]
                ]
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_not_get_a_users_drift_list_if_a_user_is_invalid()
    {
        $token = User::inRandomOrder()->first()
            ->userSessions()->first()->token;
        $maxId = User::max('id');
        $invalidId = $maxId + 10;

        $this->get('/users/' . $invalidId . '/drifts?token=' . $token)
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.user_not_found')
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_show_an_error_if_a_drift_invitation_is_not_found()
    {
        $driftInvitedUserIds = DriftInvitations::all()->pluck('inviteduser_id');
        $driftInvitingUserIds = DriftInvitations::all()->pluck('invitinguser_id');
        $user = User::whereNotIn('id', $driftInvitedUserIds)
            ->whereNotIn('id', $driftInvitingUserIds)
            ->first();
        $token = $this->getTokenFromUser($user);
        $query = http_build_query([
            'token' => $token,
            'status' => [
                DriftInvitations::STATUS_OPEN,
                DriftInvitations::STATUS_ACCEPTED,
                DriftInvitations::STATUS_REJECTED
            ]
        ]);
        
        $this->get('/users/me/driftinvitations?' . $query)
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.invites_not_found')
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_get_all_drift_invitations_if_all_params_are_correct()
    {
        $driftInvitation = DriftInvitations::inRandomOrder()->first();
        $user = User::find($driftInvitation->invitinguser_id);
        $token = $this->getTokenFromUser($user);

        $query = http_build_query([
            'token' => $token,
            'status' => [
                DriftInvitations::STATUS_OPEN,
                DriftInvitations::STATUS_ACCEPTED,
                DriftInvitations::STATUS_REJECTED
            ],
            'dateuntil' => Carbon::now()->addYears(1)->toDateTimeString()
        ]);
        $this->get('/users/me/driftinvitations?' . $query)
            ->seeJsonStructure([
                'status',
                'driftinvitations' => [[
                    'id',
                    'inviteduser_id',
                    'invitinguser_id',
                    'invitationdate',
                    'status'
                ]]
            ]);

        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_not_get_a_photo_list_if_a_user_is_not_found()
    {
        $maxId = User::max('id');
        $invalidId = $maxId + 10;
        $token = User::inRandomOrder()
            ->first()->userSessions()->first()->token;
        
        $this->get('/users/' . $invalidId . '/photos?token=' . $token)
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.user_not_found')
            ]);

        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_return_an_empty_photos_list_for_a_user_if_no_photos_are_found()
    {
        $userIdsWithPhotos = App\Models\Photos::all()->pluck('user_id');
        $user = User::whereNotIn('id', $userIdsWithPhotos)
            ->inRandomOrder()->first();
        $token = $user->userSessions()->first()->token;

        $this->get('/users/' . $user->id . '/photos?token=' . $token)
            ->seeJsonEquals([
                'status' => 'success',
                'user_id' => $user->id,
                'photos' => []
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_return_an_array_of_photos_for_a_user_if_all_params_are_correct()
    {
        $userIdsWithPhotos = App\Models\Photos::all()->pluck('user_id');
        $user = User::whereIn('id', $userIdsWithPhotos)
            ->inRandomOrder()->first();
        $token = $user->userSessions()->first()->token;

        $this->get('/users/' . $user->id . '/photos?token=' . $token)
            ->seeJsonStructure([
                'status',
                'user_id',
                'photos' => [
                    ['caption', 'id', 'lat', 'lon', 'timestamp', 'url']
                ]
            ]);

        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_return_an_error_if_required_parameters_are_not_passed_when_adding_a_new_location()
    {
        $token = $this->getRandomUserToken();
        $this->post('/users/me/location', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => 'The lat field is required.' . PHP_EOL
                    . 'The lon field is required.' . PHP_EOL
            ]);
        
        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_update_or_create_a_user_location_when_params_are_correct()
    {
        $user = $this->getRandomUser();
        $token = $this->getTokenFromUser($user);
        $faker = $this->app->make('Faker\Generator');
        $latitude = $faker->latitude(-90, 90);
        $longitude = $faker->longitude(-70, 70);

        $this->post('/users/me/location', [
            'token' => $token,
            'lat' => $latitude,
            'lon' => $longitude
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        
        $this->seeStatusCode(200);
        
        $this->seeInDatabase('userlocations', [
            'user_id' => $user->id,
            'lat' => $latitude,
            'lon' => $longitude
        ]);
    }

    /** @test **/
    public function it_should_show_an_error_if_destination_required_params_are_not_present()
    {
        $token = $this->getRandomUserToken();
        $this->post('/users/me/destination', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => 'The lat field is required.' . PHP_EOL
                    . 'The lon field is required.' . PHP_EOL
                    . 'The caption field is required.' . PHP_EOL
            ]);

        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_update_the_user_destination_when_all_params_are_correct()
    {
        $user = $this->getRandomUser();
        $token = $this->getTokenFromUser($user);
        $faker = $this->app->make('Faker\Generator');

        $latitude = round($faker->latitude(-70, 70), 2);
        $longitude = round($faker->longitude(-90, 90), 2);
        $caption = $faker->sentence;

        $this->post('/users/me/destination', [
            'token' => $token,
            'lat' => $latitude,
            'lon' => $longitude,
            'caption' => $caption
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);

        $this->seeStatusCode(200);

        $this->seeInDatabase('users', [
            'id' => $user->id,
            'destinationlat' => $latitude,
            'destinationlon' => $longitude,
            'destinationcaption' => $caption
        ]);
    }

    /** @test **/
    public function it_should_throw_an_error_if_companion_doesnt_exist()
    {
        $user = $this->getRandomUser();
        $token = $this->getTokenFromUser($user);
        $companion = Companions::where('user_id', '!=', $user->id)
            ->first();
        
        $this->post('/users/me/companions/' . $companion->companion_id . '/remove', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => trans('default.companion_not_found')
            ]);
        
        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_delete_companion_records_if_params_are_correct()
    {
        $companion = Companions::inRandomOrder()->first();
        $user = User::find($companion->user_id);
        $token = $this->getTokenFromUser($user);
        $this->post('/users/me/companions/' . $companion->companion_id . '/remove', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        
        $this->seeStatusCode(200);

        $this->notSeeInDatabase('companions', [
            'user_id' => $user->id,
            'companion_id' => $companion->companion_id
        ]);
        $this->notSeeInDatabase('companions', [
            'companion_id' => $user->id,
            'user_id' => $companion->companion_id
        ]);
    }

    /** @test **/
    public function it_should_not_block_a_companion_if_a_user_is_not_found()
    {
        $maxId = User::max('id');
        $invalidId = $maxId + 10;
        $token = $this->getRandomUserToken();

        $this->post('/users/me/companions/' . $invalidId . '/block', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => trans('default.user_not_found')
            ]);
        
        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_block_a_user_if_all_params_are_correct()
    {
        $user = $this->getRandomUser();
        $companion = $this->getRandomUser();
        $token = $this->getTokenFromUser($user);

        $this->post('/users/me/companions/' . $companion->id . '/block', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        
        $this->seeStatusCode(200);

        $this->seeInDatabase('userblock', [
            'user_id' => $user->id,
            'blockeduser_id' => $companion->id
        ]);
    }

    /** @test **/
    public function it_should_not_unblock_a_companion_if_the_companion_doesnt_exist()
    {
        $token = $this->getRandomUserToken();
        $maxId = User::max('id');
        $invalidId = $maxId + 10;

        $this->post('/users/me/companions/' . $invalidId . '/unblock', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => trans('default.user_not_found')
            ]);
        
        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_unblock_a_user_if_all_params_are_correct()
    {
        $user = $this->getRandomUser();
        $companion = $this->getRandomUser();
        UserBlock::create([
            'user_id' => $user->id,
            'blockeduser_id' => $companion->id
        ]);
        $this->seeInDatabase('userblock', [
            'user_id' => $user->id,
            'blockeduser_id' => $companion->id
        ]);

        $token = $this->getTokenFromUser($user);

        $this->post('/users/me/companions/' . $companion->id .'/unblock', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        
        $this->seeStatusCode(200);

        $this->notSeeInDatabase('userblock', [
            'user_id' => $user->id,
            'blockeduser_id' => $companion->id
        ]);
    }

    /** @test **/
    public function it_should_not_update_users_devicetoken_if_required_params_are_not_present()
    {
        $token = $this->getRandomUserToken();
        $this->post('/users/me/devicetoken', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => 'The client field is required.' . PHP_EOL
                    . 'The devicetoken field is required.' . PHP_EOL
            ]);
        
        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_update_a_devicetoken_if_it_already_exists()
    {
        $deviceToken = DeviceToken::inRandomOrder()->first();
        $user = User::find($deviceToken->user_id);
        $token = $this->getTokenFromUser($user);
        $newDeviceToken = str_random(20);

        $this->post('/users/me/devicetoken', [
            'token' => $token,
            'client' => 'iOS',
            'devicetoken' => $newDeviceToken
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        
        $this->seeStatusCode(200);

        //Reload devicetoken
        $deviceToken = DeviceToken::where('user_id', $user->id)->first();
        $this->assertEquals(
            $deviceToken->token,
            $newDeviceToken
        );
    }

    /** @test **/
    public function it_should_show_an_error_if_a_companion_user_is_not_found()
    {
        $maxId = User::max('id');
        $invalidId = $maxId + 10;
        $token = $this->getRandomUserToken();

        $this->post('/users/' . $invalidId . '/companions/request', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => trans('default.user_not_found')
            ]);
        
        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_show_an_error_if_there_is_already_a_companion_request()
    {
        $companionRequest = CompanionRequests::where('status', CompanionRequests::STATUS_OPEN)
            ->inRandomOrder()->first();
        $user = User::find($companionRequest->invitinguser_id);
        $token = $this->getTokenFromUser($user);

        $this->post('/users/' . $companionRequest->inviteduser_id . '/companions/request', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => trans('default.already_have_companion_invite')
            ]);

        $this->seeStatusCode(400);
    }

    /** @test **/
    public function it_should_create_a_companion_request_if_all_params_are_correct()
    {
        $usersWithoutVehicles = User::where('hasvehicle', false)->get();
        $userId = $usersWithoutVehicles->random()->id;
        $companionId = $usersWithoutVehicles->random()->id;
        $user = User::find($userId);
        $token = $this->getTokenFromUser($user);

        $this->mockPushNotification();

        $this->post('/users/' . $companionId . '/companions/request', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);

        $this->seeStatusCode(200);

        $this->seeInDatabase('companionrequests', [
            'invitinguser_id' => $user->id,
            'inviteduser_id' => $companionId,
            'status' => CompanionRequests::STATUS_OPEN
        ]);
    }

    /** @test **/
    public function it_should_search_for_a_user_and_return_results()
    {
        $token = $this->getRandomUserToken();
        $query = http_build_query([
            'searchtext' => 'e',
            'token' => $token,
            'pagenumber' => 1,
            'itemcount' => 5
        ]);

        $this->get('/users/search?' . $query)
            ->seeJsonStructure([
                'companions' => [
                    [
                        'user_id',
                        'nickname',
                        'pictureurl'
                    ]
                ]
            ]);
        
        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_show_an_error_if_it_does_not_find_a_user_request()
    {
        Artisan::call('migrate:refresh');
        Artisan::call('db:seed');
        $companionRequest = CompanionRequests::all();
        $companionUsers = $companionRequest->pluck('inviteduser_id')->toArray()
            + $companionRequest->pluck('invitinguser_id')->toArray();
        $user = User::whereNotIn(
            'id',
            $companionUsers
        )->first();
        $token = $this->getTokenFromUser($user);

        $query = http_build_query([
            'token' => $token,
            'dateuntil' => Carbon::now()->toDateString(),
            'statuses' => 'open, accepted'
        ]);

         $this->get('/users/me/companions/request?' . $query)
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.companion_requests_not_found')
            ]);

        $this->seeStatusCode(200);
    }

    /** @test **/
    public function it_should_return_companion_requests_as_per_given_criteria()
    {
        $companionRequest = CompanionRequests::inRandomOrder()->first();
        $user = User::find($companionRequest->invitinguser_id);
        $token = $this->getTokenFromUser($user);
        $query = http_build_query([
            'token' => $token,
            'dateuntil' => Carbon::now()->toDateString(),
            'statuses' => 'open, accepted'
        ]);

        $this->get('/users/me/companions/request?' . $query)
            ->seeJsonStructure([
                'status',
            ]);
        
        $this->seeStatusCode(200);
    }
}
