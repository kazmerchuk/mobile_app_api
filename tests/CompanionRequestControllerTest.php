<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\DeviceToken;

class CompanionRequestControllerTest extends TestCase
{
    use \DriftServerTestHelperTrait;

    /** @test **/
    public function it_should_not_accept_a_request_if_no_companion_request_is_present()
    {
        $maxCompanionRequestId = App\Models\CompanionRequests::max('id');
        $invalidCompanionRequestId = $maxCompanionRequestId + 10;
        $token = App\Models\User::inRandomOrder()->first()
            ->userSessions()->first()->token;
        $this->post('/companionrequests/' . $invalidCompanionRequestId . '/accept', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.companionrequest_not_found')
            ]);
    }

    /** @test **/
    public function it_should_not_accept_a_companion_request_if_the_request_status_is_not_open()
    {
        $companionRequest = App\Models\CompanionRequests::where(
            'status', '!=', App\Models\CompanionRequests::STATUS_OPEN
        )->first();
        $token = App\Models\User::inRandomOrder()->first()
            ->userSessions()->first()->token;
        $this->post('/companionrequests/' . $companionRequest->id . '/accept', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.companionrequest_not_found')
            ]);
    }

    /** @test **/
    public function it_should_show_an_error_if_inviting_user_id_is_not_equal_to_current_user_id()
    {
        $companionRequest = App\Models\CompanionRequests::where(
            'status',
            App\Models\CompanionRequests::STATUS_OPEN
        )->first();
        $user = App\Models\User::where('id', '!=', $companionRequest->inviteduser_id)
            ->first();
        $token = $user->userSessions()->first()->token;
        $this->post('/companionrequests/' . $companionRequest->id . '/accept', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => trans('default.companion_wrong_user')
            ]);
    }

    /** @test **/
    public function it_should_accept_a_companion_request_if_all_params_are_correct()
    {
        $companionRequest = App\Models\CompanionRequests::where(
            'status',
            App\Models\CompanionRequests::STATUS_OPEN
        )->first();
        $user = App\Models\User::where('id', $companionRequest->inviteduser_id)
            ->first();
        $token = $user->userSessions()->first()->token;

        $this->mockPushNotification();

        $this->post('/companionrequests/' . $companionRequest->id . '/accept', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        // Reload companionRequest
        $companionRequest = App\Models\CompanionRequests::find($companionRequest->id);
        $this->assertEquals(
            App\Models\CompanionRequests::STATUS_ACCEPTED,
            $companionRequest->status
        );
        $this->seeInDatabase('companions', [
            'user_id' => $user->id,
            'companion_id' => $companionRequest->invitinguser_id
        ]);
        $this->seeInDatabase('companions', [
            'user_id' => $companionRequest->invitinguser_id,
            'companion_id' => $user->id,
        ]);
        $this->assertNotEquals(
            $user->id,
            $companionRequest->invitinguser_id
        );
    }

    /** @test **/
    public function it_should_not_reject_a_request_if_it_is_not_found()
    {
        $maxCompanionRequestId = App\Models\CompanionRequests::max('id');
        $invalidCompanionRequestId = $maxCompanionRequestId + 10;
        $token = App\Models\User::inRandomOrder()->first()
            ->userSessions()->first()->token;
        $this->post('/companionrequests/' . $invalidCompanionRequestId . '/reject', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.companionrequest_not_found')
            ]);
    }

    /** @test **/
    public function it_should_not_reject_a_request_if_its_status_is_not_open()
    {
        $companionRequest = App\Models\CompanionRequests::where(
            'status', '!=', App\Models\CompanionRequests::STATUS_OPEN
        )->first();
        $token = App\Models\User::inRandomOrder()->first()
            ->userSessions()->first()->token;
        $this->post('/companionrequests/' . $companionRequest->id . '/reject', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'not found',
                'message' => trans('default.companionrequest_not_found')
            ]);
    }

    /** @test **/
    public function it_should_not_reject_a_companion_request_if_current_userid_is_not_equal_to_inviteduser_id()
    {
        $companionRequest = App\Models\CompanionRequests::where(
            'status',
            App\Models\CompanionRequests::STATUS_OPEN
        )->first();
        $user = App\Models\User::where('id', '!=', $companionRequest->inviteduser_id)->first();
        $token = $user->userSessions()->first()->token;
        $this->post('/companionrequests/' . $companionRequest->id . '/reject', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'error',
                'message' => trans('default.companion_wrong_user')
            ]);
    }

    /** @test **/
    public function it_should_reject_a_companion_request_if_all_params_are_correct()
    {
        $companionRequest = App\Models\CompanionRequests::where(
            'status',
            App\Models\CompanionRequests::STATUS_OPEN
        )->first();
        $user = App\Models\User::find($companionRequest->inviteduser_id);
        $token = $user->userSessions()->first()->token;

        $this->mockPushNotification();

        $this->post('/companionrequests/' . $companionRequest->id . '/reject', [
            'token' => $token
        ])
            ->seeJsonEquals([
                'status' => 'success'
            ]);
        // Reload companionRequest
        $companionRequest = App\Models\CompanionRequests::find($companionRequest->id);
        $this->assertEquals(
            App\Models\CompanionRequests::STATUS_REJECTED,
            $companionRequest->status
        );
    }
}
