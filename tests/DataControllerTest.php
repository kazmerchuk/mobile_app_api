<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DataControllerTest extends TestCase
{
    /** @test **/
    public function it_should_fetch_the_countries()
    {
        $this->get('/countries')
            ->seeJsonStructure([
                'status',
                'countries' => ['code', 'text']
            ]);
    }
}
